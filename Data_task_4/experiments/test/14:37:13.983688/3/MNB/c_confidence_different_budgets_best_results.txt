Best weighted F1 score with budget 20: 0.357528532074

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 2        | 0        | 0        | 
| neutral  | 3        | 0        | 1        | 
| positive | 4        | 0        | 1        | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
Classifier: MNB
{'C': 0.1, 'verbose': 0, 'intercept_scaling': 1, 'fit_intercept': True, 'max_iter': 1000, 'penalty': 'l2', 'multi_class': 'ovr', 'random_state': None, 'dual': False, 'tol': 0.001, 'solver': 'lbfgs', 'class_weight': 'auto'}
