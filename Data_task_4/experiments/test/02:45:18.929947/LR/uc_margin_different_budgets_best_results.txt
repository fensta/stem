Best weighted F1 score with budget 3: 0.341300669028

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 0        | 0        | 2        | 
| neutral  | 1        | 0        | 3        | 
| positive | 2        | 2        | 1        | 
