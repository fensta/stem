"""
This script represents our whole workflow. In contrast to run_experiments.py.
here we use our own BoW vectorizer.
"""
import ast
import os
import copy
import cPickle
import numpy as np
import shutil
import datetime
import operator

from sklearn.cross_validation import KFold, ShuffleSplit, \
    StratifiedKFold, train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_curve, auc, f1_score, confusion_matrix
from sklearn.multiclass import OneVsOneClassifier, OneVsRestClassifier
from sklearn.preprocessing import label_binarize, StandardScaler
from sklearn.linear_model import LogisticRegression
from nltk.corpus import stopwords
from sklearn.naive_bayes import MultinomialNB
import matplotlib.pyplot as plt
from sklearn.externals import joblib
from sklearn.svm import SVC

from Data_task_4.feature_selection.remove_features import remove_features

from Data_task_4.models.BoW import BoW
from Data_task_4.io.input_output import open_dataset, open_dataset2
from Data_task_4.preprocess.filters import to_bag_of_words, \
    to_bag_of_words_both
from parser.config_parse import parse_config_file
from Data_task_4 import settings
from Data_task_4.models.containers import Option
from manual_improvements import EMOTICONS


def read_config(config_path):
    """
    Read in a configuration file and return the parameters.

    Parameters
    ----------
    config_path: Path to a configuration file.

    Returns
    -------
    List, list, list, list, list, list.
    Returns a quintuple of lists where each one corresponds to a number of
    parameters for input, preprocessing, training, evaluation, feature
    selection and output (in that order). Most of the lists contain
    dictionaries to access the parameters, but for preprocessing lists of
    tuples are used to follow the order specified in the config file.

    """
    out = []
    inp = []
    train = []
    evalu = []
    pre = []
    sel = []
    params = parse_config_file(config_path)
    print params
    for param_set in params:
        if "input" in param_set:
            inp.append(param_set)
        if "preprocess" in param_set:
            pre.append(param_set)
        if "classifier" in param_set:
            train.append(param_set)
        if "evaluation" in param_set:
            evalu.append(param_set)
        if "feature_selection" in param_set:
            sel.append(param_set)
        if "output" in param_set:
            out.append(param_set)
    print "in", inp
    print "pre", pre
    print "train", train
    print "eval", evalu
    print "selection", sel
    print "out", out
    return inp, pre, train, evalu, sel, out


def _filter_tweets(tweets, remove_invalid_tweets=False,
                   ignore_error_messages=False):
    """
    Removes some undesired tweets according to one's needs.

    Parameters
    ----------
    tweets: List of tweet objects.
    remove_invalid_tweets: If True removes invalid tweets from the dataset such
                           that they won't be used for training or anything at
                           all. If False those tweets won't be treated
                           differently. Default: False
    ignore_error_messages: If True, tweets for which the texts couldn't be
                           retrieved will be considered, but the error
                           messages won't count toward the BoW frequencies.
                           If False nothing happens. Default: False

    Returns
    -------
    List of tweet objects that contains only valid tweets.

    """
    # Nothing to do
    if not remove_invalid_tweets and not ignore_error_messages:
        return tweets
    filtered_tweets = []
    dummy = ""
    # Dictionary containing all messages that are deemed invalid
    INVALID_TWEETS = {}
    # Convert keys to lowercase for this function
    for txt in settings.INVALID_TWEETS:
        INVALID_TWEETS[txt.lower()] = None

    for tweet in tweets:
        is_valid = True
        # Ignore all tweets for which no texts could be retrieved
        # Test this at first because after potentially replacing the text,
        # it's impossible to find out
        if remove_invalid_tweets:
            if hasattr(tweet, "text"):
                if tweet.text in INVALID_TWEETS:
                    is_valid = False
            # else:
            #     # If it has no text, it means it was already invalid while
            #     # reading it in
            #     is_valid = False
        # Use placeholder message
        if ignore_error_messages:
            if tweet.text in INVALID_TWEETS:
                tweet.text = dummy
        if is_valid:
            filtered_tweets.append(tweet)
    return filtered_tweets


def _get_vectorizer(options):
    """
    Creates a BoW object according to the parameters.

    Parameters
    ----------
    options: Option object storing all options.
    min_df: Cut-off words occurring less than min_df times in the corpus.
            Range is [0-1]. Integers are interpreted as absolute values.
    max_df: Cut-off words occurring more often than max times in the corpus.
            Range is [0.7-1]. Integers are interpreted as absolute values.
    use_bigrams: True, if bigrams should be added to BoW in addition to
                 unigrams.
    additional_attrs: List of attributes to add to BoW. See sample.ini for a
                      list of possible values. These attributes must exist in
                      the Tweet objects, otherwise it's not working.

    Returns
    -------
    Initialised BoW object.

    """
    # Instantiate a new vectorizer because none exists so far
    vectorizer = BoW(
        min_words=options.min_df,
        max_words=options.max_df,
        use_bigrams=options.use_bigrams,
        additional_attrs=options.more_attrs
    )
    return vectorizer


def preprocess_tweets(tweets, params, selected_tweets=None,
                      vectorizer=None,
                      vectorizer_alt=None,
                      use_cpd=False,
                      cpd_threshold=0.0
                      ):
    """
    Preprocess the tweets according to the configuration parameters.

    Parameters
    ----------
    tweets: List of tweet objects.
    params: Parameters for preprocessing.
    selected_tweets: List with tweets indices to be considered. If None,
                     then all tweets are used, otherwise only the indexed
                     tweets will be used from tweets.
    vectorizer: If None, it means a new one should be trained on the
                vocabulary of tweets. Otherwise an instance of a vectorizer (
                with a vocabulary) must be passed in. This is useful for
                creating the exact same BoW representation of the training
                set for the test set as well.
    vectorizer_alt: BoW vectorizer for the alternative representation.
    use_cpd: If True, Conditional Proportional Difference, is used to reduce
             the feature set.
    cpd_threshold: Threshold for CPD when to discard a word. 0.0 = Keep all,
                   1.0 = remove all that don't occur only within a single class.

    Returns
    -------
    List, list, BoW, Bow.
    List of tweet objects that were preprocessed accordingly for the normal
    representation, for the alternative representation, and the two
    corresponding BoW vectorizer objects (first one: normal, second:
    alternative).

    """
    # Use only the tweets indexed in selected_tweets as corpus
    if selected_tweets is not None:
        tweets = [tweets[i] for i in selected_tweets]
    # Variables are all initialized with default values that are used anyway
    # Bag-of-word transformation
    use_bow = False
    # lowercase = True
    stop_words_sci = "None"
    stop_words_nltk = "None"
    # binary = False
    # stop_words = "None"
    bow = Option()
    bow.remove_hashtags = False
    bow.remove_urls = False
    bow.remove_emojis = False
    bow.remove_handles = False
    bow.remove_punctuation = False
    bow.use_polarity_for_bow = False
    bow.use_polarity = False
    bow.remove_stopwords = False
    bow.stemmer = "None"
    bow.stemmer_lang = "None"
    bow.alternative_bow = False
    bow.no_preprocessing = False
    bow.max_df = 1.0
    bow.min_df = 0.0
    bow.use_bigrams = False
    bow.max_features = "None"
    bow.more_attrs = []
    bow.use_emojis = False
    bow.use_pos_tags = False
    # defined in the config and extract them first
    for param in params[0]["preprocess"]:   # Param is a tuple: (key, value)
        # Read in all parameters for BoW transformation and applied it
        if param[0] == "bow":
            use_bow = True
            for p in param[1]:
                # Get the key and value from the string
                k, v = p.split(":")
                k = k.split()[0]
                # If value isn't empty
                if len(v) > 0:
                    v = v.split()[0]
                # if k == "lowercase":
                #     if v == "False":
                #         lowercase = False
                if k == "stop_words_sci":
                    if v != "None":
                        stop_words_sci = v
                if k == "stop_words_nltk":
                    if v != "None":
                        stop_words_nltk = stopwords.words(v)
                if k == "max_df":
                    # Float and ints have different meaning here -> careful!
                    if v != "1.0":
                        # float number
                        try:
                            bow.max_df = int(v)
                        except ValueError:
                            bow.max_df = float(v)
                if k == "min_df":
                    # Float and ints have different meaning here -> careful!
                    if v != "1.0":
                        # float number
                        try:
                            bow.min_df = int(v)
                        except ValueError:
                            bow.min_df = float(v)
                # if k == "binary":
                #     if v == "True":
                #         binary = True
                if k == "use_bigrams":
                    if v == "True":
                        bow.use_bigrams = True
                # if k == "max_features":
                #     if v != "None":
                #         max_features = int(v)
                if k == "alternative_bow":
                    if v == "True":
                        bow.alternative_bow = True
                # if k == "ignore_invalid_tweets":
                #     if v == "True":
                #         ignore_invalid_tweets = True
                # if k == "ignore_error_messages":
                #     if v == "True":
                #         ignore_error_messages = True
                # if k == "n_gram_range":
                #     if v != "None":
                #         n_gram_range = literal_eval(v)
                if k == "more_attributes":
                    # If it's a non-empty list
                    if len(v) > 0:
                        bow.more_attrs = v.split(",")
                    else:
                        bow.more_attrs = []
                if k == "no_preprocessing":
                    if v == "True":
                        bow.no_preprocessing = True
                # if k == "strip_accents":
                #     if v != "None":
                #         strip_accents = v
                if k == "stemmer":
                    if v != "None":
                        bow.stemmer = v
                if k == "stemmer_lang":
                    if v != "None":
                        bow.stemmer_lang = v
                if k == "use_pos_tags":
                    if v == "True":
                        bow.remove_hashtags = True
                if k == "remove_urls":
                    if v == "True":
                        bow.remove_urls = True
                if k == "use_emoticons":
                    if v == "True":
                        bow.use_emojis = True
                if k == "remove_handles":
                    if v == "True":
                        bow.remove_handles = True
                if k == "remove_punctuation":
                    if v == "True":
                        bow.remove_punctuation = True
                if k == "polarity_for_bow":
                    if v == "True":
                        bow.use_polarity_for_bow = True
                if k == "use_polarity":
                    if v == "True":
                        bow.use_polarity = True
                if k == "remove_stopwords":
                    if v == "True":
                        bow.remove_stopwords = True
                if k == "use_pos_tags":
                    if v == "True":
                        bow.use_pos_tags = True
            # Give higher precedence to NLTK stop word list if both lists are
            #  set
            if stop_words_nltk != "None":
                stop_words = stop_words_nltk
            elif stop_words_sci != "None":
                stop_words = stop_words_sci
            print "BoW", use_bow
            # print "lowercase", lowercase
            # print "stop_words sci", stop_words_sci
            # print "stop_words nltk", stop_words_nltk
            # print "stop_words", stop_words
            print "max_df", bow.max_df
            print "min_df", bow.min_df
            # print "binary", binary
            # print "max_features", max_features
            print "use_bigrams?", bow.use_bigrams
            # print "discard invalid tweets?", ignore_invalid_tweets
            # print "ignore error messages in BoW?", ignore_error_messages
            # print "n-gram range", n_gram_range
            # print "strip_accents", strip_accents
            print "stemmer", bow.stemmer
            print "stemmer language", bow.stemmer_lang
            print "vectorizer", vectorizer
            print "preprocess before turning into BoW?", bow.no_preprocessing
            print "additional attributes", bow.more_attrs
            print "use alternative representation?", bow.alternative_bow
            print "remove hashtags?", bow.remove_hashtags
            print "remove punctuation?", bow.remove_punctuation
            print "remove urls?", bow.remove_urls
            print "use emoticons?", bow.use_emojis
            print "remove twitter handles?", bow.remove_handles
            print "keep polarity bearing words in BoW?", \
                bow.use_polarity_for_bow
            print "use sentence polarity?", bow.use_polarity
            print "use POS tags?", bow.use_pos_tags
            if use_bow:
                if (bow.more_attrs is None or len(bow.more_attrs) == 0) and (
                        bow.alternative_bow is True):
                    raise ValueError("You want to use an alternative "
                                     "representation, but don't specify any "
                                     "features. Please select some features "
                                     "for 'more_attributes' in your config "
                                     "file.")
                if not bow.use_pos_tags and (bow.use_polarity or
                                             bow.use_polarity_for_bow):
                    raise ValueError("You want to use sentence polarity or "
                                     "keep polarity words, but don't use POS "
                                     "tags. Please enable them - otherwise "
                                     "it's not working!")
                # If no vectorizer exists so far
                do_train = True if vectorizer is None else False
                if vectorizer is None:
                    vectorizer = _get_vectorizer(bow)
                    # Alternative representation uses the same vectorizer
                    if len(bow.more_attrs) > 0:
                        vectorizer_alt = _get_vectorizer(bow)
                # Always create the alternative representation for free
                if len(bow.more_attrs) > 0:
                    # If CPD was used earlier and we want to remove the same
                    # words again in test data
                    if hasattr(vectorizer, "removed_words"):
                        tweets_normal, tweets_alternative, vectorizer_normal, \
                        vectorizer_alternative = to_bag_of_words_both(
                            tweets,
                            bow,
                            vectorizer=vectorizer,
                            vectorizer_alt=vectorizer_alt,
                            do_train=do_train,
                            use_cpd=use_cpd,
                            cpd_threshold=cpd_threshold,
                            removed_words=vectorizer.removed_words
                        )
                    else:
                        tweets_normal, tweets_alternative, vectorizer_normal, \
                        vectorizer_alternative = to_bag_of_words_both(
                            tweets,
                            bow,
                            vectorizer=vectorizer,
                            vectorizer_alt=vectorizer_alt,
                            do_train=do_train,
                            use_cpd=use_cpd,
                            cpd_threshold=cpd_threshold,
                        )

                else:
                    # CPD was used previously
                    if hasattr(vectorizer, "removed_words"):
                        tweets_normal, vectorizer_normal = to_bag_of_words(
                            tweets,
                            bow,
                            vectorizer=vectorizer,
                            do_train=do_train,
                            use_cpd=use_cpd,
                            cpd_threshold=cpd_threshold,
                            removed_words=vectorizer.removed_words
                        )
                    else:
                        # CPD wasn't used
                        tweets_normal, vectorizer_normal = to_bag_of_words(
                            tweets,
                            bow,
                            vectorizer=vectorizer,
                            do_train=do_train,
                            use_cpd=use_cpd,
                            cpd_threshold=cpd_threshold,
                        )
                    tweets_alternative = []
                    vectorizer_alternative = None

    return tweets_normal, tweets_alternative, vectorizer_normal, \
           vectorizer_alternative


def _remove_invalid_tweets(tweets, params):
    """
    Removes some of the tweets depending on the user's preferences. Tweets
    are removed if they contain invalid messages, i.e. their texts couldn't
    be retrieved.

    Parameters
    ----------
    tweets: List of tweet objects.
    params: List of pre-processing parameters to be evaluated.

    Returns
    -------
    List of valid tweet objects.

    """
    ignore_invalid_tweets = False
    ignore_error_messages = False
    for param in params[0]["preprocess"]:
        print param
        print param[0]
        if param[0] == "ignore_invalid_tweets":
            if param[1][0] == "True":
                    ignore_invalid_tweets = True
            if param[0] == "ignore_error_messages":
                if param[1][0] == "True":
                    ignore_error_messages = True
    print "discard invalid tweets?", ignore_invalid_tweets
    print "ignore error messages in BoW?", ignore_error_messages
    return _filter_tweets(
                tweets,
                remove_invalid_tweets=ignore_invalid_tweets,
                ignore_error_messages=ignore_error_messages,
            )


def get_random_forest(params):
    """
    Instantiates a Random Forest classifier in scikit.

    Parameters
    ----------
    params: List of parameters to be used for instantiating the classifier.

    Returns
    -------
    RandomForest classifier from scikit.

    """
    n_estimators = 10
    criterion = "gini"
    max_features = "auto"
    max_depth = None
    min_samples_split = 2
    min_samples_leaf = 1
    min_weight_fraction_leaf = 0.0
    max_leaf_nodes = None
    bootstrap = True
    oob_score = True
    n_jobs = -1
    random_state = None
    verbose = 0
    warm_start = False
    class_weight = None

    for param, val in params[0]["classifier"].iteritems():
        if param == "n_estimators":
            n_estimators = int(val)
        if param == "criterion":
            criterion = val
        if param == "max_features":
            try:
                max_features = int(val)
            except ValueError:
                try:
                    max_features = float(val)
                except ValueError:
                    max_features = val
        if param == "max_depth":
            try:
                max_depth = int(val)
            except ValueError:
                max_depth = None
        if param == "min_samples_split":
            min_samples_split = int(val)
        if param == "min_samples_leaf":
            min_samples_leaf = int(val)
        if param == "min_weight_fraction_leaf":
            min_weight_fraction_leaf = float(val)
        if param == "max_leaf_nodes":
            try:
                max_leaf_nodes = int(val)
            except ValueError:
                max_leaf_nodes = None
        if param == "bootstrap":
            if val == "False":
                bootstrap = False
        if param == "oob_score":
            if val == "False":
                oob_score = False
        if param == "n_jobs":
            n_jobs = int(val)
        if param == "random_state":
            try:
                random_state = int(val)
            except ValueError:
                random_state = 42
        if param == "verbose":
            verbose = int(val)
        if param == "warm_start":
            if val == "True":
                warm_start = True
        if param == "class_weight":
            try:
                class_weight = ast.literal_eval(val)
            except ValueError:
                class_weight = None
    print "Random Forest parameters"
    print "n_estimators", n_estimators
    print "criterion", criterion
    print "max_features", max_features, type(max_features)
    print "max_depth", max_depth, type(max_depth)
    print "min_samples_split", min_samples_split
    print "min_samples_leaf", min_samples_leaf
    print "min_weight_fraction_leaf", min_weight_fraction_leaf
    print "max_leaf_nodes", max_leaf_nodes
    print "bootstrap", bootstrap
    print "oob_score", oob_score
    print "n_jobs", n_jobs, type(n_jobs)
    print "random_state", random_state
    print "verbose", verbose
    print "warm_start", warm_start
    print "class_weight (left out if equal weights)", class_weight, \
        type(class_weight)
    # Equal class weights
    if class_weight is None:
        print "equal class weight"
        return RandomForestClassifier(
            n_estimators=n_estimators,
            criterion=criterion,
            max_features=max_features,
            max_depth=max_depth,
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            min_weight_fraction_leaf=min_weight_fraction_leaf,
            max_leaf_nodes=max_leaf_nodes,
            bootstrap=bootstrap,
            oob_score=oob_score,
            n_jobs=n_jobs,
            random_state=random_state,
            verbose=verbose,
            warm_start=warm_start,
        )
    else:
        # Weighted classes
        print "weighted classes"
        return RandomForestClassifier(
            n_estimators=n_estimators,
            criterion=criterion,
            max_features=max_features,
            max_depth=max_depth,
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            min_weight_fraction_leaf=min_weight_fraction_leaf,
            max_leaf_nodes=max_leaf_nodes,
            bootstrap=bootstrap,
            oob_score=oob_score,
            n_jobs=n_jobs,
            random_state=random_state,
            verbose=verbose,
            warm_start=warm_start,
            class_weight=class_weight,
        )


def get_mnb(params):
    """
    Instantiates a Multinomial Naive Bayes classifier in scikit.

    Parameters
    ----------
    params: List of parameters to be used for instantiating the classifier.

    Returns
    -------
    MNB classifier from scikit.

    """
    learn_prior = True
    alpha = 1.0
    for param, val in params[0]["classifier"].iteritems():
        if param == "learn_prior":
            learn_prior = True if val == "True" else False
        if param == "alpha":
            alpha = float(val)
    return MultinomialNB(alpha=alpha, fit_prior=learn_prior, class_prior=None)


def get_logistic_regression(params):
    """
    Instantiates a Logistic regression classifier in scikit.

    Parameters
    ----------
    params: List of parameters to be used for instantiating the classifier.

    Returns
    -------
    Logistic regression classifier from scikit.

    """
    dual = False
    C = 1.0
    seed = 42
    max_iter = 1000
    solver = "liblinear"
    penalty = "l1"
    class_weight = None
    tolerance = 0.001
    multi_class = None

    for param, val in params[0]["classifier"].iteritems():
        if param == "dual":
            dual = True if val == "True" else False
        if param == "c":
            C = float(val)
        if param == "random_state":
            seed = int(val)
        if param == "tol":
            tolerance = float(val)
        if param == "solver":
            solver = val
        if param == "max_iter":
            max_iter = int(val)
        if param == "penalty":
            penalty = val
        if param == "multi_class":
            if val == "None":
                multi_class = None
            else:
                multi_class = val
        if param == "class_weight":
            try:
                class_weight = ast.literal_eval(val)
            except ValueError:
                if val == "auto":
                    class_weight = val
                else:
                    class_weight = None
    print "Logistic regression parameters"
    print "dual", dual
    print "C", C
    print "seed", seed
    print "tolerance", tolerance
    print "solver", solver
    print "max_iter", max_iter
    print "penalty", penalty
    print "multi-class", multi_class, type(multi_class)
    print "class_weight (left out if equal weights)", class_weight, \
        type(class_weight)
    # Equal class weights
    if class_weight is None:
        # No multi-class
        if multi_class is None:
            print "equal class weights, binary problem"
            return LogisticRegression(
                dual=dual,
                C=C,
                random_state=seed,
                tol=tolerance,
                solver=solver,
                max_iter=max_iter,
                penalty=penalty,
            )
        else:
            print "equal class weights, multi-class problem"
            return LogisticRegression(
                dual=dual,
                C=C,
                random_state=seed,
                tol=tolerance,
                solver=solver,
                max_iter=max_iter,
                penalty=penalty,
                multi_class=multi_class
            )
    else:
        # Weighted classes
        # No multi-class
        if multi_class is None:
            print "weighted class weights, binary problem"
            return LogisticRegression(
                dual=dual,
                C=C,
                random_state=seed,
                tol=tolerance,
                solver=solver,
                max_iter=max_iter,
                penalty=penalty,
                class_weight=class_weight
            )
        else:
            print "weighted class weights, multi-class problem"
            return LogisticRegression(
                dual=dual,
                C=C,
                random_state=seed,
                tol=tolerance,
                solver=solver,
                max_iter=max_iter,
                penalty=penalty,
                multi_class=multi_class,
                class_weight=class_weight
            )


def get_svm(params):
    """
    Instantiates an SVM classifier in scikit.

    Parameters
    ----------
    params: List of parameters to be used for instantiating the classifier.

    Returns
    -------
    SVM classifier from scikit.

    """
    C = 1.0
    kernel = "rbf"
    degree = 3
    shrinking = True
    gamma = 0.0
    coeff = 0.0
    seed = 42
    class_weight = None
    tolerance = 0.001
    cache_size = None
    max_iter = -1
    for param, val in params[0]["classifier"].iteritems():
        if param == "c":
            C = float(val)
        if param == "kernel":
            kernel = val
        if param == "degree":
            degree = int(val)
        if param == "shrinking":
            shrinking = True if val == "True" else False
        if param == "gamma":
            gamma = float(val)
        if param == "coef0":
            coeff = float(val)
        if param == "random_state":
            seed = int(val)
        if param == "max_iter":
            max_iter = int(val)
        if param == "tol":
            tolerance = float(val)
        if param == "cache_size":
            cache_size = int(val) if val != "None" else None

    print "C", C
    print "kernel", kernel
    print "degree", degree
    print "shrinking", shrinking
    print "gamma", gamma
    print "coef0", coeff
    print "seed", seed
    print "max_iter", max_iter
    print "tol", tolerance
    print "cache size", cache_size

    if cache_size is None:
        return SVC(
            C=C,
            kernel=kernel,
            degree=degree,
            shrinking=shrinking,
            gamma=gamma,
            coef0=coeff,
            random_state=seed,
            max_iter=max_iter,
            tol=tolerance,
        )
    else:
        return SVC(
            C=C,
            kernel=kernel,
            degree=degree,
            shrinking=shrinking,
            gamma=gamma,
            coef0=coeff,
            random_state=seed,
            max_iter=max_iter,
            tol=tolerance,
            cache_size=cache_size
        )


def get_classifier(params):
    """
    Creates a classifier based on the parameters.

    Parameters
    ----------
    params: List of parameters to be used for instantiating the classifier.

    Returns
    -------
    Classifier based on the parameters. If the specified algorithm isn't
    supported, None is returned.

    """
    algo = params[0]["classifier"]["name"]
    is_multi_class = False
    mc_type = None
    if "multi_class" in params[0]["classifier"] and not algo == \
            "LogisticRegression":
        is_multi_class = True
        print "multi-class dict", params[0]["classifier"]["multi_class"]
        if "type" in params[0]["classifier"]["multi_class"]:
            mc_type = params[0]["classifier"]["multi_class"]["type"]
        else:
            raise ValueError("In the config file multi-class is selected, "
                             "but no type is specified.")
    print "use multi-class?", is_multi_class
    print "type?", mc_type
    clf = None
    if algo == "RandomForest":
        clf = get_random_forest(params)
    if algo == "LogisticRegression":
        clf = get_logistic_regression(params)
    # MNB has no seed
    if algo == "MultiNominalNaiveBayes":
        clf = get_mnb(params)
    if algo == "SVM":
        clf = get_svm(params)
    if algo == "KNearestNeighbor":
        use_knn = True
    if is_multi_class:
        if mc_type == "OneVsOne":
            return OneVsOneClassifier(clf)
        else:
            return OneVsRestClassifier(clf)
    return clf


def evaluate(classifier, tweets, eval_params, pre_params, sel_params,
             output_dir, test_path):
    """
    Evaluate a given classifier according to the specified parameters.

    Parameters
    ----------
    classifier: Scikit classifier.
    tweets: List of tweet objects representing the dataset.
    eval_params: Parameters from the configuration file to be used for
                 evaluation.
    pre_params: Parameters used for pre-processing.
    sel_params: Parameters for feature selection.
    output_dir: Directory in which all results should be stored.
    test_path: Path to the test set. Will be ignored if no test set should be
               used for evaluations.

    Returns
    -------
    Specified scores/metrics indicating how successful the given model is.

    """
    use_grid_search = False
    use_split = False
    use_cv = False
    use_test_set = False
    # Add all selected scores
    metrics = []
    seed = 42
    precomputed = None
    is_submission = False
    use_precomputed = False
    is_regression = False
    use_precomputed_test_set = False
    build_full_model = False
    continue_fold = False
    use_precomputed_full = False
    do_cv_before_full = True
    # Default values for CV
    cv_params = {
        "cv_type": "k",
        "shuffle": True,
        "train_size": 0.8,
        "outer_folds": 10,
        "inner_folds": 10,
    }
    # Default values for splitting data into train and test set
    split_params = {
        "train_size": 0.8,
        "shuffle": True,
    }
    params = eval_params[0]["evaluation"]
    for k, v in params.iteritems():
        # print k, ":", v
        if k == "no_cv":
            if v == "True":
                do_cv_before_full = False
        if k == "build_full_model":
            if v == "True":
                build_full_model = True
        if k == "is_submission":
            if v == "True":
                is_submission = True
        if k == "metrics":
            if "f1" in params[k]:
                metrics.append("f1")
            if "precision" in params[k]:
                metrics.append("precision")
            if "recall" in params[k]:
                metrics.append("recall")
            if "accuracy" in params[k]:
                metrics.append("accuracy")
            if "roc" in params[k]:
                metrics.append("roc"),
            if "confusion_matrix" in params[k]:
                metrics.append("confusion_matrix")
            if "support" in params[k]:
                metrics.append("support")
            # TODO: This is optimization
            if "learning_curve" in params[k]:
                metrics.append("learning_curve")
        if k == "is_regression":
            if v == "True":
                is_regression = True
        if k == "precomputed":
            precomputed = params[k]["base"]
            if params[k]["use"] == "True":
                use_precomputed = True
            if params[k]["use_full"] == "True":
                use_precomputed_full = True
            if params[k]["continue_fold"] == "True":
                continue_fold = True
            if params[k]["use_test_set"] == "True":
                use_precomputed_test_set = True
        if k == "cv":
            use_cv = True
            if "inner_folds" in params[k]:
                cv_params["inner_folds"] = int(params[k]["inner_folds"])
            if "outer_folds" in params[k]:
                cv_params["outer_folds"] = int(params[k]["outer_folds"])
            if "train_size" in params[k]:
                cv_params["train_size"] = float(params[k]["train_size"])
            if "shuffle" in params[k]:
                if params[k]["shuffle"] == "False":
                    cv_params["shuffle"] = False
            if "type" in params[k]:
                cv_params["cv_type"] = params[k]["type"]
        if k == "split":
            use_split = True
            if "train_size" in params[k]:
                split_params["train_size"] = float(params[k]["train_size"])
            if "shuffle" in params[k]:
                split_params["shuffle"] = True if params[k]["shuffle"] == \
                                                 "True" else False
        # TODO: Grid search is also part of optimization -> put it there
        if k == "grid_search":
            use_grid_search = True
        if k == "seed":
            seed = v
        if k == "use_test_set":
            if v == "True":
                use_test_set = True

    print "regression problem?", is_regression
    print "use CV?", use_cv
    print "store/load dataset in/from", precomputed
    print "use pre-built dataset?", use_precomputed
    print "use pre-built testset?", use_precomputed_test_set
    print "use grid search?", use_grid_search
    print "use split?", use_split
    print "metrics for evaluation", metrics
    print "seed", seed
    print "cv parameters", cv_params
    print "build full model?", build_full_model
    print "split parameters", split_params
    # output_dir = out_params[0]["output"]["base"]
    print "Store results under:", output_dir
    print "use test set?", use_test_set
    print "Continue building folds?", continue_fold
    print "Is submission file?", is_submission
    print "Perform CV before building training set?", do_cv_before_full
    # CV has precedence over simple split
    if use_cv and use_split:
        use_split = False
    if use_cv:
        print "use cv"
        for metric in metrics:
            # Run CV
            score = _run_cv(tweets, classifier, metric, pre_params, sel_params,
                            output_dir, test_path, precomputed,
                            use_test_set=use_test_set,
                            use_partial=continue_fold,
                            use_precomputed=use_precomputed,
                            use_precomputed_full=use_precomputed_full,
                            is_regression=is_regression,
                            build_full_model=build_full_model,
                            use_precomputed_test_set=use_precomputed_test_set,
                            seed=seed,
                            is_submission=is_submission,
                            do_cv_before_full=do_cv_before_full,
                            **cv_params)
    if use_split:
        print "use split"
        score = _run_split_evaluation(tweets, classifier, metric, pre_params,
                                      output_dir, seed=seed, **split_params)
    if use_grid_search:
        print "use grid search"


def _run_cv(tweets, clf, score, pre_params, sel_params, output_dir,
            test_path, prebuilt, use_test_set=False,
            use_precomputed=False, is_regression=True, cv_type="k",
            build_full_model=False, use_precomputed_full=False,
            use_precomputed_test_set=False,
            use_partial=False, use_alternative=False,
            outer_folds=10, inner_folds=10, train_size=0.8, seed=42,
            shuffle=True, is_submission=False, do_cv_before_full=True):
    """
    Performs CV with the given values. How to calculate ROC and F1 in an
    unbiased manner with CV?
    see "Apples-to-Apples in Cross-Validation Studies: Pitfalls in Classifier
    Performance Measurement" by Forman and Scholz: average ROC over the folds
    and compute F1 at the end based on the final confusion table for all
    classes.

    Parameters
    ----------
    tweets: Dataset to be used. In our case the tweet texts in BoW
            representation.
    clf: Scikit classifier object used in CV.
    score: Score to be used for evaluating the suitability of a given model.
    pre_params: Parameters used for pre-processing.
    sel_params: Parameters used for feature selection.
    output_dir: Base directory in which the results will be stored.
    test_path: Path to test set. Ignored, iff use_test_set is False.
    prebuilt: If not None, the preprocessed (stored) dataset with the given
              name will be used for CV. The user has to make sure that it's
              suitable (data must be available for each fold).
    use_test_set: True if separate test set should be used for evaluation.
    use_precomputed: If True, an existing prebuilt dataset will be used and
                     the dataset won't be preprocessed again. User must
                     guarantee that it exists.
    use_precomputed_full: True, if a stored prebuilt full dataset should be
                          used for building the full model. Default: False.
    use_partial: True, if the partially existing model should be continued to
                 be built.
    use_alternative: True, if the precomputed alternative model should be
                     used. Otherwise the standard one is used.
    use_precomputed_test_set: True, if precomputed test set should be loaded.
                              Otherwise it will be created from scratch.
    is_regression: True if labels are numbers. Otherwise it's treated as a
                   classification problem.
    cv_type: String to specify which type of CV to use. Valid values are:
             "shuffle" for random permutations CV, "k" for simple k-fold CV,
             "bootstrap" for bootstrapped CV or
             "stratified" for stratified CV. Default = "k".
    build_full_model: Build a model on full training set after CV.
    outer_folds: Number of folds in outer loop of nested CV.
    inner_folds: Number of folds in inner loop of nested CV.
    train_size: Amount of data to be used for training. This is only relevant if
                "shuffle" for cv_type is selected. Parameter will be ignored
                otherwise.
    seed: Seed for PRNG to allow reproducibility of results.
    shuffle: Dataset will be shuffled prior to creating folds if True. If
             False, dataset will be split as is.
    is_submission: True, if test set should be submitted. <use_test_set> must
                   be True for this parameter to be considered. If True,
                   no evaluation statistics are computed. <build_full_model>
                   must be True as well.
    do_cv_before_full: If True, CV will be performed before building model on
                       full training set.

    Raises
    ------
    ValueError: if use_test_set is True and build_full_model is False.

    """
    if use_test_set and not build_full_model and not use_precomputed_full:
        raise ValueError("You must build a model on the full dataset to test "
                         "it with a separate dataset. Either set "
                         "'build_full_model' to True or 'use_test_set' to "
                         "false")

    # Use separate classifiers for different feature spaces
    # 1 classifier for basic features + hashtags
    # 1 classifier for basic features + emoticons
    # 1 classifier for basic features

    # Extract feature selection parameters
    use_cpd = False
    use_feature_selection = False
    params = sel_params[0]["feature_selection"]
    removed_features = []
    cpd_threshold = 0.0
    for k, v in params.iteritems():
        if k == "dimensionality_reduction":
            if v == "cpd":
                use_cpd = True
        if k == "features_to_remove":
            removed_features = v.split(",")
        if k == "use":
            if v == "True":
                use_feature_selection = True
        if k == "cpd_threshold":
            cpd_threshold = float(v)
    # Ignore CPD threshold if it shouldn't be used
    if not use_cpd:
        cpd_threshold = 0.0
    print "use feature selection?", use_feature_selection
    print "which features to remove?", removed_features
    print "use CPD?", use_cpd
    print "CPD threshold", cpd_threshold
    # It could happen that some tweets need to be removed -> do this before
    # computing the folds!
    tweets = _remove_invalid_tweets(tweets, pre_params)
    CLASSES = list(set([t.label for t in tweets]))
    print "#CLASSES", CLASSES
    CLASSES = sorted(CLASSES)
    # Location of preprocessed dataset
    PREBUILT_DATASET = "../preprocessed_datasets"
    outer = _get_cv_iterator(len(tweets), cv_type=cv_type, folds=outer_folds,
                             train_size=train_size, seed=seed, shuffle=shuffle)

    # Evaluate multi-class classifiers correctly
    is_multi_class = False
    if isinstance(clf, OneVsRestClassifier):
        is_multi_class = True
    if isinstance(clf, OneVsOneClassifier):
        is_multi_class = True
    # Logistic regression handles multi-class internally, so we need to check
    #  it separately and overwrite the settings
    if isinstance(clf, LogisticRegression):
        if "multi_class" in clf.get_params():
            is_multi_class = True
    print "CV iterator:", type(outer)
    print "classification problem", type(clf)
    outer_scores = {}   # {fold1: {compute_aggregate_score()}, fold2: {...}}
    outer_f1_scores = []
    number_of_features = 0
    outer_confusion_matrix = np.zeros((len(CLASSES), len(CLASSES)))
    # outer_scores = []
    # Apply nested CV to get an unbiased estimate of our model: all
    # pre-processing, optimization steps etc. must be applied per fold.
    # Otherwise data leakage might occur yielding an optimistic estimate of a
    #  model's performance, e.g. creating BoW representation on all tweets
    # implies no missing tweets in the test set as all words were already
    # considered. This is highly unrealistic -> build BoW in each inner CV loop
    # outer cross-validation
    if do_cv_before_full:
        for fold, (train_idx_outer, test_idx_outer) in enumerate(outer):
            fold += 1
            print "\nFOLD:", fold
            print "--------"
            # Assume fold must be built from scratch
            build_fold = True
            # Path under which the preprocessed dataset for this fold will be
            # stored or loaded from
            train_fold_path = os.path.join(PREBUILT_DATASET, prebuilt + str(
                outer_folds) + "_train" + "_fold_" + str(fold) + ".txt")
            test_fold_path = os.path.join(PREBUILT_DATASET, prebuilt + str(
                outer_folds) + "_test" + "_fold_" + str(fold) + ".txt")
            # Determine if an existing model should be continued to be built
            # If yes, continue from the last fold that exists
            if use_partial:
                # If training set exists for current fold, don't build it from
                # scratch
                if os.path.isfile(train_fold_path):
                    build_fold = False
            # If the fold has to be built from scratch
            if build_fold:
                # Do preprocessing in here to avoid data leakage
                print "pre_params", pre_params
                print "###########"
                print "training instances"
                print train_idx_outer
                print "-----------"
                print "test instances"
                print test_idx_outer
                print "###########"
                # Compute training set for fold from scratch
                if not use_precomputed:
                    train_outer, train_outer_alt, vec, vec_alt = \
                        preprocess_tweets(tweets,
                                          pre_params,
                                          selected_tweets=train_idx_outer,
                                          use_cpd=use_cpd,
                                          cpd_threshold=cpd_threshold
                                          )
                    print "Training instances:", len(train_outer)
                    # print "indices of tweets in test set", train_idx_outer
                    # for i in train_idx_outer:
                    #     print tweets[i].tweet_id
                    print "elements in test:", test_idx_outer.shape[0]
                    # print "indices of tweets in test set", test_idx_outer
                    # for i in test_idx_outer:
                    #     print tweets[i].tweet_id
                    # TODO: pass in removed_words
                    test_outer, test_outer_alt, vec, vec_alt = \
                        preprocess_tweets(tweets,
                                          pre_params,
                                          selected_tweets=test_idx_outer,
                                          vectorizer=vec,
                                          vectorizer_alt=vec_alt,
                                          )
                    # Store existing preprocessed training/test dataset per fold
                    with open(train_fold_path,
                              "wb") as f:
                        cPickle.dump(train_outer, f)
                    with open(test_fold_path, "wb") as f:
                        cPickle.dump(test_outer, f)
                    # Store vectorizer per fold
                    with open(os.path.join(PREBUILT_DATASET, prebuilt + str(
                        outer_folds) + "_bow_vectorizer" + "_fold_" + str(fold) +
                            ".txt"), "wb") as f:
                        cPickle.dump(vec, f)
                    # TODO: Write a function that stores the results
                    # Only if additional features were selected
                    if vec_alt is not None:
                        # Store alternative representation as well
                        with open(os.path.join(PREBUILT_DATASET, "alt_" + prebuilt +
                                str(outer_folds) + "_train" + "_fold_" + str(fold) +
                                ".txt"), "wb") as f:
                            cPickle.dump(train_outer_alt, f)
                        with open(os.path.join(PREBUILT_DATASET, "alt_" + prebuilt +
                                str(outer_folds) + "_test" + "_fold_" + str(fold) +
                                ".txt"), "wb") as f:
                            cPickle.dump(test_outer_alt, f)
                        # Store vectorizer per fold
                        with open(os.path.join(PREBUILT_DATASET, "alt_" + prebuilt +
                                str(outer_folds) + "_bow_vectorizer" + "_fold_" +
                                str(fold) + ".txt"), "wb") as f:
                            cPickle.dump(vec_alt, f)
                        # vec = sorted(t_vectorizer.vocabulary.items(),
                        #                       key=operator.itemgetter(1))
                        # print "vocabulary", vec
                else:
                    print "Load existing dataset"
                    # Load existing preprocessed training/test dataset per fold
                    with open(train_fold_path, "rb") as f:
                        train_outer = cPickle.load(f)
                    with open(test_fold_path, "rb") as f:
                        test_outer = cPickle.load(f)
                    with open(os.path.join(PREBUILT_DATASET, prebuilt + str(
                        outer_folds) + "_bow_vectorizer" + "_fold_" + str(fold) +
                            ".txt"), "rb") as f:
                        vec = cPickle.load(f)
                    # vec = sorted(vec.vocabulary.items(), key=operator.itemgetter(1))
                    # print "vocabulary", vec
            else:
                print "Load existing dataset for fold", fold
                # Load existing preprocessed training/test dataset per fold
                with open(train_fold_path, "rb") as f:
                    train_outer = cPickle.load(f)
                with open(test_fold_path, "rb") as f:
                    test_outer = cPickle.load(f)
                with open(os.path.join(PREBUILT_DATASET, prebuilt + str(
                    outer_folds) + "_bow_vectorizer" + "_fold_" + str(fold) +
                        ".txt"), "rb") as f:
                    vec = cPickle.load(f)
                # vec = sorted(vec.vocabulary.items(), key=operator.itemgetter(1))
                # print "vocabulary", vec
            # Select features
            if use_feature_selection:
                # Determine the indices of the features to be removed - it could be
                # indices or feature names
                feature_removal_indices = []
                for feature in removed_features:
                    try:
                        # Feature is given by an index
                        idx = int(feature)
                    except ValueError:
                        # User specified the feature name and not an index
                        idx = vec.vocabulary[feature]
                    feature_removal_indices.append(idx)
                print "feature indices to be removed:", feature_removal_indices
                vec_train = vec
                # Create a copy because it'll be altered in remove_features() and
                # if no copy is used, it'll throw an error/could lead to an
                # invalid state of the vectorizer, but after removing the
                # features, both vectorizers will be identical
                vec_test = copy.deepcopy(vec_train)
                remove_features(train_outer, feature_removal_indices, vec_train)
                remove_features(test_outer, feature_removal_indices, vec_test)
            for t in train_outer:
                print "remaining features: ", t.bow.shape
                break

            # Important: scale features for SVM and logistic regression!
            # Otherwise they don't work well!
            # In multi-class problems clf is either OVR or OVO classifier ->
            # check it's estimator instead
            to_scale = must_values_be_scaled(clf)
            # if isinstance(clf, OneVsOneClassifier) or \
            #         isinstance(clf, OneVsRestClassifier):
            #         if isinstance(clf.get_params()["estimator"], SVC):
            #             to_scale = True
            #             # scaler = StandardScaler()
            #             # X_train_outer = scaler.fit_transform(
            #             #     X=X_train_outer.astype(np.float))
            #             # X_test_outer = scaler.transform(X=X_test_outer.astype(
            #             #     np.float))
            # # Logistic regression has internal multi-class parameter
            # elif isinstance(clf, LogisticRegression):
            #     to_scale = True
            #     # scaler = StandardScaler()
            #     # X_train_outer = scaler.fit_transform(X=X_train_outer.astype(
            #     #     np.float))
            #     # X_test_outer = scaler.transform(X=X_test_outer.astype(np.float))

            # inner_mean_scores = []
            # define explored parameter space.
            # procedure below should be equal to GridSearchCV
            # TODO: let these parameters be set from config file as a dictionary
            # optimize:
            #   name: param1
            #   range: (start, end, step)
            #
            # and then when instantiating the classifier, replace the config
            # parameter with the optimal solution, something like
            # params = old_params
            # params[name] = optimized value
            # clf = get_algorithm(params)
            # tuned_parameter = [1000, 1100, 1200]
            # tuned_parameter = [1000]

            # for param in tuned_parameter:
            #
            #     inner_scores = []
            #
            #     # inner cross-validation
            #     # inner = KFold(len(X_train_outer), n_folds=2, shuffle=True,
            #     #               random_state=seed)
            #     inner = _get_cv_iterator(len(X_train_outer), folds=inner_folds,
            #                              train_size=train_size, seed=seed)
            #     for train_index_inner, test_index_inner in inner:
            #         # split the training data of outer CV
            #         X_train_inner, X_test_inner = \
            #             X_train_outer[train_index_inner], X_train_outer[
            #                 test_index_inner]
            #         y_train_inner, y_test_inner = y_train_outer[train_index_inner],\
            #                                       y_train_outer[test_index_inner]
            #
            #         # fit extremely randomized trees regressor to training data of
            #         # inner CV
            #         # TODO: instantiate new classifier
            #         # clf = RandomForestClassifier(n_estimators=100)
            #         # clf = ensemble.ExtraTreesRegressor(param, n_jobs=-1,
            #         # random_state=1)
            #         clf.fit(X_train_inner, y_train_inner)
            #         # TODO: calculate score for metric
            #         inner_scores.append(clf.score(X_test_inner, y_test_inner))
            #
            #     # calculate mean score for inner folds
            #     inner_mean_scores.append(np.mean(inner_scores))
            #
            # # get maximum score index
            # index, value = max(enumerate(inner_mean_scores),
            #                    key=operator.itemgetter(1))
            #
            # print 'Best parameter of %i fold: %i' % (fold + 1, tuned_parameter[
            #     index])

            # fit the selected model to the training set of outer CV
            # for prediction error estimation
            # TODO: initiate classifier with tuned parameters here
            # clf2 = RandomForestClassifier(n_estimators=tuned_parameter[index])
            # clf2 = ensemble.ExtraTreesRegressor(tuned_parameter[index],
            # n_jobs=-1, random_state=1)

            clf_basic, clf_emo, clf_hash, number_of_features = \
                training_phase(train_outer, clf, vec, to_scale=to_scale)
            # number_of_features = X_train_outer.shape[1]
            # print X_train_outer.shape
            # clf.fit(X_train_outer, y_train_outer)
            # Store classifier per fold
            name = "classifier_basic_" + str(fold) + ".pkl"
            joblib.dump(clf_basic, os.path.join(output_dir, name), compress=9)
            name = "classifier_emo_" + str(fold) + ".pkl"
            joblib.dump(clf_emo, os.path.join(output_dir, name), compress=9)
            name = "classifier_hash_" + str(fold) + ".pkl"
            joblib.dump(clf_hash, os.path.join(output_dir, name), compress=9)
            # This is how you can load the classifier from the file:
            #clf = joblib.load(os.path.join(output_dir, name))
            pred, y_test_outer = testing_phase(test_outer, vec, clf_basic,
                                               clf_emo, clf_hash)
            print "true labels", y_test_outer
            # pred = clf.predict(X_test_outer)
            print "multi-class?", is_multi_class
            current_score = compute_single_score(
                pred, y_test_outer, score, classes=CLASSES,
                is_regression=is_regression, is_multi_class=is_multi_class)
            inner_f1_score = f1_score(y_test_outer, pred, average="micro")
            outer_f1_scores.append(inner_f1_score)
            cm = confusion_matrix(y_test_outer, pred, labels=np.array(CLASSES))
            print "confusion matrix in fold ", fold, ":"
            print cm
            outer_confusion_matrix += cm
            outer_scores[fold] = current_score
        CLASSES = np.array(CLASSES)
        _compute_aggregate_score(outer_scores, score, outer_folds, output_dir,
                                is_multi_class=is_multi_class)
        print "order of classes in confusion matrix", np.array(CLASSES)
        print "confusion matrix"
        print outer_confusion_matrix
        f1_cls_scores, avg_f1 = _calculate_f1(outer_confusion_matrix, np.array(
            CLASSES))
        print "Results of biased F1 score:"
        print "f1 scores per fold"
        print outer_f1_scores
        print np.mean(outer_f1_scores)
        print "Results of unbiased F1 score:"
        for cls_idx, cls in enumerate(CLASSES):
            print "Class {}: {}".format(cls, f1_cls_scores[cls_idx])
        print "Weighted average F1: ", avg_f1
        with open(os.path.join(output_dir, "metrics.txt"), "w") as f:
            f.write("F1 score per class:\n")
            for cls_idx, cls in enumerate(CLASSES):
                f.write("Class {}: {}\n".format(cls, f1_cls_scores[cls_idx]))
            f.write("Average weighted F1: {}".format(avg_f1))
            _write_confusion_matrix(outer_confusion_matrix, CLASSES, f)
            f.write("\nApproximate number of features: " + str(number_of_features))
        # Write #features
        # show the prediction error estimate produced by nested CV
        # print 'Unbiased prediction error: %.2f (+/- %.2f)' % (np.mean(outer_scores),
        #                                            np.std(outer_scores))
        # Build final model now based on all training data because we have
        # estimated its performance above
    if build_full_model:
        print "Now learn model on full training set"
        # Path for storage/loading
        p_train = os.path.join(PREBUILT_DATASET, prebuilt + str(
                    outer_folds) + "_train_full" + ".txt")
        p_train_alt = os.path.join(PREBUILT_DATASET, "alt_" + prebuilt + str(
                    outer_folds) + "_train_full" + ".txt")
        p_vec = os.path.join(PREBUILT_DATASET, prebuilt + str(
                    outer_folds) + "_bow_vectorizer" + "_train_full" + ".txt")
        p_vec_alt = os.path.join(PREBUILT_DATASET, "alt_" + prebuilt + str(
                    outer_folds) + "_bow_vectorizer" + "_train_full" + ".txt")
        p_test_set = os.path.join(PREBUILT_DATASET, prebuilt + str(
                    outer_folds) + "_test_full" + ".txt")
        p_test_set_alt = os.path.join(PREBUILT_DATASET, "alt_" + prebuilt + str(
                    outer_folds) + "_test_full" + ".txt")
        p_submission_set = os.path.join(PREBUILT_DATASET, prebuilt + str(
                    outer_folds) + "_sub_full" + ".txt")
        p_submission_set_alt = os.path.join(PREBUILT_DATASET, "alt_" + prebuilt + str(
                    outer_folds) + "_sub_full" + ".txt")
        # Perform training on full training set if not precomputed
        if not use_precomputed_full:
            data, data_alt, vec, vec_alt = \
                preprocess_tweets(tweets, pre_params, use_cpd=use_cpd,
                                  cpd_threshold=cpd_threshold)
            # # Load and preprocess test set too
            # if use_test_set:
            #     # Open tweets
            #     test_tweets = open_dataset(test_path)
            #     test_data, test_data_alt, _, _ = preprocess_tweets(
            #         test_tweets, pre_params, vectorizer=vec,
            #         vectorizer_alt=vec_alt)
            # Store model
            with open(p_train, "wb") as f:
                cPickle.dump(data, f)
            with open(p_vec, "wb") as f:
                cPickle.dump(vec, f)
            # Store alternative model
            with open(p_train_alt, "wb") as f:
                cPickle.dump(data_alt, f)
            with open(p_vec_alt, "wb") as f:
                cPickle.dump(vec_alt, f)
            # if use_test_set:
            #     with open(p_test_set, "wb") as f:
            #         cPickle.dump(test_data, f)
            #     with open(p_test_set_alt, "wb") as f:
            #         cPickle.dump(test_data_alt, f)
        else:
            print "Load existing full dataset"
            # User could already be using alternative dataset,
            # i.e. preprend "alt_" to the dataset name --> remove one
            # "alt_"
            p_vec_alt = p_vec_alt.replace("alt_alt_", "alt_")
            # TODO: works only for alternative representation. Unclear
            # whether user wants to use alternative representation or not ->
            # remove "alt_" or keep it?
            # p_vec = p_vec.replace("alt_", "")
            # p_train = p_train.replace("alt_", "")
            # Load existing preprocessed training/test dataset per fold
            with open(p_train, "rb") as f:
                data = cPickle.load(f)
            with open(p_vec, "rb") as f:
                vec = cPickle.load(f)
            with open(p_vec_alt, "rb") as f:
                vec_alt = cPickle.load(f)
            print "vec", len(vec.vocabulary)
            print "vec_alt", len(vec_alt.vocabulary)
            print "training data", data[0].bow.shape
            removed_words = {}
            # if hasattr(vec, "removed_words"):
            #     removed_words = removed_words
            # Process test set now and store it
            # test_tweets = open_dataset(test_path)
            # test_data, test_data_alt, _, _ = preprocess_tweets(
            #     test_tweets, pre_params, vectorizer=vec,
            #     vectorizer_alt=vec_alt)
            # Store results
            # with open(p_test_set, "wb") as f:
            #     cPickle.dump(test_data, f)
            # with open(p_test_set_alt, "wb") as f:
            #     cPickle.dump(test_data_alt, f)

        # Select features
        if use_feature_selection:
            print "Feature selection on full training set"
            # Determine the indices of the features to be removed - it could be
            # indices or feature names
            feature_removal_indices = []
            for feature in removed_features:
                try:
                    # Feature is given by an index
                    idx = int(feature)
                except ValueError:
                    # User specified the feature name and not an index
                    idx = vec.vocabulary[feature]
                feature_removal_indices.append(idx)

            print "feature indices to be removed:", feature_removal_indices
            train_vec = copy.deepcopy(vec)
            remove_features(data, feature_removal_indices, train_vec)

            # Storing reduced dataset only for AL experiment for submission
            # red_train = os.path.join(PREBUILT_DATASET, prebuilt + str(
            #         outer_folds) + "_train_reduced" + ".txt")
            # with open(red_train, "wb") as f:
            #     cPickle.dump(data, f)

        # Important: scale features for SVM and logistic regression!
        # Otherwise they don't work well!
        # In multi-class problems clf is either OVR or OVO classifier ->
        # check it's estimator instead
        to_scale = must_values_be_scaled(clf)
        clf_basic, clf_emo, clf_hash, _ = training_phase(data, clf, vec,
                                                         to_scale=to_scale)
        name = "final_classifier_basic_" + ".pkl"
        joblib.dump(clf_basic, os.path.join(output_dir, name), compress=9)
        name = "final_classifier_emo_" + ".pkl"
        joblib.dump(clf_emo, os.path.join(output_dir, name), compress=9)
        name = "final_classifier_hash_" + ".pkl"
        joblib.dump(clf_hash, os.path.join(output_dir, name), compress=9)
        # joblib.load(os.path.join(output_dir, "final_classifier.pkl"))
        # Test full model on separate test set
        if use_test_set:
            # p_train = os.path.join(PREBUILT_DATASET, prebuilt + str(
            #         outer_folds) + "_train_full" + ".txt")
            # p_vec = os.path.join(PREBUILT_DATASET, prebuilt + str(
            #             outer_folds) + "_bow_vectorizer" + "_train_full" + ".txt")
            # p_test_set = os.path.join(PREBUILT_DATASET, prebuilt + str(
            #             outer_folds) + "_test_full" + ".txt")
            # Load precomputed test set
            if use_precomputed_test_set:
                with open(p_vec, "rb") as f:
                    vec = cPickle.load(f)
                # Load either test set or set used for submission
                if is_submission:
                    with open(p_submission_set, "rb") as f:
                        test_data = cPickle.load(f)
                else:
                    with open(p_test_set, "rb") as f:
                        test_data = cPickle.load(f)
            else:
                # Build test set from scratch -> no idea why open_dataset()
                # doesn't work, but it misses some tweets as it can't detect
                # the end of a line, so it adds following tweets as text of
                # previous tweet. This happens only for 23 tweets in the
                # test set of SemEval 2016.
                test_tweets = open_dataset2(test_path)
                test_data, test_data_alt, _, _ = preprocess_tweets(
                    test_tweets, pre_params, vectorizer=vec,
                    vectorizer_alt=vec_alt)
                # User could already be using alternative dataset,
                # i.e. preprend "alt_" to the dataset name --> remove one
                # "alt_"
                # TODO: works only for alternative representation. Unclear
                # whether user wants to use alternative representation or not ->
                # remove "alt_" or keep it?
                # p_test_set = p_test_set.replace("alt_", "")
                p_test_set_alt = p_test_set_alt.replace("alt_alt_", "alt_")
                # p_submission_set = p_submission_set.replace("alt_", "")
                p_submission_set_alt = p_submission_set_alt.replace("alt_alt_",
                                                                    "alt_")

                # Store results
                if is_submission:
                    with open(p_submission_set, "wb") as f:
                        cPickle.dump(test_data, f)
                    with open(p_submission_set_alt, "wb") as f:
                        cPickle.dump(test_data_alt, f)
                else:
                    with open(p_test_set, "wb") as f:
                        cPickle.dump(test_data, f)
                    with open(p_test_set_alt, "wb") as f:
                        cPickle.dump(test_data_alt, f)

            # Important: scale features for SVM and logistic regression!
            # Otherwise they don't work well!
            # In multi-class problems clf is either OVR or OVO classifier ->
            # check it's estimator instead
            to_scale = must_values_be_scaled(clf)

            # Select features
            if use_feature_selection:
                print "Feature selection on full test set"
                # Determine the indices of the features to be removed - it
                # could be indices or feature names
                feature_removal_indices = []
                for feature in removed_features:
                    try:
                        # Feature is given by an index
                        idx = int(feature)
                    except ValueError:
                        # User specified the feature name and not an index
                        idx = vec.vocabulary[feature]
                    feature_removal_indices.append(idx)

                print "feature indices to be removed:", feature_removal_indices
                test_vec = copy.deepcopy(vec)
                remove_features(test_data, feature_removal_indices, test_vec)
                # Storing reduced dataset only for AL experiment for submission
                # red_test = os.path.join(PREBUILT_DATASET, prebuilt + str(
                #     outer_folds) + "_test_reduced" + ".txt")
                # with open(red_test, "wb") as f:
                #     cPickle.dump(test_data, f)

            pred, y_test = testing_phase(test_data, vec, clf_basic, clf_emo,
                                         clf_hash, to_scale=to_scale,
                                         has_no_labels=is_submission)

            # Store results
            if is_submission:
                with open(os.path.join(output_dir, "SteM-A.output"),
                          "w") as f:
                    for id_, y_pred in enumerate(pred):
                        f.write("{}\t{}\n".format(id_ + 1, y_pred))
            else:
                # Compute evaluation statistics
                micro_f1 = f1_score(y_test, pred, average="micro")
                cm = confusion_matrix(y_test, pred, labels=np.array(CLASSES))
                print "confusion matrix on test set:"
                print cm
                print "Micro-averaged F1 (biased):", micro_f1
                f1_cls_scores, avg_f1 = _calculate_f1(cm, np.array(CLASSES))
                print "F1 per class (unbiased):"
                print f1_cls_scores
                print "Weighted F1 (unbiased):", avg_f1
                with open(os.path.join(output_dir, "metrics_test_set.txt"),
                          "w") as f:
                    f.write("F1 score per class:\n")
                    for cls_idx, cls in enumerate(CLASSES):
                        f.write("Class {}: {}\n".format(cls, f1_cls_scores[
                            cls_idx]))
                    f.write("Average weighted F1: {}".format(avg_f1))
                    _write_confusion_matrix(cm, CLASSES, f)
                    f.write("\nApproximate number of features: " + str(
                        number_of_features))


def _calculate_f1(conf_matrix, classes):
    """
    Calculate F1 score with precision and recall based on the confusion
    matrix. This computation is done based on the final confusion matrix for
    binary or multi-class problems.
    F1 score is averaged over the weighted F1 scores per class.

    confusion matrix
    true (y-axis)/predicted (x-axis)
    class    1     2     3     4     5
      1 [[   0.    0.    8.    1.    1.]
      2  [   0.    2.   17.    3.    0.]
      3  [   0.    0.  131.    6.    8.]
      4  [   0.    0.   46.   18.    2.]
      5  [   0.    0.   31.    3.   10.]]

    Parameters
    ----------
    conf_matrix: numpy array [n_classes, n_classes] - symmetric matrix where
    the rows correspond to the ground truth and columns to predicted values.
    classes: numpy array indicating the order of the classes.

    Returns
    -------
    List, float.
    List of F1 scores per class in the same order as <classes>, weighted F1
    score.

    """
    print "dimensions confusion matrix", conf_matrix.shape[0]
    dims = conf_matrix.shape[0]
    TP = {}  # {class1: count, class2: count}
    FP = {}
    FN = {}
    TN = {}
    instances = {}  # = elements in row
    for cls_idx, cls in enumerate(classes):
        for i in range(dims):
            for j in range(dims):
                if i == j and i == cls_idx:
                    TP[cls] = conf_matrix[i][j]
                elif not i == cls_idx and not j == cls_idx:
                    # All other elements having a different i and j are TNs
                    if cls not in TN:
                        TN[cls] = 0
                    TN[cls] += conf_matrix[i][j]
                elif i != j and j == cls_idx:
                    # j-th column represents FPs for class j
                    if cls not in FP:
                        FP[cls] = 0
                    FP[cls] += conf_matrix[i][j]
                elif i != j and i == cls_idx:
                    # i-th row corresponds to FNs of class i
                    if cls not in FN:
                        FN[cls] = 0
                    FN[cls] += conf_matrix[i][j]
                if i == cls_idx:
                    if cls not in instances:
                        instances[cls] = 0
                    instances[cls] += conf_matrix[i][j]
    print "TP", TP
    print "TN", TN
    print "FP", FP
    print "FN", FN
    print "#instances per class", instances
    return _weighted_f1(TP, FP, FN, instances, classes)


def must_values_be_scaled(clf):
    """
    Determines whether a dataset must be scaled or not. This depends on the
    classifier to be used, e.g. it's crucial for SVMs and Logistic Regression.

    Parameters
    ----------
    clf: Classifier object.

    Returns
    -------
    boolean.
    True if dataset must be scaled, otherwise False.

    """
    to_scale = False
    if isinstance(clf, OneVsOneClassifier) or \
                isinstance(clf, OneVsRestClassifier):
                if isinstance(clf.get_params()["estimator"], SVC):
                    to_scale = True
    # Logistic regression has internal multi-class parameter
    elif isinstance(clf, LogisticRegression):
        to_scale = True
    return to_scale


def training_phase(train, clf, vec, to_scale=False):
    """
    Trains the classifiers with the respective datasets.
    Also makes sure that all separate classifiers are trained for the
    specific feature spaces.

    train: List of Tweet objects that comprise the training set.
    clf: Scikit classifier object.
    vec: BoW vectorizer object.
    to_scale: If True, <train> and <test> will be normalized with unit
    variance, otherwise the input data will be left unaltered.

    Returns
    -------
    classifier, classifier, classifier, int.
    The three classifier objects trained on the training set and the three
    sets of labels from the three training sets (represented as numpy.array).
    The last value is the number of features in the training set.

    """
    clf_emo = copy.deepcopy(clf)
    clf_hash = copy.deepcopy(clf)
    train_basic, train_emo, train_hash, _ = _get_data_for_feature_spaces(
        train)

    # Now remove the respective unnecessary features
    # Features to remove
    emos = ["pos_emoticons", "neg_emoticons", "elongated", "upper"]
    hashtags = ["has_hashtag", "neg_hash", "obj_hash", "pos_hash",
                "negative_words_hash",
                "objective_words_hash", "positive_words_hash",
                "polarity_words_hash", "negation_words_hash",
                "negative_words_sum_hash", "objective_words_sum_hash",
                "positive_words_sum_hash"]
    # Get the indices to be deleted
    emos_idx = []
    hashtags_idx = []
    tmp = copy.deepcopy(vec)
    tmp = sorted(tmp.vocabulary.items(), key=operator.itemgetter(1))
    print "vocabulary", len(tmp), tmp
    for feature in emos:
        # It's a custom feature, so we need to prefix it
        name = "$$$_" + feature
        # It's possible that some of the custom features were already removed
        if name in vec.vocabulary:
            emos_idx.append(vec.vocabulary[name])
    for feature in hashtags:
        # It's a custom feature, so we need to prefix it
        name = "$$$_" + feature
        # It's possible that some of the custom features were already removed
        if name in vec.vocabulary:
            hashtags_idx.append(vec.vocabulary[name])
    basic_idx = emos_idx + hashtags_idx
    # Don't directly change vec as it might be needed somewhere else
    basic_vec = copy.deepcopy(vec)
    emo_vec = copy.deepcopy(vec)
    hash_vec = copy.deepcopy(vec)
    # Delete emoticon- and hashtag-related features
    remove_features(train_basic, basic_idx, basic_vec)
    # Delete hashtag-related features
    remove_features(train_emo, hashtags_idx, emo_vec)
    # Delete emoticon-related features
    remove_features(train_hash, emos_idx, hash_vec)
    # print "Dimensions basic feature space:", train_basic[0].bow.shape
    # if len(train_emo) > 0:
    #     print "Dimensions emoticon-related feature space:", train_emo[
    #         0].bow.shape
    # if len(train_hash) > 0:
    #     print "Dimensions hashtag-related feature space:", train_hash[
    #         0].bow.shape

    # To make computations more efficient, let's turn the list into a numpy
    # array and separate the X and y explicitly
    X_train_basic, y_train_basic = _get_bow_data(train_basic)
    X_train_emo, y_train_emo = _get_bow_data(train_emo)
    X_train_hash, y_train_hash = _get_bow_data(train_hash)

    # Scale the data if necessary
    X_train_basic = _scale_values(X_train_basic, to_scale)
    X_train_emo = _scale_values(X_train_emo, to_scale)
    X_train_hash = _scale_values(X_train_hash, to_scale)

    # Learn the models
    clf.fit(X_train_basic, y_train_basic)
    if len(y_train_emo) > 0:
        clf_emo.fit(X_train_emo, y_train_emo)
    if len(y_train_hash) > 0:
        clf_hash.fit(X_train_hash, y_train_hash)
    return clf, clf_emo, clf_hash, X_train_basic.shape[1]


def testing_phase(test, vec, clf_basic, clf_emo, clf_hash, to_scale=False,
                  has_no_labels=False):
    """
    Predicts labels of unknown test instances using an appropriate classifier.

    Parameters
    ----------
    test: List of Tweet objects used for the test set.
    vec: BoW vectorizer object.
    clf_basic: Classifier object for the basic feature space.
    clf_emo: Classifier object for the basic feature space + emoticons.
    clf_hash: Classifier object for the basic feature space + hashtags.
    to_scale: If True, <train> and <test> will be normalized with unit
    variance, otherwise the input data will be left unaltered.
    has_no_labels: True if no labels in the test set exist.

    Returns
    -------
    List, list.
    List of predictions for the Tweet objects and list of true labels.

    """
    true_labels = []
    if not has_no_labels:
        true_labels = [t.label for t in test]
    # Build 3 different classifiers on 3 different feature subsets

    # Predicted labels for tweets following the order of <test> and
    # <true_labels>
    preds = []
    test_basic, test_emo, test_hash, order = _get_data_for_feature_spaces(
        test)

    # Now remove the respective unnecessary features
    # Features to remove
    emos = ["pos_emoticons", "neg_emoticons", "elongated", "upper"]
    hashtags = ["has_hashtag", "neg_hash", "obj_hash", "pos_hash",
                "negative_words_hash",
                "objective_words_hash", "positive_words_hash",
                "polarity_words_hash", "negation_words_hash",
                "negative_words_sum_hash", "objective_words_sum_hash",
                "positive_words_sum_hash"]
    # Get the indices to be deleted
    emos_idx = []
    hashtags_idx = []
    for feature in emos:
        # It's a custom feature, so we need to prefix it
        name = "$$$_" + feature
        # It's possible that some of the custom features were already removed
        if name in vec.vocabulary:
            emos_idx.append(vec.vocabulary[name])
    for feature in hashtags:
        # It's a custom feature, so we need to prefix it
        name = "$$$_" + feature
        # It's possible that some of the custom features were already removed
        if name in vec.vocabulary:
            hashtags_idx.append(vec.vocabulary[name])
    basic_idx = emos_idx + hashtags_idx
    # Don't directly change vec as it could be needed for other purposes
    basic_vec = copy.deepcopy(vec)
    emo_vec = copy.deepcopy(vec)
    hash_vec = copy.deepcopy(vec)
    # Delete emoticon- and hashtag-related features
    remove_features(test_basic, basic_idx, basic_vec)
    # Delete hashtag-related features
    remove_features(test_emo, hashtags_idx, emo_vec)
    # Delete emoticon-related features
    remove_features(test_hash, emos_idx, hash_vec)
    # print "Dimensions basic feature space:", test_basic[0].bow.shape
    # if len(test_emo) > 0:
    #     print "Dimensions emoticon-related feature space:", test_emo[
    #         0].bow.shape
    # if len(test_hash) > 0:
    #     print "Dimensions hashtag-related feature space:", test_hash[
    #         0].bow.shape

    # To make computations more efficient, let's turn the list into a numpy
    # array and separate the X and y explicitly
    X_test_basic, y_test_basic = _get_bow_data(test_basic, has_no_labels)
    X_test_emo, y_testn_emo = _get_bow_data(test_emo, has_no_labels)
    X_test_hash, y_test_hash = _get_bow_data(test_hash, has_no_labels)

    # Scale the data if necessary
    X_test_basic = _scale_values(X_test_basic, to_scale)
    X_test_emo = _scale_values(X_test_emo, to_scale)
    X_test_hash = _scale_values(X_test_hash, to_scale)

    # Predict the labels
    print X_test_basic.shape
    basic_preds = clf_basic.predict(X_test_basic)
    if len(X_test_emo) > 0:
        emo_preds = clf_emo.predict(X_test_emo)
    if len(X_test_hash) > 0:
        hash_preds = clf_hash.predict(X_test_hash)

    # Counters for the three predictions indicating how many elements have
    # been read from the respective list before.
    basic_idx = 0
    emo_idx = 0
    hash_idx = 0
    # Merge the predictions from the 3 different feature spaces in the same
    # order as the tweets in the test set
    # print "basic has {} instances".format(len(basic_preds))
    # if len(X_test_emo) > 0:
    #     print "emo has {} instances".format(len(emo_preds))
    # print "hash has {} instances".format(len(hash_preds))
    for idx, list_no in enumerate(order):
        # Basic feature space
        if list_no == 0:
            preds.append(basic_preds[basic_idx])
            basic_idx += 1
        # Basic feature space + emoticons
        if list_no == 1:
            preds.append(emo_preds[emo_idx])
            emo_idx += 1
        # Basic feature space + hashtags
        if list_no == 2:
            preds.append(hash_preds[hash_idx])
            hash_idx += 1

    return preds, true_labels


def _get_data_for_feature_spaces(data):
    """
    Assigns each tweet to one of the feature spaces.

    Parameters
    ----------
    data: List of Tweet objects which should be assigned.

    Returns
    -------
    List, list, list, list.
    Lists of the respective 3 feature spaces (basic, basic + emoticons,
    basic + hashtags). Each list contains the respective Tweet objects. If a
    tweet contains hashtags and emoticons, we give hashtags a higher
    priority. The fourth list contains the indices in which of the 3 lists a
    tweet was stored. 0 indicates basic, 1 refers to emoticons, and 2 refers
    to hashtags. This list is necessary for evaluating predictions.

    """
    # Create the different feature spaces:
    # 1. Basic
    basics = []
    # 2. Basic + emoticons
    emos = []
    # 3. Basic + hashtags
    hashtags = []
    # Mapping for evaluating predictions later.
    order = []
    # How many tweets exist that contain emoticons and hashtags?
    both = 0
    for tweet in data:
        has_hash = True if tweet.has_hashtag > 0 else False
        has_emo = True if tweet.pos_emoticons > 0 or tweet.neg_emoticons > 0 \
            else False
        # Hashtag exists
        if has_hash:
            hashtags.append(tweet)
            # has_hash = True
        # Emoticon exists
        # for emo in EMOTICONS:
            # Only store if tweet doesn't contain a hashtag, but an emoticon
            # if emo in tweet.text and not has_hash:
            #     print "{} contains emojis, namely {}".format(tweet.text, emo)
            #     emos.append(tweet)
                # Only 1 of the 2 booleans can be true
                # if has_hash:
                #     both += 1
                # else:
                #     has_emo = True
                # break
        if has_emo and not has_hash:
            emos.append(tweet)
        if has_emo and has_hash:
            both += 1
        # If tweet doesn't contain any emoticons and hashtags
        if not has_emo and not has_hash:
            basics.append(tweet)
        list_no = 0
        if has_hash:
            list_no = 2
        if has_emo and not has_hash:
            list_no = 1
        order.append(list_no)
    print "{}/{} tweets ({}%) contain hashtags and emoticons".format(
        both, len(data), 100.0*both/len(data))
    return basics, emos, hashtags, order


def _get_bow_data(data, has_no_labels=False):
    """
    Given a list of tweets, the function determins the respective BoW
    representation and the labels.

    Parameters
    ----------
    data: List of Tweet objects.
    has_no_labels: True, if Tweet has no labels.

    Returns
    -------
    List, list.
    List of BoW representations per tweet as numpy arrays, list of labels of
    the tweets (same order as in the first list) as numpy arrays.

    """
    y = []
    X = [t.bow for t in data]
    if not has_no_labels:
        y = [t.label for t in data]
    return np.array(X), np.array(y)


def _scale_values(data, to_scale):
    """
    Transforms the values by normalizing them with a unit variance of 1.

    Parameters
    ----------
    data: Numpy array containing the representation of a tweet.
    to_scale: True, if the data should be scaled.

    Returns
    -------
    numpy.ndarray.
    Same data, but normalized.

    """
    if to_scale:
        scaler = StandardScaler()
        data = scaler.fit_transform(X=data.astype(
            np.float))
    return data


def _write_confusion_matrix(conf_matrix, classes, f):
    """
    Writes the confusion matrix in a file in human-readable format.

    confusion matrix
    true (y-axis)/predicted (x-axis)
    class    1     2     3     4     5
      1 [[   0.    0.    8.    1.    1.]
      2  [   0.    2.   17.    3.    0.]
      3  [   0.    0.  131.    6.    8.]
      4  [   0.    0.   46.   18.    2.]
      5  [   0.    0.   31.    3.   10.]]

    Parameters
    ----------
    conf_matrix: numpy array [n_classes, n_classes] - symmetric matrix where
    the rows correspond to the ground truth and columns to predicted values.
    classes: numpy array indicating the order of the classes.
    f: Open file to which the matrix should be written.

    """
    # Space for numbers in cells -> if number > longest cell, reserve this
    # much space for table entries
    space_for_numbers = 5
    dims = conf_matrix.shape[0]
    longest = 0
    # Number of letters in all classes
    chars = 0
    # Find width to be used for tables
    for c in classes:
        if len(c) > longest:
            longest = len(c)
            chars += len(c)
    # Number of whitespaces
    width = max([longest, space_for_numbers])
    # Add extra whitespaces per column
    chars += 2 * len(classes)
    # Add also extra whitespaces in case numbers are longer than class names
    # Add 1 extra column as first header is empty
    line_width = max([width, space_for_numbers]) * len(classes) + chars + width
    # Header
    f.write("\nConfusion matrix: (rows: truth, columns: predicted)\n")
    # Predicted class names header -> let first column be empty
    f.write("{:<{width}}".format("", width=(width + 2)))
    for i in xrange(dims):
        f.write(" | {:<{width}}".format(classes[i], width=width))
    f.write("\n")
    f.write("-"*line_width + "\n")
    # f.write("{:<{width}}".format(width=width))
    for i in xrange(dims):
        # True class name
        f.write("| {:<{width}} | ".format(classes[i], width=width))
        # All values
        for j in xrange(dims):
            row = "{:<{width}} | ".format(conf_matrix[i][j], width=width)
            f.write(row)
        f.write("\n")


def _f1(TP, FP, FN):
    """
    Computes F1 score based on TPs, FPs and FNs.
    """
    return 2. * TP / (2 * TP + FN + FP)


def _weighted_f1(TP, FP, FN, instances, classes):
    """
    Computes the weighted average F1 score given TPs, FPs, FNs and number of
    instances per class.

    Parameters
    ----------
    TP: Dictionary containing true positives per class.
    FP: Dictionary containing false positives per class.
    FN: Dictionary containing false negatives per class.
    instances: Dictionary containing the number of instances per class.
    classes: Classes to be used in the dataset. Order of the classes
    determines order of the F1 scores.

    Returns
    -------
    List, float.
    List of F1 scores per class in the same order as <classes>, weighted F1
    score.

    """
    total_instances = sum(instances.values())
    print "total number of instances", total_instances
    avg_f1 = 0
    f1_scores = []
    for cls in classes:
        cls_weight = 1. * instances[cls] / total_instances
        print "weight of class", cls, ":", cls_weight
        f1 = _f1(TP[cls], FP[cls], FN[cls])
        f1_scores.append(f1)
        avg_f1 += f1 * cls_weight
    return f1_scores, avg_f1


def _run_split_evaluation(tweets, clf, score, pre_params, output_dir,
                          train_size=0.8, seed=42, shuffle=True):
    """
    Runs evaluation by splitting the dataset into a train and test set
    without applying CV.

    Parameters
    ----------
    tweets: Dataset to be used. In our case the tweet texts in BoW
            representation.
    clf: Scikit classifier object used in CV.
    score: Score to be used for evaluating the suitability of a given model.
    pre_params: Parameters used for pre-processing.
    output_dir: Base directory in which the results will be stored.
    train_size: Amount of data to be used for training. This is only relevant if
                "shuffle" for cv_type is selected. Parameter will be ignored
                otherwise.
    seed: Seed for PRNG to allow reproducibility of results.
    shuffle: Dataset will be shuffled prior to creating folds if True. If
             False, dataset will be split as is.

    Returns
    -------

    """
    # metric = _get_score(pred, true, score, is_multi_class=is_multi_class)
    X = [t.bow for t in tweets]
    y = [t.label for t in tweets]
    X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                        train_size=train_size)
    clf.fit(X_train, y_train)
    predicted = clf.predict(X_test)
    # TODO: compute scores here
    score = clf.score(y_test, predicted)
    return score


def _get_cv_iterator(inst, cv_type="k", folds=10,
                     train_size=0.8, seed=42, shuffle=True):
    """
    Creates a CV iterator according to the given parameters.

    Parameters
    ----------
    inst: Number of instances in the dataset.
    cv_type: String to specify which type of CV to use. Valid values are:
             "shuffle" for random permutations CV, "k" for simple k-fold CV,
             "bootstrap" for bootstrapped CV or
             "stratified" for stratified CV. Default = "k".
    folds: Number of folds in CV.
    train_size: Amount of data to be used for training. This is only relevant if
                "shuffle" for cv_type is selected. Parameter will be ignored
                otherwise.
    seed: Seed for PRNG to allow reproducibility of results.
    shuffle: Dataset will be shuffled prior to creating folds if True. If
             False, dataset will be split as is

    Returns
    -------
    CV iterator of the specified type. If an error occurred or some
    parameters were missing, None is returned.

    """
    seed = int(seed)
    if cv_type == "stratified":
        outer = StratifiedKFold(
            inst,
            n_folds=folds,
            shuffle=shuffle,
            random_state=seed,
        )
    elif cv_type == "shuffle":
        test_size = 1 - train_size
        outer = ShuffleSplit(
            inst,
            n_iter=folds,
            train_size=train_size,
            test_size=test_size,
            random_state=seed,
        )
    # elif cv_type == "bootstrap":
    #     test_size = 1 - train_size
    #     outer = Bootstrap(
    #         inst,
    #         n_iter=folds,
    #         train_size=train_size,
    #         test_size=test_size,
    #         random_state=seed,
    #     )
    else:
        outer = KFold(
            inst,
            shuffle=shuffle,
            n_folds=folds,
            random_state=seed,
        )
    return outer


def compute_single_score(pred, true, metric, classes=[-1, 1],
                         is_regression=True, is_multi_class=False):
    """
    Computes how well a model performed in predicting labels for a single fold.

    Parameters
    ----------
    pred: List of predicted labels.
    true: List of true labels.
    metric: Name of a metric to compute score.
    classes: List of classes in the dataset.
    is_regression: True if labels are numbers. Otherwise it's treated as a
                   classification problem.
    is_multi_class: If True, score is computed fo multi-class problem and not
                    a binary one.

    Returns
    -------
    Score given the predictions.

    """
    if metric == "roc":
        return _compute_roc_auc(pred, true, classes=classes,
                                is_regression=is_regression,
                                is_multi_class=is_multi_class)
    return None


def _compute_roc_auc_regression(pred, true, classes=[-1, 1],
                              is_multi_class=False):
    """
    Computes ROC if the 3 classes 'positive', 'neutral' and 'negative' are
    given as a regression problem.

    Parameters
    ----------
    pred: List of predicted labels.
    true: List of true labels.
    classes: List of classes in dataset.
    is_multi_class: If True, score is computed fo multi-class problem and not
                    a binary one.

    Returns
    -------
    dict.
    Dictionary containing the results per class: once for "default"
    computation, once for "micro" aggregation. For both the FPR, FNR and AUC
    are stored as separate entries, e.g.
    {"default":
        {"positive":
            { "fpr": 1,
              "tpr": 0.4,
              "auc": 0.6,
            },
        "negative":
            { "fpr": 1,
              "tpr": 0.4,
              "auc": 0.6,
            }
        },
        "micro":
            { "fpr": 0.5,
              "tpr": 0.5,
              "auc": 0.7,
            }
        ,
        "negative": ...
    }

    """
    if is_multi_class:
        print "calculate ROC multi regression"
        # http://scikit-learn.org/stable/auto_examples/model_selection/
        # plot_roc.html
        # Compute ROC curve and ROC area for each class
        result = {
            "micro": {},
            "default": {}
        }
        # Make sure that in binarization the mapping of the labels is correct
        #  to map the right results to each class
        # map_str2i = {"vneg": "1", "pneg": "2", "negative": "3", "neutral": "4",
        #              "positive": "5"}
        # map_i2str = {"1": "vneg", "2": "pneg", "3": "negative",
        #              "4": "neutral", "5": "positive"}
        map_str2i = {}
        map_i2str = {}
        labels = set(true)
        labels = sorted(list(labels))
        print labels
        for no, l in enumerate(labels):
            map_str2i[str(l)] = str(no + 1)
            map_i2str[str(no + 1)] = str(l)
        print "Truth", true
        print "str2i", map_str2i
        print "i2str", map_i2str
        # labels = [l for l in map_str2i.iterkeys()]
        print "labels", labels
        true = label_binarize(true, classes=labels)
        # print "Truth binarized", true
        pred = label_binarize(pred, classes=labels)
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        # true = np.array(true)
        # pred = np.array(pred)
        print "classes", classes
        # TODO: can <classes> be removed???
        for i, cls in enumerate(labels):
            print type(cls), cls, cls in map_i2str[cls]
            result["default"][map_i2str[cls]] = {}
            result["default"][map_i2str[cls]]["fpr"], result["default"][
                map_i2str[cls]]["tpr"], _ = roc_curve(true[:, i], pred[:, i])
            result["default"][map_i2str[cls]]["auc"] = \
                auc(result["default"][map_i2str[cls]]["fpr"], result[
                    "default"][map_i2str[cls]]["tpr"])
        # Compute micro-averaged ROC curve and ROC area
        # fpr["micro"], tpr["micro"], _ = roc_curve(true.ravel(), pred.ravel())
        # roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        # result["micro"] = {}
        # result["micro"]["fpr"], result["micro"]["tpr"], _ = roc_curve(
        #     true.ravel(), pred.ravel())
        # result["micro"]["auc"] = auc(result["micro"]["fpr"], result["micro"][
        #     "tpr"])
        print "fpr", fpr
        print "tpr", tpr
        print "auc", roc_auc
        return result
    else:
        print "calculate ROC binary"
        print pred
        # http://scikit-learn.org/stable/auto_examples/model_selection/
        # plot_roc.html
        # Compute ROC curve and ROC area for each class
        result = {
            "micro": {},
            "default": {}
        }
        # Make sure that in binarization the mapping of the labels is correct
        #  to map the right results to each class
        existing_labels = sorted(classes)
        print "labels", existing_labels
        print "largest one", existing_labels[-1]
        print "Truth", true
        true = np.array(true)
        pred = np.array(pred)
        print "classes", classes
        print "true", true.shape
        print "pred", pred.shape
        result["default"] = {}
        print "shape", true.shape, pred.shape, type(existing_labels[-1]),
        # Choose the largest label as positive class
        result["default"]["fpr"], result["default"]["tpr"], _ = roc_curve(
            true, pred, pos_label=int(existing_labels[-1]))
        result["default"]["auc"] = \
            auc(result["default"]["fpr"], result["default"]["tpr"])
        # Compute micro-averaged ROC curve and ROC area
        # fpr["micro"], tpr["micro"], _ = roc_curve(true.ravel(), pred.ravel())
        # roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        # result["micro"] = {}
        # result["micro"]["fpr"], result["micro"]["tpr"], _ = roc_curve(
        #     true.ravel(), pred.ravel(), pos_label=int(existing_labels[-1]))
        # result["micro"]["auc"] = auc(result["micro"]["fpr"], result["micro"][
        #     "tpr"])
        # print "fpr", fpr
        # print "tpr", tpr
        # print "auc", roc_auc
        return result


def _compute_roc_auc_classification(pred, true, classes=[-1, 1],
                                    is_multi_class=False):
    """
    Computes ROC.

    Parameters
    ----------
    pred: List of predicted labels.
    true: List of true labels.
    classes: List of classes in dataset.
    is_multi_class: If True, score is computed fo multi-class problem and not
                    a binary one.

    Returns
    -------
    dict.
    Dictionary containing the results per class: once for "default"
    computation, once for "micro" aggregation. For both the FPR, FNR and AUC
    are stored as separate entries, e.g.
    {"default":
        {"positive":
            { "fpr": 1,
              "tpr": 0.4,
              "auc": 0.6,
            },
        "negative":
            { "fpr": 1,
              "tpr": 0.4,
              "auc": 0.6,
            }
        },
        "micro":
            { "fpr": 0.5,
              "tpr": 0.5,
              "auc": 0.7,
            }
        ,
        "negative": ...
    }

    """
    if is_multi_class:
        print "calculate ROC multi classification"
        # http://scikit-learn.org/stable/auto_examples/model_selection/
        # plot_roc.html
        # Compute ROC curve and ROC area for each class
        result = {
            "micro": {},
            "default": {}
        }
        # Make sure that in binarization the mapping of the labels is correct
        #  to map the right results to each class
        # map_str2i = {"vneg": "1", "pneg": "2", "negative": "3", "neutral": "4",
        #              "positive": "5"}
        # map_i2str = {"1": "vneg", "2": "pneg", "3": "negative",
        #              "4": "neutral", "5": "positive"}
        map_str2i = {}
        map_i2str = {}
        labels = set(true)
        labels = sorted(list(labels))
        # print labels
        for no, l in enumerate(labels):
            map_str2i[str(l)] = str(no + 1)
            map_i2str[str(no + 1)] = str(l)
        # print "Truth", true
        # print "str2i", map_str2i
        # print "i2str", map_i2str
        # labels = [l for l in map_str2i.iterkeys()]
        # print "labels", labels
        true = label_binarize(true, classes=labels)
        # print "Truth binarized", true
        pred = label_binarize(pred, classes=labels)
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        # true = np.array(true)
        # pred = np.array(pred)
        # print "classes", classes
        # TODO: can <classes> be removed???
        for i, cls in enumerate(labels):
            # print type(cls), cls, cls in map_str2i[cls]
            result["default"][map_str2i[cls]] = {}
            result["default"][map_str2i[cls]]["fpr"], result["default"][
                map_str2i[cls]]["tpr"], _ = roc_curve(true[:, i], pred[:, i])
            result["default"][map_str2i[cls]]["auc"] = \
                auc(result["default"][map_str2i[cls]]["fpr"], result[
                    "default"][map_str2i[cls]]["tpr"])
        # Compute micro-averaged ROC curve and ROC area
        # fpr["micro"], tpr["micro"], _ = roc_curve(true.ravel(), pred.ravel())
        # roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        # result["micro"] = {}
        # result["micro"]["fpr"], result["micro"]["tpr"], _ = roc_curve(
        #     true.ravel(), pred.ravel())
        # result["micro"]["auc"] = auc(result["micro"]["fpr"], result["micro"][
        #     "tpr"])
        # print "fpr", fpr
        # print "tpr", tpr
        # print "auc", roc_auc
        return result
    else:
        print "calculate ROC binary"
        print pred
        # http://scikit-learn.org/stable/auto_examples/model_selection/
        # plot_roc.html
        # Compute ROC curve and ROC area for each class
        result = {
            "micro": {},
            "default": {}
        }
        # Make sure that in binarization the mapping of the labels is correct
        #  to map the right results to each class
        existing_labels = sorted(classes)
        print "labels", existing_labels
        print "largest one", existing_labels[-1]
        print "Truth", true
        true = np.array(true)
        pred = np.array(pred)
        print "classes", classes
        print "true", true.shape
        print "pred", pred.shape
        result["default"] = {}
        print "shape", true.shape, pred.shape, type(existing_labels[-1]),
        # Choose the largest label as positive class
        result["default"]["fpr"], result["default"]["tpr"], _ = roc_curve(
            true, pred, pos_label=int(existing_labels[-1]))
        result["default"]["auc"] = \
            auc(result["default"]["fpr"], result["default"]["tpr"])
        # Compute micro-averaged ROC curve and ROC area
        # fpr["micro"], tpr["micro"], _ = roc_curve(true.ravel(), pred.ravel())
        # roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        # result["micro"] = {}
        # result["micro"]["fpr"], result["micro"]["tpr"], _ = roc_curve(
        #     true.ravel(), pred.ravel(), pos_label=int(existing_labels[-1]))
        # result["micro"]["auc"] = auc(result["micro"]["fpr"], result["micro"][
        #     "tpr"])
        # print "fpr", fpr
        # print "tpr", tpr
        # print "auc", roc_auc
        return result


def _compute_roc_auc(pred, true, classes=[-1, 1],
                     is_regression=True, is_multi_class=False):
    """
    Computes ROC.

    Parameters
    ----------
    pred: List of predicted labels.
    true: List of true labels.
    classes: List of classes in dataset.
    is_regression: True if labels are numbers. Otherwise it's treated as a
                   classification problem.
    is_multi_class: If True, score is computed fo multi-class problem and not
                    a binary one.

    Returns
    -------
    dict.
    Dictionary containing the results per class: once for "default"
    computation, once for "micro" aggregation. For both the FPR, FNR and AUC
    are stored as separate entries, e.g.
    {"default":
        {"positive":
            { "fpr": 1,
              "tpr": 0.4,
              "auc": 0.6,
            },
        "negative":
            { "fpr": 1,
              "tpr": 0.4,
              "auc": 0.6,
            }
        },
        "micro":
            { "fpr": 0.5,
              "tpr": 0.5,
              "auc": 0.7,
            }
        ,
        "negative": ...
    }

    """
    # if is_multi_class:
    #     print "calculate ROC multi"
    #     # http://scikit-learn.org/stable/auto_examples/model_selection/
    #     # plot_roc.html
    #     # Compute ROC curve and ROC area for each class
    #     result = {
    #         "micro": {},
    #         "default": {}
    #     }
    #     # Make sure that in binarization the mapping of the labels is correct
    #     #  to map the right results to each class
    #     # map_str2i = {"vneg": "1", "pneg": "2", "negative": "3", "neutral": "4",
    #     #              "positive": "5"}
    #     # map_i2str = {"1": "vneg", "2": "pneg", "3": "negative",
    #     #              "4": "neutral", "5": "positive"}
    if is_regression:
        return _compute_roc_auc_regression(pred, true, classes,
                                         is_multi_class)
    else:
        return _compute_roc_auc_classification(pred, true, classes,
                                         is_multi_class)
    #     map_str2i = {}
    #     map_i2str = {}
    #     labels = set(true)
    #     labels = sorted(list(labels))
    #     print labels
    #     for no, l in enumerate(labels):
    #         map_str2i[str(l)] = str(no + 1)
    #         map_i2str[str(no + 1)] = str(l)
    #     print "Truth", true
    #     print "str2i", map_str2i
    #     print "i2str", map_i2str
    #     # labels = [l for l in map_str2i.iterkeys()]
    #     print "labels", labels
    #     true = label_binarize(true, classes=labels)
    #     # print "Truth binarized", true
    #     pred = label_binarize(pred, classes=labels)
    #     fpr = dict()
    #     tpr = dict()
    #     roc_auc = dict()
    #     # true = np.array(true)
    #     # pred = np.array(pred)
    #     print "classes", classes
    #     # TODO: can <classes> be removed???
    #     for i, cls in enumerate(labels):
    #         print type(cls), cls, cls in map_i2str[cls]
    #         result["default"][map_i2str[cls]] = {}
    #         result["default"][map_i2str[cls]]["fpr"], result["default"][
    #             map_i2str[cls]]["tpr"], _ = roc_curve(true[:, i], pred[:, i])
    #         result["default"][map_i2str[cls]]["auc"] = \
    #             auc(result["default"][map_i2str[cls]]["fpr"], result[
    #                 "default"][map_i2str[cls]]["tpr"])
    #     # Compute micro-averaged ROC curve and ROC area
    #     # fpr["micro"], tpr["micro"], _ = roc_curve(true.ravel(), pred.ravel())
    #     # roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    #     # result["micro"] = {}
    #     # result["micro"]["fpr"], result["micro"]["tpr"], _ = roc_curve(
    #     #     true.ravel(), pred.ravel())
    #     # result["micro"]["auc"] = auc(result["micro"]["fpr"], result["micro"][
    #     #     "tpr"])
    #     print "fpr", fpr
    #     print "tpr", tpr
    #     print "auc", roc_auc
    #     return result
    # else:
    #     print "calculate ROC binary"
    #     print pred
    #     # http://scikit-learn.org/stable/auto_examples/model_selection/
    #     # plot_roc.html
    #     # Compute ROC curve and ROC area for each class
    #     result = {
    #         "micro": {},
    #         "default": {}
    #     }
    #     # Make sure that in binarization the mapping of the labels is correct
    #     #  to map the right results to each class
    #     existing_labels = sorted(classes)
    #     print "labels", existing_labels
    #     print "largest one", existing_labels[-1]
    #     print "Truth", true
    #     true = np.array(true)
    #     pred = np.array(pred)
    #     print "classes", classes
    #     print "true", true.shape
    #     print "pred", pred.shape
    #     result["default"] = {}
    #     print "shape", true.shape, pred.shape, type(existing_labels[-1]),
    #     # Choose the largest label as positive class
    #     result["default"]["fpr"], result["default"]["tpr"], _ = roc_curve(
    #         true, pred, pos_label=int(existing_labels[-1]))
    #     result["default"]["auc"] = \
    #         auc(result["default"]["fpr"], result["default"]["tpr"])
    #     # Compute micro-averaged ROC curve and ROC area
    #     # fpr["micro"], tpr["micro"], _ = roc_curve(true.ravel(), pred.ravel())
    #     # roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    #     # result["micro"] = {}
    #     # result["micro"]["fpr"], result["micro"]["tpr"], _ = roc_curve(
    #     #     true.ravel(), pred.ravel(), pos_label=int(existing_labels[-1]))
    #     # result["micro"]["auc"] = auc(result["micro"]["fpr"], result["micro"][
    #     #     "tpr"])
    #     # print "fpr", fpr
    #     # print "tpr", tpr
    #     # print "auc", roc_auc
    #     return result


def _compute_aggregate_score(data, metric, folds, output_dir,
                             is_multi_class=False):
    """

    Computes the overall score of a classifier.

    Parameters
    ----------
    data: Dictionary containing the scores for the separate folds.
    metric: Name of a metric to compute score.
    folds: Number of folds that were used.
    output_dir: Directory in which the results should be stored.
    is_multi_class: If True, score is computed fo multi-class problem and not
                    a binary one.

    Returns
    -------
    An aggregate score.

    """
    # Display mean ROC curve per class -> aggregate folds per class
    if metric == "roc":
        return _compute_aggregate_roc(data, folds, output_dir,
                                      is_multi_class=is_multi_class)


def _compute_aggregate_roc(data, folds, output_dir, is_multi_class=False):
    """
    Aggregates the results of the separate folds.

    Parameters
    ----------
    data: List of dictionaries containing the scores for the separate folds.
    metric: Name of a metric to compute score.
    folds: Number of folds that were used.
    output_dir: Directory in which the results should be stored.
    is_multi_class: If True, score is computed fo multi-class problem and not
                    a binary one.

    Returns
    -------
    An AUC value for each class and the corresponding TPR and FPR for the ROC
    curve.

    Raises
    ------
    ValueError if no value for one of the classes is available.

    """
    # http://scikit-learn.org/stable/auto_examples/model_selection/plot_roc_crossval.html
    # Calculate a value per class
    mean_tpr = {}
    mean_fpr = {}
    tpr = {}    # {class1: [res of fold1, res of fold2...]}
    fpr = {}
    roc_auc = {}
    if is_multi_class:
        print "multi-class data", data
        for fold in data:
            print "fold", fold
            for cls in data[fold]["default"]:
                # Initialize the values
                if cls not in tpr:
                    tpr[cls] = []
                    fpr[cls] = []
                    mean_tpr[cls] = 0.0
                    mean_fpr[cls] = np.linspace(0, 1, 100)
                    roc_auc[cls] = []
                tmp_tpr = data[fold]["default"][cls]["tpr"]
                tmp_fpr = data[fold]["default"][cls]["fpr"]
                fpr[cls].append(data[fold]["default"][cls]["fpr"])
                mean_tpr[cls] += np.interp(mean_fpr[cls], tmp_fpr, tmp_tpr)
                mean_tpr[cls][0] = 0.0
                tpr[cls].append(data[fold]["default"][cls]["tpr"])
                roc_auc[cls].append(data[fold]["default"][cls]["auc"])
        # print "TPR:", tpr
        # print "FPR:", fpr
        # print "AUC:", auc
        # print "Average TPR", mean_tpr
        # print "Average FPR", mean_fpr
        # print "Average AUC", mean_auc
        for cls in mean_tpr:
            mean_tpr[cls] /= folds
            # At the end it has to be 1
            mean_tpr[cls][-1] = 1.0
            # print type(mean_fpr[cls]), mean_fpr[cls]
            # print type(mean_tpr[cls]), mean_tpr[cls]
            mean_auc = auc(mean_fpr[cls], mean_tpr[cls])
            if np.isnan(mean_auc):
                raise ValueError("No instance available for class %s" % cls)
            plt.plot(mean_fpr[cls], mean_tpr[cls], lw=1,
                     label='Avg. ROC (area = %0.2f) ("%s")' % (
                         mean_auc, cls))
    else:
        tpr = []
        fpr = []
        mean_tpr = 0.0
        mean_fpr = np.linspace(0, 1, 100)
        print "binary data", data
        for fold in data:
            print "fold", fold
            tmp_tpr = fold["default"]["tpr"]
            tmp_fpr = fold["default"]["fpr"]
            fpr.append(fold["default"]["fpr"])
            mean_tpr += np.interp(mean_fpr, tmp_fpr, tmp_tpr)
            mean_tpr[0] = 0.0
            tpr.append(fold["default"]["tpr"])
        print "TPR:", tpr
        print "FPR:", fpr
        print "AUC:", auc
        # print "Average TPR", mean_tpr
        # print "Average FPR", mean_fpr
        # print "Average AUC", mean_auc
        # for cls in mean_tpr:
        mean_tpr /= folds
        # At the end it has to be 1
        mean_tpr[-1] = 1.0
        # print type(mean_fpr[cls]), mean_fpr[cls]
        # print type(mean_tpr[cls]), mean_tpr[cls]
        mean_auc = auc(mean_fpr, mean_tpr)
        if np.isnan(mean_auc):
            raise ValueError("No instance available for binary class")
        plt.plot(mean_fpr, mean_tpr, lw=1,
                 label='Avg. ROC (area = %0.2f)' % (
                     mean_auc))
    # Now do the plotting
    plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Average ROC curve for %s-fold CV' % folds)
    plt.legend(loc="lower right")
    # Store average FPR, TPR and AUC of model
    with open(os.path.join(output_dir, "results.bin"), "wb") as f:
        cPickle.dump(mean_fpr, f)
        cPickle.dump(mean_tpr, f)
        cPickle.dump(mean_auc, f)
    # How to reload
    # with open(os.path.join(output_dir, "results.bin"), "rb") as f:
    #     a = cPickle.load(f)
    #     b = cPickle.load(f)
    #     c = cPickle.load(f)
    # Store ROC curve
    output_dir = os.path.join(output_dir, "plots")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    plt.savefig(os.path.join(output_dir, "roc.png"))
    return mean_auc


def run_experiments(base_config_dir):
    """
    Executes all specified experiments for a task and stores the outcomes in
    the respective output directories.

    Parameters
    ----------
    base_config_dir: Base directory in which all configuration files for the
    experiments are stored.

    """
    # This is a list of entries each config file MUST include, otherwise it
    # can't be read in successfully and the experiment is bound to fail.
    MANDATORY = ["input", "preprocess", "classifier", "output"]
    # For each config file
    for conf in os.listdir(base_config_dir):
        config_path = os.path.join(base_config_dir, conf)
        # Omit directories
        if os.path.isfile(config_path):
            # print conf
            # Read in the parameters
            in_params, pre_params, train_params, eval_params, sel_params, \
                out_params = read_config(config_path)
            print "\n------------Reading in parameters completed-------------\n"
            output_dir = out_params[0]["output"]["base"]
            algo = train_params[0]["classifier"].get("name", None)
            if algo is None:
                raise ValueError("You must specify an ML algorithm in your "
                                 "config file! None could be found.")
            now = datetime.datetime.now()
            now = str(now).split(".")[0]
            name = str(now) + "_" + algo
            output_dir = os.path.join(output_dir, name)
            # Store the config file in the experiment subfolder for
            # reproducibility
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            shutil.copy(config_path, os.path.join(output_dir, "config.txt"))
            # Make sure that a directory for the tweet texts of the dataset
            # was specified because otherwise BoW transformation won't work
            # Second, utilize the tweet information
            tweet_path = in_params[0]["input"].get("tweets", None)
            # First, read in the raw directory
            # clean_path = in_params[0]["input"]["raw"]
            print "dataset path", tweet_path
            tweets = open_dataset2(tweet_path)
            # raw_tweets, tweets_idx = open_cleansed_tsv(clean_path)
            # Add the actual text message to the existing tweets
            # tweets = open_tweets_tsv(
            #     raw_tweets,
            #     tweets_idx,
            #     tweet_path
            # )
            print "Read in: {} tweets for subtask a".format(len(tweets))
            # Directory from where the test set would be read in
            test_path = in_params[0]["input"].get("test", None)
            print "Test set (if to be used) is stored here: {}".format(
                test_path)
            # print "Existing lookup indices: {}".format(len(tweets_idx))
            # tweets = preprocess_tweets(
            #     tweets,
            #     pre_params,
            # )
            print "Entire corpus to be used for training/testing comprises {" \
                  "} tweets. However, test set is ignored in this " \
                  "computation, thus it could be more.".format(len(tweets))
            print "\n----------------Pre-processing completed----------------\n"
            classifier = get_classifier(train_params)
            print "\n----------------Classifier instantiated-----------------\n"
            print "classifier to be used is", type(classifier)
            evaluate(classifier, tweets, eval_params,
                     pre_params, sel_params, output_dir, test_path)
            print "\n-----------------Evaluation completed-------------------\n"


if __name__ == "__main__":
    if settings.DEBUG:
        run_experiments("configs/test")
    else:
        run_experiments("configs")
    # Relative path to the file containing all experiments to be run
