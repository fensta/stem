Best weighted F1 score with budget 100: 0.467809359621

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 151      | 101      | 107      | 
| neutral  | 148      | 235      | 302      | 
| positive | 88       | 196      | 471      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
