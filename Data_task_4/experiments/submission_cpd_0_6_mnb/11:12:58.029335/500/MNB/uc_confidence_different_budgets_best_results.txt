Best weighted F1 score with budget 100: 0.478032594947

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 115      | 155      | 89       | 
| neutral  | 117      | 285      | 283      | 
| positive | 73       | 206      | 476      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
