Best weighted F1 score with budget 500: 0.449642928174

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 59       | 229      | 71       | 
| neutral  | 100      | 321      | 264      | 
| positive | 66       | 252      | 437      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
