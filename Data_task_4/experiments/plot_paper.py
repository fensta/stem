import cPickle
import os

import numpy as np
from matplotlib import pyplot as plt


def find_budget(budgets, given):
    return budgets.index(given)

def get_data_at(pos, scores, cms):
    return scores[pos], cms[pos]

def find_best_budget(cms, f1_scores, budgets):
    """
    Finds the budget that optimizes F1 score.

    Parameters
    ----------
    cms: List of confusion matrices for the respective budgets.
    f1_scores: List of F1 scores for the respective budgets.
    budgets: Budgets for which the performance of the AL strategy was analyzed.

    Returns
    -------
    double, np_array, int.
    Best F1 score, respective confusion matrix, best budget where F1 score
    was achieved.

    """
    best_f1 = 0.0
    best_cm = []
    best_budget = 0
    for idx in xrange(len(budgets)):
        f1 = f1_scores[idx]
        if f1 > best_f1:
            best_f1 = f1
            best_cm = cms[idx]
            best_budget = budgets[idx]
    return best_f1, best_cm, best_budget


if __name__ == "__main__":
    src_dir = "test_reuseability/20:16:10.967132/500/MNB/"
    # src_dir = "test_reuseability/19:50:47.939053/500/MNB/"
    # src_dir = "test_reuseability/14:45:23.159146/500/MNB/"
    plt.rcParams.update({'font.size': 18})
    plt.clf()
    bases = []
    vals = []
    budget = 400
    seed_set_size = 0
    for f in os.listdir(src_dir):
        if f.endswith("_reuse.txt"):
            src = os.path.join(src_dir, f)
            # Add one bar
            with open(src, "rb") as f:
                # See apply_al.py for format
                # [[Al], [Baseline]]
                # For each:
                # F1 scores, confusion matrices, seed set size, budgets, name
                strat, base = cPickle.load(f)
                al_scores, cms, seed_set_size, budgets, name = strat
                idx = find_budget(budgets, budget)
                f1, _ = get_data_at(idx, al_scores, cms)
                # Now we want to store 3 digits after comma instead of 2
                name = name[:-14]
                if name.endswith("-margin"):
                    name = name[:-7] + "-marg"
                if name.endswith("-confidence"):
                    name = name[:-11] + "-conf"
                # name = name[:-14] + "({:.3f})".format(f1)
                vals.append([f1, budget, name])
                base_scores, base_cms, _, _, name = base
                bases.append(base_scores)
    # Since we calculated random baseline per method, let's make use of the
    # data and average it per column (= per budget)
    bases = np.array(bases)
    print bases
    avg_bases = list(np.mean(bases, axis=0))
    # IMPORTANT: cms is NOT the correct list of confusion matrices, but we're
    #  not using them anyway
    idx = find_budget(budgets, budget)
    f1, _ = get_data_at(idx, al_scores, cms)
    # f1, cm, budget = find_best_budget(cms, avg_bases, budgets)
    # vals.append([f1, budget, "Baseline({:.3f})".format(f1)])
    vals.append([f1, budget, "Baseline"])
    # Convert F1-scores to scalars
    ys = [x[0] for x in vals]
    xs = np.arange(len(vals))
    names = [x[2] for x in vals]
    width = 0.8
    margin = 0.0
    # width = (1.-2.*margin) / len(vals)
    p = plt.bar(xs, ys, width=width, align="edge", color="orange")
    # Center ticks on x-axis
    ax = plt.gca()
    ax.set_xticks(xs + width/2)
    ax.set_ylabel("Weighted F1-score")
    plt.ylim(0, 0.52)
    ax.set_xticklabels(names, rotation=0)
    ys = ["{:.3f}".format(x) for x in ys]
    # Add F1 score on top of bar
    for idx, rect in enumerate(p):
        h = rect.get_height()
        plt.text(rect.get_x()+rect.get_width()/2., h, '%s' % (ys[idx]),
                 ha='center', va='bottom')
    # Set structures in bars for readability
    # p[0].set_hatch("-")
    plt.show()
    # http://stackoverflow.com/questions/11617719/how-to-plot-a-very-simple-bar-chart-python-matplotlib-using-input-txt-file
