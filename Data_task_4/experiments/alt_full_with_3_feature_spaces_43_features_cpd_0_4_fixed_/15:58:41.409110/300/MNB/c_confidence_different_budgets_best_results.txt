Best weighted F1 score with budget 700: 0.443048774602

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 63       | 209      | 87       | 
| neutral  | 81       | 342      | 262      | 
| positive | 50       | 288      | 417      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
