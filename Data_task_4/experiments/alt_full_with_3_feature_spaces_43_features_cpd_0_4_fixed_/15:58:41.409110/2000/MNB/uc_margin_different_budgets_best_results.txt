Best weighted F1 score with budget 1000: 0.466624879915

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 94       | 163      | 102      | 
| neutral  | 78       | 250      | 357      | 
| positive | 47       | 196      | 512      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
