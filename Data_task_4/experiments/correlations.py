"""
Find polarity words that approximately occur in all classes, this giving no
useful information
"""
import os
from parser.config_parse import parse_config_file
from collections import Counter
import cPickle

import enchant

from Data_task_4.io.input_output import open_dataset
from Data_task_4.preprocess.preprocessing import *
from manual_improvements import SLANG, REPLACEMENTS, NEGATION_REPLACEMENTS


def read_config(config_path):
    """
    Read in a configuration file and return the parameters.

    Parameters
    ----------
    config_path: Path to a configuration file.

    Returns
    -------
    List, list, list, list, list
    Returns a quintuple of lists where each one corresponds to a number of
    parameters for input, preprocessing, training, evaluation and output (in
    that order). Most of the lists contain dictionaries to access the
    parameters, but for preprocessing lists of tuples are used to follow the
    order specified in the config file.

    """
    inp = []
    params = parse_config_file(config_path)
    # print params
    for param_set in params:
        if "input" in param_set:
            inp.append(param_set)
    return inp


def read_file(base_config_dir):
    """
    Reads in tweets from a configuration file.

    Parameters
    ----------
    base_config_dir: Directory in which a configuration is stored to be read.

    Returns
    -------
    List of Tweet objects.

    """
    for conf in os.listdir(base_config_dir):
        config_path = os.path.join(base_config_dir, conf)
        # Omit directories
        if os.path.isfile(config_path):
            in_params = read_config(config_path)
    tweet_path = in_params[0]["input"].get("tweets", None)
    return open_dataset(tweet_path)


def count_class_distribution_per_word(tweet):
    """
    Counts in which classes a polarity bearing word occurs.

    Parameters
    ----------
    tweet: Tweet object.

    Returns
    -------
    dict.
    Dictionary containing the words as keys and how often they occur per class.

    """
    # {word1: Counter{"negative": x, "positive": y, "objective": z}}
    distribution = {}
    for w in tweet.text:
        if w not in distribution:
            distribution[w] = Counter()
        distribution[w][tweet.label] += 1
    return distribution


def update_counts(this, other):
    """
    Merges the counts in 2 dictionaries and adds them to the first dictionary.

    Parameters
    ----------
    this: Dictionary 1 with Counter objects as values.
    other: Dictionary 2 with Counter objects as values.

    Returns
    -------
    dict.
    Dictionary including values of <this> updated with those from <other>.

    """
    for k, v in other.iteritems():
        if k in this:
            print k
            print "single", v, ":", this[k]
            this[k].update(v)
        else:
            this[k] = v
    return this


def analyze_tweets(tweets):
    """
    Finds correlations within the tweets.

    Parameters
    ----------
    tweets: List of Tweet objects.

    """
    distribution = {}
    d = enchant.Dict("en")
    for tweet in tweets:
        text = tweet.text
        text = multiple_replace(text, REPLACEMENTS)
        text = remove_numbers(text)
        text = text.split()
        text = remove_url(text)
        text = separate_punctuation(" ".join(text)[:-1])
        text = remove_twitter_handle(text)
        text = remove_hash(text, d)
        text, intensify_emoji, pos, neg = emoticons(text)
        text = replace_slang(text, SLANG)
        text = expand_negations(text, NEGATION_REPLACEMENTS)
        text = " ".join(text)
        text = get_pos_tags(text)
        text, intensify_punct = punctuation(text)
        sentence_polarity = determine_sentence_polarity(
            text, intensify_emoji, intensify_punct, None, None)
        # Text contains now only sentiment bearing words
        tmp = sentence_polarity[-1]
        text = remove_all_words_except_(tmp)
        text = to_lower(text)
        text = remove_stop_words(text)
        text = stem(text, "snowball", "english")
        tweet.text = text
        # distribution.update(count_class_distribution_per_word(tweet))
        distribution = update_counts(distribution,
                                     count_class_distribution_per_word(tweet))
    return distribution


def find_uninformative_words(words, dst):
    """
    Finds the words that bear no useful information. Stores them in a file.

    Parameters
    ----------
    words: Dictionary of words as keys and a dictionary as value where the
    latter stores the occurrences of the word w.r.t. the different classes.
    dst: Destination where output file should be stored.

    """
    # in %
    threshold = 10
    # Calculate probabilities per word to occur in the respective classes
    for k, v in words.iteritems():
        total = sum(v.values())
        pos = 100.0 * words[k].get("positive", 0) / total
        neg = 100.0 * words[k].get("negative", 0) / total
        obj = 100.0 * words[k].get("neutral", 0) / total
        print "{}: negative: {}% neutral: {}% positive: {}%".format(k, neg,
                                                                  obj, pos)
        if abs(pos - neg) < threshold and abs(pos - obj) < threshold and \
                abs(neg - obj) < threshold:
            # print "UNINFORMATIVE:"
            # print "-------------"
            # print k
            with open(dst, "a") as f:
                f.write(k + "\n")


if __name__ == "__main__":
    # True if analysis should be started from the beginning
    start_new = False
    dst = "../dataset_analysis/uninformative_words.txt"
    # Start new analysis
    if start_new:
        tweets = read_file("configs")
        distribution = analyze_tweets(tweets)
        with open(os.path.join("../dataset_analysis/raw_counts.pkl"),
                  "wb") as f:
            cPickle.dump(distribution, f)
    else:
        # Load stored data
        with open(os.path.join("../dataset_analysis/raw_counts.pkl"),
                  "rb") as f:
            distribution = cPickle.load(f)

    find_uninformative_words(distribution, dst)

