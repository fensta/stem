Best weighted F1 score with budget 400: 0.468560562258

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 116      | 140      | 103      | 
| neutral  | 119      | 265      | 301      | 
| positive | 74       | 174      | 507      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
