Best weighted F1 score with budget 500: 0.467329985

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 143      | 108      | 108      | 
| neutral  | 138      | 196      | 351      | 
| positive | 79       | 130      | 546      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
