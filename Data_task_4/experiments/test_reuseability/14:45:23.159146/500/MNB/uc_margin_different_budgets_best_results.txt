Best weighted F1 score with budget 300: 0.453065591476

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 65       | 161      | 133      | 
| neutral  | 69       | 220      | 396      | 
| positive | 28       | 150      | 577      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
