Best weighted F1 score with budget 500: 0.476235540151

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 150      | 168      | 41       | 
| neutral  | 156      | 337      | 192      | 
| positive | 79       | 335      | 341      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
