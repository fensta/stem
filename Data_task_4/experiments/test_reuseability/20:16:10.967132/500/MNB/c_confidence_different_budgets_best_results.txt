Best weighted F1 score with budget 500: 0.471387499103

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 62       | 236      | 61       | 
| neutral  | 55       | 408      | 222      | 
| positive | 23       | 337      | 395      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
