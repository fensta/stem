"""
Use AlchemyAPI to extract NERs and their respective overall sentiment.
Furthermore, compute overall sentiment of tweets.

TO USE THIS SCRIPT, ADD YOUR FILE CALLED "api_key.txt" containing your API
key TO THE SAME DIRECTORY AS THIS SCRIPT.
"""
import os
import json
import codecs
import requests

from alchemyapi import AlchemyAPI

from Data_task_4.io.input_output import open_dataset


def load_result(dic_path):
    """

    Reads in a stored dictionary for further processing.

    Parameters
    ----------
    dic_path: Path to a dic to be loaded.

    Returns
    -------
    The JSON-encoded content of the tweet.

    """
    with codecs.open(dic_path, "r", "utf-8-sig") as f:
        unicode_json = json.load(f)
    return unicode_json


def __get_api_key():
    # Load API key (40 HEX character key) from local file which isn't in
    # repository
    key = open('api_key.txt').readline().strip()
    return key


def __alchemy_calls_left(api_key):
    """ Typical response from Alchemy:
    {
     "status": "OK",
     "consumedDailyTransactions": "1020",
     "dailyTransactionLimit": "1000"
    }
    This URL tells us how many calls we have left in a day
    """
    URL = "http://access.alchemyapi.com/calls/info/GetAPIKeyInfo?apikey={}\
    &outputMode=json".format(api_key)
    # call AlchemyAPI, ask for JSON response
    response = requests.get(URL)
    calls_left = json.loads(response.content)
    return calls_left


def __get_calls_left(api_key):
    """Call AlchemyAPI, report still_calls_left and details"""
    calls_left = __alchemy_calls_left(api_key)
    # convert the text number fields to integers
    calls_left['consumedDailyTransactions'] = int(
        calls_left['consumedDailyTransactions'])
    calls_left['dailyTransactionLimit'] = int(calls_left[
        'dailyTransactionLimit'])
    # add a convenience boolean
    calls_left['still_calls_left'] = calls_left['consumedDailyTransactions'] <\
    calls_left['dailyTransactionLimit']
    return calls_left


def merge_dics(existing, keywords, relations):
    """
    This function merges an existing dictionary with 2 more dictionaries.

    Parameters
    ----------
    existing: Existing dictionary.
    keywords: Dictionary containing sentiment for keywords.
    relations: Dictionary containing sentiment for relations.

    Returns
    -------
    dict.
    Dictionary that contains entries for 4 different types of sentiment:
    1. document -> key "docSentiment"
    2. per entity -> key "entities"
    3. per keyword -> key "keywords"
    4. relations -> key "relations"

    """
    if "keywords" in existing:
        existing["keywords"] = keywords["keywords"]
    if "relations" in existing:
        existing["relations"] = relations["relations"]
    # return existing


def store_response(entity_response, document_response, out_path):
    """
    Stores AlchemyAPI response in JSON format under the same name as the
    original tweet. The information is stored in UTF-8 format.

    Parameters:
    -----------
    entity_response: Dictionary containing sentiment of extracted entities.
    document_response: Dictionary containing document sentiment.
    out_path: Path where results will be stored.

    """
    # See here for examples:
    # http://www.alchemyapi.com/products/demo/alchemylanguage

    # Merge dictionaries
    response = {}
    # Merge only if the API can deal with the language
    if "language" in entity_response and "language" in document_response:
        if entity_response["language"] == "english" or document_response[
                "language"] == "english":
            response["status"] = entity_response["status"]
            response["language"] = entity_response["language"]
            response["usage"] = entity_response["usage"]
            response["totalTransactions"] =\
                int(entity_response["totalTransactions"]) + int(
                    document_response["totalTransactions"])
            response["text"] = document_response["text"]
            response["docSentiment"] = document_response["docSentiment"]
            response["entities"] = entity_response["entities"]

            with codecs.open(out_path, "w", encoding="utf-8") as f:
                json.dump(response, f, indent=4, sort_keys=True,
                          ensure_ascii=False)
        else:
            print "skipped", document_response["text"]


def get_more_sentiment(text):
    """
    Finds the sentiment of keywords and also the sentiment of relations. For
    the latter, NOT the sentiment of single words is considered, but the
    sentiment of relations, e.g. "Ugly bob attacked beautiful Susan" -> "Ugly
    bob", "beautiful Susan" and the subject to object relationship are assigned
    sentiments - in this case the subject to object relationship is negative
    ("bob attacked Susan").

    Parameters
    ----------
    text: Text for which the named entities need to be determined by AlchemyAPI.

    Returns
    -------
    dict, dict.
    The AlchemyAPI responses for both scenarios (keywords, relations) in JSON
    format. The former is accessible via the key "keywords", while the latter
    is with the key "relations".

    """
    keyword_sentiment = {}
    relation_sentiment = {}
    api_key = __get_api_key()
    calls_left = __get_calls_left(api_key)
    print "Calls left:", calls_left
    calls_left = calls_left["still_calls_left"]
    if calls_left:
        alchemyapi = AlchemyAPI()
        keyword_sentiment = alchemyapi.keywords('text', text, {'sentiment': 1})
        relation_sentiment = alchemyapi.relations('text', text, {'sentiment':
                                                                 1})
        if keyword_sentiment['status'] != 'OK':
            print('Error in entity extraction call: ',
                  keyword_sentiment['statusInfo'])
        if relation_sentiment['status'] != 'OK':
            print('Error in document extraction call: ',
                  relation_sentiment['statusInfo'])
    else:
        keyword_sentiment["error"] = json.dumps("Rate limit exceeded")
        relation_sentiment["error"] = json.dumps("Rate limit exceeded")
    return keyword_sentiment, relation_sentiment


def get_sentiment(text):
    """

    Retrieves the named entities from a given text with their respective
    sentiment and also the sentiment of the whole tweet.

    Parameters:
    -----------
    text: Text for which the named entities need to be determined by AlchemyAPI.

    Returns:
    --------
    dict, dict.
    The AlchemyAPI responses for both scenarios (entities, tweet) in JSON
    format.

    """
    api_key = __get_api_key()
    calls_left = __get_calls_left(api_key)
    print "Calls left:", calls_left
    entity_sentiment = {}
    document_sentiment = {}
    calls_left = calls_left["still_calls_left"]
    if calls_left:
        alchemyapi = AlchemyAPI()
        entity_sentiment = alchemyapi.entities('text', text, {'sentiment': 1})
        document_sentiment = alchemyapi.sentiment('text', text,
                                                  {'showSourceText': 1})
        if entity_sentiment['status'] != 'OK':
            print('Error in entity extraction call: ',
                  entity_sentiment['statusInfo'])
        if document_sentiment['status'] != 'OK':
            print('Error in document extraction call: ',
                  document_sentiment['statusInfo'])
    else:
        entity_sentiment["error"] = json.dumps("Rate limit exceeded")
        document_sentiment["error"] = json.dumps("Rate limit exceeded")
    return entity_sentiment, document_sentiment


def store_final_response(response, out_path):
    """
    Stores AlchemyAPI response in JSON format under the same name as the
    original tweet. The information is stored in UTF-8 format.

    Parameters:
    -----------
    response: Dictionary containing sentiment information of a tweet.
    out_path: Path where results will be stored.

    """
    # See here for examples:
    # http://www.alchemyapi.com/products/demo/alchemylanguage

    # Store only if the API can deal with the language
    if response["language"] == "english":
        with codecs.open(out_path, "w", encoding="utf-8") as f:
            json.dump(response, f, indent=4, sort_keys=True, ensure_ascii=False)
    else:
        print "skipped", response["text"]


def process_tweets(tweet_path, response_dir):
    """

    Process all tweets in the given directory, i.e. extract their named
    entities.

    Parameters:
    -----------
    tweet_path: Path to the file containing all tweets.
    response_dir: Directory in which the results will be stored.

    """
    tweets = open_dataset(tweet_path)
    for idx, tweet in enumerate(tweets):
        dst = os.path.join(response_dir, tweet.tweet_id + ".txt")
        # If information wasn't downloaded yet
        if not os.path.exists(dst):
            ner_polarities, tweet_polarity = get_sentiment(tweet.text)
            # If both requests could be processed by API, store results
            if not ner_polarities.get("error") == '"Rate limit exceeded"' and\
                    not tweet_polarity.get("error") == '"Rate limit exceeded"':
                store_response(ner_polarities, tweet_polarity, dst)
            else:
                print "#################################################################"
                print "#AlchemyAPI rate limit exceeded for today - continue tomorrow...#"
                print "#################################################################"
                break
        # Load existing dictionary if it exists (it doesn't exist if its
        # language is unsupported)
        if os.path.isfile(dst):
            current_response = load_result(dst)
            if "keywords" not in current_response:
                keyword_polarities, relation_polarities = \
                    get_more_sentiment(tweet.text)
                # If both requests could be processed by API, store results
                if not keyword_polarities.get("error") == '"Rate limit exceeded"' \
                        and not relation_polarities.get("error") == '"Rate limit ' \
                                                                    'exceeded"':
                    merge_dics(current_response, keyword_polarities,
                               relation_polarities)
                    store_final_response(current_response, dst)
                    print "updated tweet {}".format(tweet.tweet_id)
                else:
                    print "#################################################################"
                    print "#AlchemyAPI rate limit exceeded for today - continue tomorrow...#"
                    print "#################################################################"
                    break

if __name__ == "__main__":
    # Training data
    tweet_path = "../semeval2016_task4/training/tweets.txt"
    # Dev data
    tweet_path = "../semeval2016_task4/dev/tweets.txt"
    # Dev test data
    # tweet_path = "../semeval2016_task4/test/tweets.txt"
    tweet_path = "../semeval2016_task4/TestTest/SemEval2016-task4-test.subtask-A.txt"
    output_dir = "../extracted_alchemy_api_data"
    process_tweets(tweet_path, output_dir)