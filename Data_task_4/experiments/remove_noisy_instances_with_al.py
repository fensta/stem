"""
Let AL strategy select the instances that should be used for supervised
learning. Why is it necessary to relearn the supervised model? We use 3
classifiers for different subspaces.

Workflow:
1. Run run_experiments_own_bow.py (also create datasets with reduced feature
set by commenting out 2 parts where reduced training and test set would be
stored)
2. Run remove_noisy_instances_with_al.py using the 2 reduced feature sets
created above.
3. Replace the original training set of step 1 by the one created here with
fewer instances. DON'T FORGET TO CREATE A COPY OF THE ORIGINAL DATASET FIRST!
4. Rerun run_experiments_own_bow.py with reduced training set.

"""
import os
import random
import cPickle

from sklearn.linear_model import LogisticRegression
from sklearn.multiclass import OneVsRestClassifier
from sklearn.naive_bayes import MultinomialNB

from Data_task_4.al_strategies.certainty_sampling import CertaintySampling
from Data_task_4.al_strategies.uncertainty_sampling import UncertaintySampling


def create_seed_set_and_unlabeled_pool(tweets, budget=300):
    """
    Seed set contains at least 1 tweet per class (3) for every topic (100),
    so at least 300 tweets.

    Parameters
    ----------
    tweets: List of Tweet objects.
    budget: Number of instances to be used in seed set. Minimum value is 300.

    Returns
    -------
    List, list.
    List of Tweet objects in seed set, list of tweet objects remaining in
    unlabeled pool.

    """
    unlabeled = []
    # Create 3 pools of labels, one per class.
    pos = []
    neg = []
    obj = []
    pos_idx = []
    obj_idx = []
    neg_idx = []
    # Mapping to retain order of original tweets. 2 = neg, 1 = obj, 0 = pos
    order = []
    # Create unique IDs for each tweet (ID = position in original list)
    idx = 0
    for t in tweets:
        if t.label == "positive":
            list_no = 0
        if t.label == "neutral":
            list_no = 1
        if t.label == "negative":
            list_no = 2
        order.append(list_no)
        if t.label == "positive":
            pos.append(t)
            pos_idx.append(idx)
        if t.label == "neutral":
            obj.append(t)
            obj_idx.append(idx)
        if t.label == "negative":
            neg.append(t)
            neg_idx.append(idx)
        idx += 1
    # pos = [t for t in tweets if t.label == "positive"]
    # obj = [t for t in tweets if t.label == "neutral"]
    # neg = [t for t in tweets if t.label == "negative"]
    print "Pool sizes"
    print "positive:", len(pos)
    print "neutral:", len(obj)
    print "negative:", len(neg)
    print "total:", len(tweets), len(pos) + len(obj) + len(neg)
    print "ordered", order
    print "neg idx", neg_idx
    print "obj idx", obj_idx
    print "pos idx", pos_idx
    seed_pos_idx = []
    seed_obj_idx = []
    seed_neg_idx = []
    seed_set = []
    while budget > 0:
        # Draw a tweet from positive pool
        idx = random.randint(0, len(pos) - 1)
        # print "idx in pos", idx
        orig_idx = pos_idx[idx]
        if orig_idx not in seed_pos_idx:
            tweet = tweets[orig_idx]
            seed_pos_idx.append(orig_idx)
            seed_set.append(tweet)
            budget -= 1
        if budget > 0:
            # Draw a tweet from negative pool
            idx = random.randint(0, len(neg) - 1)
            # print "idx in neg list", idx
            orig_idx = neg_idx[idx]
            if orig_idx not in seed_neg_idx:
                tweet = tweets[orig_idx]
                seed_neg_idx.append(orig_idx)
                seed_set.append(tweet)
                budget -= 1
        if budget > 0:
            # Draw a tweet from objective pool
            idx = random.randint(0, len(obj) - 1)
            # print "idx in obj list", idx
            orig_idx = obj_idx[idx]
            if orig_idx not in seed_obj_idx:
                tweet = tweets[orig_idx]
                seed_obj_idx.append(orig_idx)
                seed_set.append(tweet)
                budget -= 1
    # print "seed pos idx", seed_pos_idx
    # print "seed obj idx", seed_obj_idx
    # print "seed neg idx", seed_neg_idx
    # Build unlabeled pool (= all tweets that are not in seed set)
    for idx, l in enumerate(order):
        # Positive tweet
        if l == 0:
            # If it's not in the list
            if idx not in seed_pos_idx:
                unlabeled.append(tweets[idx])
        # Objective tweet
        if l == 1:
            if idx not in seed_obj_idx:
                unlabeled.append(tweets[idx])
        # Negative tweet
        if l == 2:
            if idx not in seed_neg_idx:
                unlabeled.append(tweets[idx])
    # print "seed"
    # for s in seed_set:
    #     print s.text
    # print "unlabeled"
    # for s in unlabeled:
    #     print s.text
    print "seed", len(seed_set)
    print "unlabeled", len(unlabeled)
    return seed_set, unlabeled


if __name__ == "__main__":
    train_path_full = \
        "../preprocessed_datasets" \
        "/alt_submission_cpd_0_6_10_train_full.txt.bak"
    # Reduced data sets were created using work flow - towards the end it's
    # hard-coded that the reduced datasets should be stored
    train_path = \
        "../preprocessed_datasets" \
        "/alt_submission_cpd_0_6_10_train_reduced.txt"
    test_path = "../preprocessed_datasets" \
                "/alt_submission_cpd_0_6_10_test_reduced.txt"
    # train_path = \
    #     "../preprocessed_datasets" \
    #     "/test3_train_full.txt"
    # test_path = "../preprocessed_datasets" \
    #             "/test3_test_full.txt"
    with open(train_path, "rb") as f:
        tweets = cPickle.load(f)
    with open(test_path, "rb") as f:
        test_tweets = cPickle.load(f)
    mnb = MultinomialNB()
    # mnb = LogisticRegression()
    scores1 = []
    scores2 = []
    ovr = OneVsRestClassifier(mnb)
    budget = 400
    seed_set, unlabeled = create_seed_set_and_unlabeled_pool(tweets, 500)
    # uc = UncertaintySampling(ovr, seed_set, budget, seed=42,
    #                          al_type="confidence")
    # f1, cm = uc.cv_uncertainty(unlabeled, k=10, test=test_tweets)
    uc = CertaintySampling(ovr, seed_set, budget, seed=42, al_type="margin")
    f1, cm = uc.cv_certainty(unlabeled, k=10, test=test_tweets)
    print cm
    print "F1", f1
    print "labeled tweets", len(uc.labeled_tweets)
    # Now find the respective instances in training set with higher
    # dimensionality, so that it also works in our workflow (attributes are
    # removed over there)
    with open(train_path_full, "rb") as f:
        training_tweets = cPickle.load(f)
    reduced = set([t.tweet_id for t in uc.labeled_tweets])
    normal = set([t.tweet_id for t in training_tweets])
    print "tweets in training set", len(normal)
    print "tweets in reduced training set", len(reduced)
    print "tweets that exist in both", len(normal.intersection(reduced))
    print "tweets that exist only in training set", len(normal - reduced)
    small_training_set = []
    # For all tweets in whole dataset
    for t in training_tweets:
        # For each tweet in small dataset
        for tt in uc.labeled_tweets:
            if t.tweet_id == tt.tweet_id:
                # Keep tweet with full dimensions
                small_training_set.append(t)
                break
    print "Tweets in small training set:", len(small_training_set)
    dst = "alt_sub_uc_reduced_900_10_train_full.txt"
    # Store reduced dataset
    cnt = 0
    # Don't blindly overwrite existing file, rename new file
    if os.path.isfile(dst):
        while os.path.isfile(dst + str(cnt)):
            cnt += 1
        dst += str(cnt)
    print "file name of reduced dataset:", dst
    with open(dst, "wb") as f:
        cPickle.dump(small_training_set, f)
    with open(dst, "rb") as f:
        test = cPickle.load(f)
    print "Tweets in small training set:", len(test)
    print "dimensions in tweets", test[0].bow.shape