Best weighted F1 score with budget 500: 0.462039117634

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 77       | 179      | 103      | 
| neutral  | 66       | 291      | 328      | 
| positive | 37       | 185      | 533      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
