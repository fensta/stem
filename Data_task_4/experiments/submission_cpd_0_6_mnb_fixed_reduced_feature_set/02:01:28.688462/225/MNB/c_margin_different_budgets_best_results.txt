Best weighted F1 score with budget 200: 0.441587631224

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 63       | 261      | 35       | 
| neutral  | 90       | 430      | 165      | 
| positive | 70       | 392      | 293      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
