Best weighted F1 score with budget 300: 0.446575308794

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 66       | 160      | 133      | 
| neutral  | 60       | 238      | 387      | 
| positive | 25       | 153      | 577      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
