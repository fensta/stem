Best weighted F1 score with budget 300: 0.439644946141

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 104      | 127      | 128      | 
| neutral  | 93       | 169      | 423      | 
| positive | 46       | 158      | 551      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
