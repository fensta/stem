Best weighted F1 score with budget 300: 0.431377500901

Confusion matrix: (rows: truth, columns: predicted)
           | negative | neutral  | positive
----------------------------------------------
| negative | 38       | 201      | 120      | 
| neutral  | 80       | 240      | 365      | 
| positive | 67       | 144      | 544      | 
Classifier: MNB
{'estimator__alpha': 1.0, 'estimator': MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True), 'estimator__fit_prior': True, 'n_jobs': 1, 'estimator__class_prior': None}
