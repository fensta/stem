# from Data_task_4.io import input_output as io
import os
from scipy import sparse
from sklearn.metrics import accuracy_score
from Data_task_4.io.input_output import open_cleansed_tsv, open_tweets_tsv
from Data_task_4.preprocess.filters import to_bag_of_words
from sklearn.ensemble import RandomForestClassifier
from Data_task_4 import settings
from sklearn.cross_validation import ShuffleSplit, KFold, train_test_split
from sklearn.cross_validation import cross_val_score
from nltk.corpus import stopwords
import numpy as np
import operator


# http://www.astro.washington.edu/users/vanderplas/Astr599/notebooks
# /18_IntermediateSklearn
def plot_confusion_matrix(y_pred, y):
    plt.imshow(metrics.confusion_matrix(y, y_pred),
               cmap=plt.cm.binary, interpolation='nearest')
    plt.colorbar()
    plt.xlabel('true value')
    plt.ylabel('predicted value')


if __name__ == "__main__":
    # Path to the cleansed dataset
    clean_path = "../semeval2016_task4_trial_c_d_e" \
                 "/semeval_2016_task4_trial_c_d_e.tsv"
    # Path to the dataset containing the tweet content
    tweet_path = "../semeval2016_task4_trial_c_d_e" \
                 "/semeval_2016_task4_trial_c_d_e_tweets.tsv"
    output_dir = "../results/test/"
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    if settings.DEBUG:
        print "DEBUG", settings.DEBUG
        clean_path = "../semeval2016_task4_trial_c_d_e/debug_trial_c_d_e.txt"
        tweet_path = "../semeval2016_task4_trial_c_d_e" \
                     "/debug_trial_c_d_e_tweets" \
                     ".txt"
    # Read in the the cleansed dataset
    tweets, tweets_idx = open_cleansed_tsv(clean_path)
    print "Read in: {} tweets for subtasks c, d, e".format(len(tweets))
    print "Existing lookup indices: {}".format(len(tweets_idx))

    # For testing purposes only
    print "#tweets", len(tweets)
    train, test = train_test_split(tweets, train_size=0.8)
    print "#tweets in training set", len(train)
    print "#tweets in test set", len(test)

    # Add the actual text message to the existing tweets
    tweets = open_tweets_tsv(tweets, tweets_idx, tweet_path)
    cv = KFold(len(tweets), n_folds=2, shuffle=True, random_state=42)
    print "folds", len(cv)
    for fold, (train_idx, test_idx) in enumerate(cv):
        print "\nFOLD", fold + 1
        print "elements in train:", train_idx.shape[0]
        print "indices of tweets in training set", train_idx
        for i in train_idx:
            print tweets[i].tweet_id
        bow_train_tweets, t_vectorizer = to_bag_of_words(
            tweets, selected_tweets=train_idx)
        print "elements in test:", test_idx.shape[0]
        print "indices of tweets in test set", test_idx
        for i in test_idx:
            print tweets[i].tweet_id
        bow_test_tweets, vectorizer = to_bag_of_words(
            tweets, selected_tweets=test_idx,
            vectorizer=t_vectorizer)
        # test_tweets = [tweets[i].text for i in test_idx]
        # print "test texts", test_tweets
        # bow_test_tweets = vectorizer.transform(test_tweets)
        # print "test transformed", bow_test_tweets
        print "vocabulary", vectorizer.vocabulary_

    # # Transform the tweets into the bag-of-words representation
    # tweets = to_bag_of_words(tweets, tweets_idx, tweet_path,
    #                          stop_words=stopwords.words("english"))
    # print "stop words in NLTK (318 in Sci-kit):", len(stopwords.words(
    #     "english"))
    # # for tweet in tweets:
    # #     print tweet.text
    # # http://stats.stackexchange.com/questions/95797/how-to-split-the-dataset-for-cross-validation-learning-curve-and-final-evaluat
    # x_train = [t.bow for t in tweets]
    # y_train = [t.label for t in tweets]
    # from sklearn.multiclass import OneVsRestClassifier
    # # Initialize a Random Forest classifier with 100 trees
    # forest = RandomForestClassifier(n_estimators=100)
    # print type(forest)
    # forest = OneVsRestClassifier(forest)
    # print type(forest)
    # # Use 2-fold cross-validation
    # #scores = cross_val_score(forest, x_train, y_train, cv=2)
    # X = [t.bow for t in tweets]
    # y = [t.label for t in tweets]
    # print "X", X
    # print "y", y
    # X_train, X_test, y_train, y_test = train_test_split(X, y,
    #                                                     train_size=0.8)
    # print "X_train", X_train
    # print "X_test", X_test
    # print "y_train", y_train
    # print "y_test", y_test
    # forest.fit(X_train, y_train)
    # predicted = forest.predict(X_test)
    # # print forest.multilabel_
    # # TODO: compute scores here
    # score = forest.score(X_test, y_test)
    # print "predicted", predicted
    # print "true", y_test
    # score = accuracy_score(y_test, predicted)

    # print "score", score
    # http://scikit-learn.org/stable/auto_examples/classification
    # /plot_digits_classification.html#example-classification-plot-digits-classification-py
    # mean score and the 95% confidence interval of the score estimate are hence given by
    # print "Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2)

    # How to evaluate a classifier and how to optimize it?
    # http://stats.stackexchange.com/questions/136296/implementation-of-nested-cross-validation
    # To make computations more efficient, let's turn the list into a numpy
    # array
    # X = np.array(x_train)
    # y = np.array(y_train)
    # print X.shape, X
    # print y.shape, y
    # kf = KFold(len(x_train), 2)
    # outer_scores = []
    # # Apply nested CV to get an unbiased estimate of our model
    # # outer cross-validation
    # outer = KFold(len(y), n_folds=2, shuffle=True, random_state=42)
    # for fold, (train_index_outer, test_index_outer) in enumerate(outer):
    #     X_train_outer, X_test_outer = X[train_index_outer], X[test_index_outer]
    #     y_train_outer, y_test_outer = y[train_index_outer], y[test_index_outer]
    #
    #     inner_mean_scores = []
    #
    #     # define explored parameter space.
    #     # procedure below should be equal to GridSearchCV
    #     tuned_parameter = [1000, 1100, 1200]
    #     for param in tuned_parameter:
    #
    #         inner_scores = []
    #
    #         # inner cross-validation
    #         inner = KFold(len(X_train_outer), n_folds=2, shuffle=True,
    #                       random_state=40)
    #         for train_index_inner, test_index_inner in inner:
    #             # split the training data of outer CV
    #             X_train_inner, X_test_inner = X_train_outer[train_index_inner], X_train_outer[test_index_inner]
    #             y_train_inner, y_test_inner = y_train_outer[train_index_inner], y_train_outer[test_index_inner]
    #
    #             # fit extremely randomized trees regressor to training data of inner CV
    #             clf = OneVsRestClassifier(RandomForestClassifier(
    #                 n_estimators=100))
    #             # clf = ensemble.ExtraTreesRegressor(param, n_jobs=-1, random_state=1)
    #             clf.fit(X_train_inner, y_train_inner)
    #             inner_scores.append(clf.score(X_test_inner, y_test_inner))
    #
    #         # calculate mean score for inner folds
    #         inner_mean_scores.append(np.mean(inner_scores))
    #
    #     # get maximum score index
    #     index, value = max(enumerate(inner_mean_scores), key=operator.itemgetter(1))
    #
    #     print 'Best parameter of %i fold: %i' % (fold + 1, tuned_parameter[index])
    #
    #     # fit the selected model to the training set of outer CV
    #     # for prediction error estimation
    #     clf2 = OneVsRestClassifier(RandomForestClassifier(
    #         n_estimators=tuned_parameter[index]))
    #     # clf2 = ensemble.ExtraTreesRegressor(tuned_parameter[index], n_jobs=-1, random_state=1)
    #     clf2.fit(X_train_outer, y_train_outer)
    #     outer_scores.append(clf2.score(X_test_outer, y_test_outer))
    #
    # # show the prediction error estimate produced by nested CV
    # print 'Unbiased prediction error: %.2f (+/- %.2f)' % (np.mean(outer_scores),
    #                                            np.std(outer_scores))

    # # Now find best parameters using standard CV for the whole dataset
    # # The previous optimization heavily depends on the respective fold and
    # # using the configuration with highest values is 99.9% due to the
    # # training samples used in the fold, so we need to optimize the parameter
    # #  not only on a fold, but rather on the entire dataset. Thus, we need to
    # #  emply standard CV to come up with the optimal parameter setting.
    # tuned_parameter = [1000, 1100, 1200]
    # mean_scores = []
    # for param in tuned_parameter:
    #
    #     scores = []
    #
    #     # normal cross-validation
    #     kfolds = KFold(len(y), n_folds=3, shuffle=True,
    #                                     random_state=42)
    #     for train_index, test_index in kfolds:
    #         # split the training data
    #         X_train, X_test = X[train_index], X[test_index]
    #         y_train, y_test = y[train_index], y[test_index]
    #
    #         # fit extremely randomized trees regressor to training data
    #         clf2_5 = OneVsRestClassifier(RandomForestClassifier(
    #             n_estimators=param))
    #         # clf2_5 = ensemble.ExtraTreesRegressor(param, n_jobs=-1,
    #         #                                       random_state=42)
    #         clf2_5.fit(X_train, y_train)
    #         scores.append(clf2_5.score(X_test, y_test))
    #
    #     # calculate mean score for folds
    #     mean_scores.append(np.mean(scores))
    #
    # # get maximum score index
    # index, value = max(enumerate(mean_scores), key=operator.itemgetter(1))
    #
    # print 'Best parameter : %i' % (tuned_parameter[index])
    # clf = OneVsRestClassifier(RandomForestClassifier(
    #     n_estimators=tuned_parameter[index]))
    # # Now train on the whole dataset -> we already got our performance
    # # estimate in the nested CV loop
    # clf.fit(X, y)

    # Fit the forest to the training set, using the bag of words as
    # features and the sentiment labels as the response variable
    #
    # This may take a few minutes to run
    # forest = forest.fit(x_train, y_train)
    # print forest

