"""
Test interplay between all preprocessing methods.
"""
import codecs
import os

import urllib2
import urllib
import json
import gzip
import re
import string
import itertools
import warnings

from StringIO import StringIO
import nltk
import enchant
from nltk.corpus import wordnet as wn
from nltk.corpus import sentiwordnet as swn
import numpy
from nltk.tag.stanford import StanfordPOSTagger
from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords
from Data_task_4.models.containers import Tweet

from manual_improvements import LABELS, INTENSIFIERS, NEGATION_REPLACEMENTS, \
    EMOTICONS, REPLACEMENTS, SLANG
from Data_task_4.Temp.temp_main import multiple_replace


def lemmatize(sentence):
    stc_tokens = nltk.word_tokenize(sentence)
    return stc_tokens


def pos_tags(path, sentence):
    """
    Adds POS tags using the Stanford POS tagger.

    Parameters
    ----------
    path: Path to the jar file of the Stanford POS tagger.
    sentence: List of words representing a tweet.

    Returns
    -------
    List of (word, POS tag) tuples representing a tweet.

    """
    pos = StanfordPOSTagger(path +
                            'models/english-bidirectional-distsim.tagger',
                            path + 'stanford-postagger.jar')
    return pos.tag(sentence)


def spell_checking(w):
    """
    Takes a word as input and returns the corrected version. If no entry
    exists, it returns w.

    Parameters
    ----------
    w: Word to be corrected.

    Returns
    -------
    string.
    Corrected word.

    """
    d = enchant.Dict("en")
    if not d.check(w) and w != "can not":
        # First, remove letters that occur more than twice and check
        # again in the dictionary. If it still can't be found,
        # correct typos
        # Matches any characters that occurs > 2 times (case
        # insensitive)
        matcher = re.compile(r"(?i)([a-z])\1{2,}")
        matches = list(match.group() for match in matcher.finditer(w))
        # Replace each match with a single occurrence of the
        # elongated letter
        for m in matches:
            letter = m[0].lower()
            w = w.replace(m, letter)
        # Correct typos
        if not d.check(w) and w != "can not":
            try:
                first_sug = d.suggest(w)[0]
                return first_sug
                # print "word: {} suggestions: {}".\
                #     format(w, d.suggest(w))
            except IndexError:
                # Impossible to correct - just return the original word
                pass
    return w


def get_wsd_(text, website, lang, key):
    params = {
        "annRes": "WN",  # Use wordnet synsets
        "text": text,
        "lang": lang,
        "key": key

    }
    url = website + '?' + urllib.urlencode(params)
    print url
    request = urllib2.Request(url)
    request.add_header('Accept-encoding', 'gzip')
    response = urllib2.urlopen(request)
    if response.info().get('Content-Encoding') == 'gzip':
        buf = StringIO(response.read())
        f = gzip.GzipFile(fileobj=buf)
        data = json.loads(f.read())
        # print data
        synsetId = data[0].get('babelSynsetID', None)

        # retrieving data
        # for result in data:
        #     # retrieving token fragment
        #     tokenFragment = result.get('tokenFragment')
        #     tfStart = tokenFragment.get('start')
        #     tfEnd = tokenFragment.get('end')
        #     print str(tfStart) + "\t" + str(tfEnd)
        #
        #     # retrieving char fragment
        #     charFragment = result.get('charFragment')
        #     cfStart = charFragment.get('start')
        #     cfEnd = charFragment.get('end')
        #     print str(cfStart) + "\t" + str(cfEnd)
        #
        #     # retrieving BabelSynset ID
        #     synsetId = result.get('babelSynsetID')
        #     print synsetId


# https://github.com/kevincobain2000/sentiment_classifier/blob/master/scripts/senti_classifier
def disambiguate_word_senses(sentence, word):
    """
    Finds the most likely sense of a word given a sentence. Gives horrible
    results currently, so it's deactivated.

    Parameters
    ----------
    sentence: List of words comprising a tweet.
    word: Word for which the most likely sense has to be found given the
    sentence.

    Returns
    -------
    Synset.
    The most likely sense of a word given a sentence.
    """
    # Remove trailing whitespace at last position
    sentence = " ".join(sentence)[:-1]
    wordsynsets = wn.synsets(word)
    bestScore = 0.0
    result = None
    for synset in wordsynsets:
        # print "{}: {}".format(synset, synset.definition())
        for w in nltk.word_tokenize(sentence):
            score = 0.0
            for wsynset in wn.synsets(w):
                sim = wn.path_similarity(wsynset, synset)
                if sim is not None:
                    score += sim
            if score > bestScore:
                bestScore = score
                result = synset
    return result


def emoticons(text):
    """
    Returns
    -------
    List, int, int, int.
    List of strings describing the tweet, overall emoticon polarity (<0:
    negative, >0 positive), number of positive emoticons, number of negative
    emoticons.

    """
    intensifier = 0  # Will be used as multiplicator
    new_text = []
    match_found = False
    # Indicates label of last emoji found in the text
    last_emoji = 1
    # Count positive and negative emoticons
    positive_emojis = 0
    negative_emojis = 0
    for w in text:
        # Emoticons present in the word
        # existing_emojis = {}
        existing_emojis = []
        w_tmp = w
        word_polarity = 0
        for emoji in EMOTICONS.iterkeys():
            if emoji in w_tmp:
                # print "emoji {} in {}".format(emoji, w_tmp)
                # existing_emojis[emoji] = ""
                existing_emojis.append(emoji)
                match_found = True
                not_processed = True
                # Search for multiple occurrences of the emoticon
                while not_processed:
                    # Split at first match and process remainder separately
                    splitted = w_tmp.split(emoji, 1)
                    if EMOTICONS[emoji] > 0:
                        positive_emojis += 1
                    else:
                        negative_emojis += 1
                    word_polarity += EMOTICONS[emoji]
                    last_emoji = EMOTICONS[emoji]
                    w_tmp = ''.join(splitted)
                    if emoji not in w_tmp:
                        not_processed = False
        # Remove emoticons from message
        # print "existing emojis in {}: {}".format(w, existing_emojis)
        # print "polarity", word_polarity
        new_word = w
        for emoji in existing_emojis:
            new_word = new_word.replace(emoji, " ")
        new_word = new_word.strip()
        # Add word only if an emoticon exists in it
        if match_found:
            new_text.extend(new_word.split())
        # if w in EMOTICONS:
        #     print "emo found", w
        #     word_polarity = EMOTICONS[w]
        #     if EMOTICONS[emoji] > 0:
        #         positive_emojis += 1
        #     else:
        #         negative_emojis += 1
        #     last_emoji = EMOTICONS[w]
        if not match_found:
            new_text.append(w)
        match_found = False
        intensifier += word_polarity
        #print "new text", new_text
        #print "Sentiment extracted and Emoticons removed"
    # print "Sentence Polarity from Emoticons dectected: ", sentence_polarity
    # print "Emoticons removed: ", new_text
    # It could happen that intensifier is 0 at the end -> make it very small,
    #  either + or - depending on the last encountered emoticon
    if intensifier == 0:
        # Positive emoji that changed intensifier to 0
        if last_emoji > 0:
            intensifier = 1.01
        else:
            intensifier = -1.01
    return new_text, intensifier, positive_emojis, negative_emojis


def punctuation(text):
    #Removes punctuation and RT
    puncts = list(string.punctuation)
    intensifier = 1  # Will be used as multiplicator
    text_update = []
    punct_found = False

    for w, t in text:
        puncts_present = []
        w_tmp = w
        # Search for ocurrences (potentially multiple times in consecutive order)
        #  of "!" and "!?" or "?!" indicating intensification of polarity
        # matcher = re.compile(r"[\?!]+")
        # for m in matcher.finditer(w):
        #     match = m.group()
        #     print "punct", match
        #     intensifier += 1
        # Accounts for '!?' or '?!' by counting '!'
        intensifier += w.count("!")
        intensifier += w.count("??")
        for punct in puncts:
            if punct in w_tmp:
                puncts_present.append(punct)
                punct_found = True
                not_seen = True
                while not_seen:
                    splitted = w_tmp.split(punct, 1)
                    w_tmp = ''.join(splitted)
                    if punct not in w_tmp:
                        not_seen = False
        update_word = w
        for punct in puncts_present:
            # If word ends with "s'" (Andreas'), remove only "'"
            if punct == "'" and len(update_word) > 2 and update_word[-2] == \
                    "s" and update_word[-1] == "'":
                update_word = update_word[:-1]
            # If apostrophe, add NO whitespace except word ends with "s" (
            # possessive form)
            if punct == "'" and update_word[-1] != "s":
                update_word = update_word.replace(punct, "")
            # Word contains a single letter which isn't "a" or "i" or a number
            elif len(update_word) == 1 and (update_word.lower() != "a" or \
                update_word.lower() != "i" or not update_word.isdigit()):
                    update_word = ""
            else:
                update_word = update_word.replace(punct, " ")
        update_word_tmp = update_word.strip()
        updated_word = []
        # Check if one of the words now is a single letter after split ->
        # remove it
        for word in update_word_tmp.split():
            if len(word) == 1:
                if update_word_tmp.lower() == "a" or update_word_tmp.lower() \
                        == "i" or update_word_tmp.isdigit():
                    updated_word.append((word, t))
            else:
                updated_word.append((word, t))
        # Remove "RT" or "rt"
        cleaned = []
        for w_, t in updated_word:
            if w_ != "rt" and w_ != "RT":
                cleaned.append((w_, t))

        if punct_found:
            text_update.extend(cleaned)
        if not punct_found and w != "rt" and w != "RT":
            text_update.append((w, t))
        punct_found = False
    return text_update, intensifier


def get_polarity(synset, word, tag=None, pos_dict=None, neg_dict=None):
    """
    Parameters
    ----------
    synset: Synset for which the polarity should be obtained.
    word: Actual word for which polarity should be obtained.
    pos_dict: Dictionary containing additional positively connotated words with
              polarity scores that should be taken into account while
              computing sentence sentiment.
    neg_dict: Dictionary containing additional negatively connotated words with
              polarity scores that should be taken into account while
              computing sentence sentiment.
    tag: Part-of-speech tag of the word.

    Returns
    -------
    double, double, double, int, int, int, double, double, double.
    The double values specifying negative, neutral and positive sentiment in
    the range from 0-1, number of negative words, number of objective
    words, number of positive words,
    polarity sum of the negative words, polarity sum of the objective
    words, polarity sum of the positive words.
    A word is considered objective, if it's objective score is >= 0.8.

    """
    # Mapping from Penntree tags to senticwordnet tags
    # Penntree:
    # https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos
    # .html
    # Sentiwordnet:
    # http://stackoverflow.com/questions/10223314/using-sentiwordnet-3-0
    # Sentiwordnet also seems to find "s" if as POS tag "a" is specified
    TAGS = {
        # Adjectives
        "JJ": "a",
        "JJS": "a",
        "JJR": "a",
        # Nouns
        "NN": "n",
        "NNS": "n",
        "NNP": "n",
        "NNPS": "n",
        # Adverbs
        "RB": "r",
        "RBR": "r",
        "RBS": "r",
        # Verbs
        "VB": "v",
        "VBD": "v",
        "VBP": "v",
        "VBG": "v",
        "VBN": "v",
        "VBZ": "v",
    }
    pos = 0
    neg = 0
    obj = 0
    # sn = Senticnet()
    # concept = sn.concept(word)
    # Values in range [-1, 1]
    # Senticnet provides only a single polarity independent of the word sense
    #  -> difficult to use
    # polarity = sn.polarity("fuck")
    # if polarity > 0:
    #     pos += polarity
    # elif polarity < 0:
    #     neg += polarity
    # print "senticnet polarity:", polarity

    # Add polarity according to opinion dictionary created by Pang et al.
    # if neg_dict is not None:
    #     if word in neg_dict:
    #         neg += neg_dict[word]
    #         print "\nneg in pang's for word:", word
    # if pos_dict is not None:
    #     if word in pos_dict:
    #         pos += pos_dict[word]
    #         print "\npos in pang's for word:", word

    # name = synset.name()
    exists = None
    # if tag is not None:
    #     # print "POS TAG", tag
    #     if tag in TAGS:
    #         # print "pos", TAGS[tag], word
    #         # Stemming of word ISN'T necessary - done automatically by swn
    #         exists = swn.senti_synset(name, TAGS[tag])
    #         # print "with pos tag", exists
    # else:
    #     exists = swn.senti_synset(name)
    # # If polarity score exists
    # if exists:
    #     pos += exists.pos_score()
    #     neg += exists.neg_score()
    #     obj += exists.obj_score()
    pos_words = 0
    neg_words = 0
    obj_words = 0
    pos_words_sum = 0
    neg_words_sum = 0
    obj_words_sum = 0
    exists = swn.senti_synsets(word)
    # If polarity score exists
    if exists:
        # Use most common sense as word sense disambiguation performs poorly
        exists = exists[0]
        pos_polarity = exists.pos_score()
        neg_polarity = exists.neg_score()
        obj_polarity = exists.obj_score()
        pos += pos_polarity
        neg += neg_polarity
        obj += obj_polarity
        # Note that a word could be classified as objective AND
        # positive/negative at the same time. But since the objective score
        # of a word tends to be very high, it's difficult to avoid that. But
        # it would be possible to put the other 2 if-statements in an else
        # block to allow for disjoint counts
        if obj_polarity > 0.8:
            obj_words += 1
            obj_words_sum += obj_polarity
        # Count as positive word if positive polarity > negative polarity
        if pos_polarity > neg_polarity:
            pos_words += 1
            pos_words_sum += pos_polarity
            # print "{} is positive: neg:{} pos:{}".format(word, neg_polarity,
            #                                              pos_polarity)
        if pos_polarity < neg_polarity:
            neg_words += 1
            neg_words_sum += neg_polarity
            # print "{} is negative: neg:{} pos:{}".format(word, neg_polarity,
            #                                              pos_polarity)
    # print "polarity for {}: neg: {} obj: {} pos: {}".format(word, neg, obj,
    #                                                          pos)
    return neg, obj, pos, neg_words, obj_words, pos_words, neg_words_sum, \
        obj_words_sum, pos_words_sum


def read_opinion_words(p, pol_value):
    """
    Parameters
    ----------
    p: Path to file containing opinion words.
    pol_value: Polarity value to be assigned to all words from that file.

    Returns
    -------
    dict.
    Dictionary containing for each opinion word its polarity

    """
    words = {}
    with open(p, "rb") as f:
        for l in f:
            # No comments
            if l and not l.startswith(";"):
                # Get rid of "\n" characters
                words[l.rstrip("\r\n")] = pol_value
    return words


def negate_polarity(n, o, p):
    """
    Negates polarity by switching positive with negative polarity.

    Parameters
    ----------
    n: Negative polarity score.
    o: Objective polarity score.
    p: Positive polarity score.

    Returns
    -------
    double, double, double.
    The new negative, objective and positive polarity scores.

    """
    tmp = n
    n = p
    p = tmp
    return n, o, p


def get_polarity_from_misspelled_word(word, tag, full_sentence,
                                      sentence, pos_dict,
                                      neg_dict):
    """
    Tries to extract polarity from a misspelled word by first correcting it
    and then extracting polarity.

    Parameters
    ----------
    word: Word to be analyzed.
    tag: POS-tag.
    sentence: List of (word, POS tag) tuples representing a tweet.
    full_sentence: The context of this word, which is the whole sentence as a
    string.
    pos_dict: Dictionary containing words with positive polarity.
    neg_dict: Dictionary containing words with negative polarity.

    Returns
    -------
    double, double, double, int, int, int, double, double, double, string.
    Triple describing negative, objective and positive polarity of a given
    word, number of negative words, number of objective words, number of
    positive words, sum of the polarities of the negative words, sum of
    the polarities of the objective words, sum of the polarities of the positive
    words, and also the corrected <sentence>.
    """
    # Find the position where the corrected word should be inserted
    replace_idx = 0
    tag = ""
    for idx, pair in enumerate(sentence):
        word_ = pair[0]
        if word_ == word:
            replace_idx = idx
    word = spell_checking(word)
    sentence[replace_idx] = (word, tag)
    # wsd = disambiguate_word_senses(full_sentence, word)
    # If a word sense exists
    # if wsd is not None:
    n, o, p, neg_words, obj_words, pos_words, neg_words_sum, obj_words_sum, \
        pos_words_sum = get_polarity(None, word, tag, pos_dict, neg_dict)
    return n, o, p, neg_words, obj_words, pos_words, neg_words_sum, \
        obj_words, pos_words_sum, sentence


def elongated(word):
    """
    Determines whether a word is elongated or not.

    Parameters
    ----------
    word: Word to be investigated.

    Returns
    -------
    double, int
    Returns the frequency of all letters that are repeated more than 2 times
    consecutively and a counter for each elongated word. In case of no
    elongated letters, 1.0 is returned.

    """
    elongate = 1.0  # Will be a multiplicator
    elongate_cnt = 0
    # Match any letter that occurs more than twice in a row
    matcher = re.compile(r"(?i)([A-Za-z])\1{2,}")
    matches = list(match.group() for match in matcher.finditer(word))
    for match in matches:
        # print "elongated", match
        # Intensify by as many times the letter is repeated
        elongate += len(match)
    if len(matches) > 0:
        elongate_cnt += 1
    # Subtract 1 because we started with 1 in case we found elongated chars
    elongate = elongate - 1 if elongate > 1 else 1
    return elongate, elongate_cnt


def keep_sentiment_carrying_words(sentence, indices):
    """
    Retains only the sentiment carrying words of a sentence.

    Parameters
    ----------
    sentence: List of (word, POS tag) pairs representing a tweet.
    indices: Indices of the words from <sentence> to retain.

    Returns
    -------
    List.
    List of (word, POS tag) tuples representing a tweet. All of the remaining
    words bear sentiment.

    """
    cleaned = []
    for i in indices:
        # It's still a (word, tag) tuple
        cleaned.append(sentence[i])
    return cleaned


def determine_sentence_polarity(sentence, emoji_factor, punct_factor,
                                removed_words, pos_dict, neg_dict):
    """
    Computes the overall polarity of a sentence w.r.t. positive, neutral and
    negative sentiment.
    To do so, the following factors are considered:
        - it's a sliding windows approach
        - store polarity of the last (NEIGHBORS - 1) words
        - polarity of uppercase words is multiplied by 1.5
        - negations swap positive and negative word polarities
        - negations are valid for NEGATION_NEIGHBORS words
        - negation words are preceding words they affect
        - intensifies affect only succeeding words
        - the only intensifiers applied backwards are: "very" and a "bit"
        - intensifiers multiply a word's polarity by 1.5 or 0.5 if they are
          diminishers
        - double negations aren't handled yet
        - negation words' polarities are discarded
        - if sentence ends with "not", intensify prevalent polarity (either
          positive or negative)
        - intensify a word if it contains elongated characters (> 2 at a time)
    Double negations aren't handled yet, e.g. "I don't don't love you not"
    should be negative.

    Parameters
    ----------
    sentence: List of (word, POS tag) tuples representing tweet texts.
    emoji_factor: Factor with which the resulting polarity should be multiplied.
                  The way of multiplication depends on the emoticon(s).
    punct_factor: Factor with which all polarity should be multiplied.
    removed_words: Dictionary of words to be ignored in the process.
    pos_dict: Dictionary containing additional positively connotated words with
              polarity scores that should be taken into account while
              computing sentence sentiment.
    neg_dict: Dictionary containing additional negatively connotated words with
              polarity scores that should be taken into account while
              computing sentence sentiment.
    Returns
    -------
    (double, double, double, int, int, int, int, int, int, int, double, double,
    double, list).
    Triple containing the negative, objective and neutral score of the
    sentence normalized by the sentence length and normalized in the range [
    0,1], plus the number of elongated words in the tweet and the number of
    uppercase words, number of negative
    words, number of objective words, and number of positive words, number of
    polarity words in a tweet,
    number of negations in a tweet, sum of the polarities scores of the
    negative words, sum of the polarity scores of the objective words, sum of
    the polarity scores of the positive words. The sums are averaged.
    The list of words at the end represents a tweet containing only sentiment
    bearing words.

    """
    # TODO: Negate only words that make syntactically sense. See Gamallo,
    # "Citius: A Naive-Bayes Strategy for Sentiment Analysis on English Tweets"
    # for an idea

    # Consider (NEIGHBORS-1) preceding and succeeding words for negations and
    #  polarity extraction
    NEIGHBORS = 4
    # Consider same number of words as NEIGHBORS
    NEGATION_NEIGHBORS = NEIGHBORS
    # Total negative, objective, positive score of the sentence so far
    pos = 0.
    obj = 0.
    neg = 0.
    # Contains at most (NEIGHBOR-1) entries at a time: polarity scores of 3
    # previous words
    scores = []
    # pos_tags = get_pos_tags(sentence)
    # print pos_tags
    # True if a negation is currently active which implies to flip
    # encountered sentiment
    is_negated = False
    # Number of words whose polarity should be negated
    negated_words = NEGATION_NEIGHBORS
    # True, if an intensifier is encountered
    is_intensified = False
    # Intensifier is only valid up to (NEIGHBORS - 1) more words - if no
    # intensifier occurs, reset it.
    neighbor_words = NEIGHBORS
    # Weight with which to multiply the polarity of a word with intensifiers
    intensify = 1.
    # Previous word of the sentence
    prev_word = ""
    # Negative, objective and positive score of a word
    n = 0.
    o = 0.
    p = 0.
    # Number of positive words in a tweet
    positive_words = 0
    # Number of negative words in a tweet
    negative_words = 0
    # Number of objective words in a tweet
    objective_words = 0
    # Sum of positive_words
    positive_words_sum = 0.
    # Sum of negative_words
    negative_words_sum = 0.
    # Sum of objective words
    objective_words_sum = 0.
    # Get full sentence as a string
    full_sentence = " ".join([w for w, _ in sentence])[:-1]
    # Number of elongated words in the tweet
    elongated_counter = 0
    # Number of uppercase words in the tweet
    uppercase_counter = 0
    # Indices of words carrying sentiment -> retain only those
    sentiment_indices = []
    # Number of negations
    negations = 0
    idx = 0
    # Stop words
    stop = stopwords.words('english')
    for w, tag in sentence:
        tmp = w
        if tmp.lower() not in removed_words:
            upper_intensify = 1.0
            # print "current word:", w
            # Is it an uppercase word with more than 2 character? If so,
            # increase its polarity
            if w.isupper() and len(w) > 2:
                upper_intensify = 1.5
                uppercase_counter += 1
            w = w.lower()
            # Store positive polarity value of the most recent word to deal with
            # negations
            last_pos_score = 0
            # Store negative polarity value of the most recent word to deal with
            # negations
            last_neg_score = 0
            # Check if word contains elongated characters. If more than 2
            # consecutive ones, intensify word polarity
            elongated_intensify, el_cnt = elongated(w)
            elongated_counter += el_cnt
            # print "elongated intensifier:", elongated_intensify
            # Get word sense of word w
            # wsd = disambiguate_word_senses(full_sentence, w)
            # Add polarity score of word
            # if wsd is not None:
            # print "most likely sense:", wsd.definition()
            # Don't consider polarity of negators and intensifiers ->
            # they'll be handled separately later
            if w not in NEGATION_REPLACEMENTS and w not in INTENSIFIERS:
                n, o, p, neg_words, obj_words, pos_words, neg_sum, obj_sum, \
                    pos_sum = get_polarity(None, w, tag, pos_dict, neg_dict)
                positive_words += pos_words
                objective_words += obj_words
                negative_words += neg_words
                negative_words_sum += neg_sum
                objective_words_sum += obj_sum
                positive_words_sum += pos_sum
                # Store last polarity word
                last_pos_score = p
                last_neg_score = n
                to_store = True
                if n == 0 and o == 0 and p == 0:
                    to_store = False
                # Try to correct word and check whether polarity can then be
                # obtained
                # if elongated_intensify > 1 and not d.check(w):
                #     n, o, p = get_polarity_from_misspelled_word(
                #         wsd, w, tag, pos_dict, neg_dict)
                #     print "misspelled polarity", n, o, p
                # print "intensifier is currently", intensify
                # make sure that intensify is positive
                n *= abs(intensify) * upper_intensify * elongated_intensify
                o *= abs(intensify) * upper_intensify * elongated_intensify
                p *= abs(intensify) * upper_intensify * elongated_intensify
                # Word carries sentiment and isn't a stop word
                if w.lower() not in stop:
                    if to_store:
                        sentiment_indices.append(idx)
            # else:
                # print "no polarity computed because '{}' a negator or " \
                #       "intensifier.".format(w)
            # Misspelled word
            if elongated_intensify > 1:
                # Store how many elongated words exist
                # elongated_counter += elongated_intensify
                to_store = True
                n, o, p, neg_words, obj_words, pos_words, neg_sum, obj_sum, \
                    pos_sum, sentence = get_polarity_from_misspelled_word(
                            w, tag, full_sentence, sentence, pos_dict, neg_dict)
                positive_words += pos_words
                objective_words += obj_words
                negative_words += neg_words
                negative_words_sum += neg_sum
                objective_words_sum += obj_sum
                positive_words_sum += pos_sum
                # Store last polarity word
                last_pos_score = p
                last_neg_score = n
                if n == 0 and o == 0 and p == 0:
                    to_store = False
                # print "misspelled polarity", n, o, p
                # print "intensifier is currently", intensify
                n *= abs(intensify) * upper_intensify * elongated_intensify
                o *= abs(intensify) * upper_intensify * elongated_intensify
                p *= abs(intensify) * upper_intensify * elongated_intensify
                # Word carries sentiment and isn't a stop word
                if w.lower() not in stop:
                    if to_store:
                        sentiment_indices.append(idx)
            if is_negated:
                # print "normal polarity", n, o, p
                neighbor_words = NEIGHBORS
                # Negate only if the word isn't an intensifier
                if w not in NEGATION_REPLACEMENTS and w not in INTENSIFIERS:
                    # Switch positive and negative polarity
                    n, o, p = negate_polarity(n, o, p)
                    # If word was initially positive
                    if n > p:
                        positive_words -= 1
                        negative_words += 1
                        positive_words_sum -= last_pos_score
                        negative_words_sum += last_pos_score
                    # If word was initially negative
                    if n < p:
                        positive_words += 1
                        negative_words -= 1
                        positive_words_sum += last_neg_score
                        negative_words_sum -= last_neg_score
                    # print "negated polarity:", n, o, p
                # Make sure only (NEGATION_NEIGHBORS-1) words' polarities are
                # negated
                negated_words -= 1
            # If it's a negation, flip negative and positive sentiment
            if w in NEGATION_REPLACEMENTS:
                # print "negate polarity because of '{}'".format(w)
                # If already a negator within NEGATION_NEIGHBORS words exists,
                # it counts as a double negation
                # if is_negated:
                #     print "double negation detected!!!"
                #     is_negated = False
                # else:
                is_negated = True
                negations += 1
                negated_words = NEGATION_NEIGHBORS
            # print "append score for word {}: {}".format(w, (n,o,p))
            scores.append((n, o, p))

            # Don't intensify an intensifier itself, that's why add polarity first
            if w in INTENSIFIERS:
                # Look at the next NEIGHBORS words for intensification
                neighbor_words = NEIGHBORS
                is_intensified = True
                # print "{} is an intensifier with weight {}".format(w,
                                                                   # INTENSIFIERS[w])
                # Increase or decrease polarity words
                intensify += INTENSIFIERS[w]
                # Since it's a multiplicator, make sure it's different from 0
                if intensify == 0:
                    intensify = 0.01
                # In case of "very" or "a bit", adjust at most the previous (
                # NEIGHBOR-1) polarity scores or if there are less entries,
                # update them
                if (w == "very") or (w == "bit" and prev_word == "a"):
                    # print "succeding intensifier", w, "with value", intensify
                    # Number of preceding scores without the current intensifier
                    n_entries = len(scores) - 1
                    for i in xrange(0, n_entries):
                        n, o, p = scores[i]
                        n *= abs(intensify)
                        o *= abs(intensify)
                        p *= abs(intensify)
                        # print "update score at position {} from {} to {}".format(
                        #     i, scores[i], (n, o, p))
                        scores[i] = (n, o, p)

        # Make sure intensifier will be reset after (NEIGHBOR-1) more words
        if is_intensified:
            neighbor_words -= 1

        # Reset intensifier after (NEIGHBOR-1) words
        if neighbor_words == 0:
            is_intensified = False
            neighbor_words = NEIGHBORS
            intensify = 1.

        # Reset negation after (NEGATION_NEIGHBORS-1) words
        if negated_words == 0:
            # print "last negated word:", w
            is_negated = False
            negated_words = NEGATION_NEIGHBORS

        # Update the previously seen word to the current word
        prev_word = w

        # Move sliding window 1 position further, i.e. remove the leftmost score
        # if it is full
        if len(scores) == NEIGHBORS:
            score = scores.pop(0)
            neg += score[0]
            obj += score[1]
            pos += score[2]

        # For next word the polarities have to be reset
        n = 0.
        o = 0.
        p = 0.

        # Next word will be read
        idx += 1

    # Delete the words from the sentence that don't carry polarity
    text = keep_sentiment_carrying_words(sentence, sentiment_indices)
    # for t in text:
    #     assert(t not in removed_words)
    polarity_words = len(text)
    # Update the polarity scores
    # [(negative, objective, positive), (negative, objective, positive)...]
    for score in scores:
        neg += score[0]
        obj += score[1]
        pos += score[2]

    # Test if the last word of the sentence is a single "not" indicating the
    # whole sentence should be negated
    # Make sure that sentence isn't empty, e.g. with foreign language
    last_word = ""
    if len(sentence) > 0:
        last_word = sentence[-1][0]  # it's a (word, tag) pair
    last_word = last_word.lower()
    if last_word == "not":
        # print "sentence ends with 'not' -> flip polarity"
        # print "before flip"
        # print "pos: {}, obj: {}, neg: {}".format(pos, obj, neg)
        # Before switching, intensify the opposite polarity.
        if pos > neg:
            pos *= 1.5
            # print "positive polarity, hence multiply by", intensify
        else:
            neg *= 1.5
            # print "negative or objective polarity, hence multiply by", intensify
        tmp = pos
        pos = neg
        neg = tmp

    # Multiply sentence according to the occurrence of exclamation marks,
    # emoticons
    pos *= punct_factor
    neg *= punct_factor
    obj *= punct_factor
    # If positive emoticons outweigh negative ones, multiply positive sentiment
    if emoji_factor > 0:
        pos *= emoji_factor
    else:
        # Don't introduce negative values because we use Naive Bayes!
        neg *= -emoji_factor
    # print "pos: {}, obj: {}, neg: {}".format(pos, obj, neg)
    # Normalize polarity scores first by number of words in sentence and then
    #  restrict its range to [0, 1]
    if len(sentence) > 0:
        neg /= len(sentence)
        obj /= len(sentence)
        pos /= len(sentence)
    total = neg + obj + pos
    if total == 0:
        neg = pos = obj = 0.0
    else:
        neg /= total
        pos /= total
        obj /= total
    # Average the sums of positive/objective/negative words
    if negative_words > 0:
        negative_words_sum /= negative_words
    else:
        negative_words_sum = 0.0
    if objective_words > 0:
        objective_words_sum /= objective_words
    else:
        objective_words_sum = 0.0
    if positive_words > 0:
        positive_words_sum /= positive_words
    else:
        positive_words_sum = 0.0
    return neg, obj, pos, elongated_counter, \
        uppercase_counter, negative_words, objective_words, positive_words, \
        polarity_words, negations, negative_words_sum, objective_words_sum, \
        positive_words_sum, text


# Forward polarity computation -> ignore succeeding intensifiers, e.g. I love
#  you very much
# def determine_sentence_polarity(sentence, pos_dict, neg_dict):
#     """
#     Computes the overall polarity of a sentence w.r.t. positive, neutral and
#     negative sentiment.
#
#     Parameters
#     ----------
#     sentence: Tweet text.
#     pos_dict: Dictionary containing additional positively connotated words with
#               polarity scores that should be taken into account while
#               computing sentence sentiment.
#     neg_dict: Dictionary containing additional negatively connotated words with
#               polarity scores that should be taken into account while
#               computing sentence sentiment.
#     Returns
#     -------
#     double, double, double.
#     Triple containing the negative, objective and neutral score of the
#     sentence normalized by the sentence length.
#
#     """
#     NEIGHBORS = 4
#     pos = 0.
#     neg = 0.
#     obj = 0.
#     pos_tags = get_pos_tags(sentence)
#     print pos_tags
#     # True, if an intensifier is encountered
#     is_intensified = False
#     # Intensifier is only valid up to (NEIGHBORS - 1) more words - if no
#     # intensifier occurs, reset it.
#     neighbor_words = NEIGHBORS
#     # Weight with which to multiply the polarity of a word
#     intensify = 1.
#     for w, tag in pos_tags[0]:
#         print "current word:", w
#         wsd = disambiguateWordSenses(sentence[0], w)
#         if wsd is not None:
#             print "most likely sense:", wsd.definition()
#             n, o, p = get_polarity(wsd, w, tag, pos_dict, neg_dict)
#             print "intensifier is currently", intensify
#             neg += n * intensify
#             obj += o * intensify
#             pos += p * intensify
#         # Don't intensify the intensifier itself
#         if w in INTENSIFIERS:
#             is_intensified = True
#             print "{} is an intensifier".format(w)
#             intensify = INTENSIFIERS[w]
#         # Make sure intensifier will be reset after 2 more words
#         if is_intensified:
#             neighbor_words -= 1
#         # Reset intensifier after 2 words
#         if neighbor_words == 0:
#             is_intensified = False
#             neighbor_words = NEIGHBORS
#             intensify = 1.
#     return neg / len(sentence), obj / len(sentence), pos / len(sentence),


def determine_hashtag_polarity(hashtags, removed_words):
    """
    Computes the overall polarity of a hashtag w.r.t. positive, neutral and
    negative sentiment.
    To do so, the following factors are considered:
        - it's a sliding windows approach
        - store polarity of the last (NEIGHBORS - 1) words
        - polarity of uppercase words is multiplied by 1.5
        - negations swap positive and negative word polarities
        - negations are valid for NEGATION_NEIGHBORS words
        - negation words are preceding words they affect
        - intensifies affect only succeeding words
        - the only intensifiers applied backwards are: "very" and a "bit"
        - intensifiers multiply a word's polarity by 1.5 or 0.5 if they are
          diminishers
        - double negations aren't handled yet
        - negation words' polarities are discarded
        - if sentence ends with "not", intensify prevalent polarity (either
          positive or negative)
        - intensify a word if it contains elongated characters (> 2 at a time)
    Double negations aren't handled yet, e.g. "I don't don't love you not"
    should be negative.

    Parameters
    ----------
    hashtags: List of list of words representing the different hashtags. Each
              hashtag is represented by a list of words.
    removed_words: Dictionary of words to be ignored.

    Returns
    -------
    (double, double, double, int, int, int, int, int, double, double, double).
    Triple containing the negative, objective and neutral score of the
    sentence normalized by the sentence length and normalized in the range [
    0,1],
    plus the number of negative words,
    number of objective words and
    number of positive words,
    number of polarity words in the hashtags,
    number of negations in the hashtags,
    sum of the polarities scores of the negative words,
    sum of the polarity scores of the objective words,
    sum of the polarity scores of the positive words.
    The sums are averaged over the number of words existing for the type of
    the sum.

    """
    # Total negative, objective and positive score of all hashtags in a tweet
    negative = 0.
    objective = 0.
    positive = 0.
    # Number of polarity words (= entry in sentiwordnet exists)
    polarity_words = 0
    # Number of positive words in a tweet
    positive_words = 0
    # Number of objective words in a tweet
    objective_words = 0
    # Number of negative words in a tweet
    negative_words = 0
    # Sum of positive_words
    positive_words_sum = 0.
    # Sum of objective words
    objective_words_sum = 0.
    # Sum of negative_words
    negative_words_sum = 0.
    # Number of negations
    negations = 0
    # POS-tags aren't used at the moment anyway
    tag = "NN"
    # Total words in hashtags
    words = 0
    # Stop words
    stop = stopwords.words('english')
    for hashtag in hashtags:
        # Contains at most (NEIGHBOR-1) entries at a time: polarity scores of 3
        # previous words
        scores = []
        # Consider (NEIGHBORS-1) preceding and succeeding words for negations
        # and polarity extraction
        NEIGHBORS = 4
        # Consider same number of words as NEIGHBORS
        NEGATION_NEIGHBORS = NEIGHBORS
        # True if a negation is currently active which implies to flip
        # encountered sentiment
        is_negated = False
        # Number of words whose polarity should be negated
        negated_words = NEGATION_NEIGHBORS
        # True, if an intensifier is encountered
        is_intensified = False
        # Intensifier is only valid up to (NEIGHBORS - 1) more words - if no
        # intensifier occurs, reset it.
        neighbor_words = NEIGHBORS
        # Weight with which to multiply the polarity of a word with intensifiers
        intensify = 1.
        # Previous word of the sentence
        prev_word = ""
        for w in hashtag:
            # Negative, objective, positive polarity for current word
            n = 0.
            o = 0.
            p = 0.
            words += 1
            w = w.lower()
            if w not in removed_words:
                # Store positive polarity value of the most recent word to deal
                # with negations
                last_pos_score = 0
                # Store negative polarity value of the most recent word to deal
                # with negations
                last_neg_score = 0
                # Check if word contains elongated characters. If more than 2
                # consecutive ones, intensify word polarity
                # Don't consider polarity of negators and intensifiers ->
                # they'll be handled separately later
                if w not in NEGATION_REPLACEMENTS and w not in INTENSIFIERS:
                    n, o, p, neg_words, obj_words, pos_words, neg_sum, \
                        obj_sum, pos_sum = get_polarity(None, w, tag)
                    positive_words += pos_words
                    objective_words += obj_words
                    negative_words += neg_words
                    negative_words_sum += neg_sum
                    objective_words_sum += obj_sum
                    positive_words_sum += pos_sum
                    # Store last polarity word
                    last_pos_score = p
                    last_neg_score = n
                    n *= abs(intensify)
                    o *= abs(intensify)
                    p *= abs(intensify)
                    if n != 0 or o != 0 or p != 0:
                        if w not in stop:
                            polarity_words += 1
                if is_negated:
                    # print "normal polarity", n, o, p
                    neighbor_words = NEIGHBORS
                    # Negate only if the word isn't an intensifier
                    if w not in NEGATION_REPLACEMENTS and w not in INTENSIFIERS:
                        # Switch positive and negative polarity
                        n, o, p = negate_polarity(n, o, p)
                        # If word was initially positive
                        if n > p:
                            positive_words -= 1
                            negative_words += 1
                            positive_words_sum -= last_pos_score
                            negative_words_sum += last_pos_score
                        # If word was initially negative
                        if n < p:
                            positive_words += 1
                            negative_words -= 1
                            positive_words_sum += last_neg_score
                            negative_words_sum -= last_neg_score
                    # Make sure only (NEGATION_NEIGHBORS-1) words' polarities
                    # are negated
                    negated_words -= 1
                # If it's a negation, flip negative and positive sentiment
                if w in NEGATION_REPLACEMENTS:
                    # print "negate polarity because of '{}'".format(w)
                    # If already a negator within NEGATION_NEIGHBORS words exists,
                    # it counts as a double negation
                    # if is_negated:
                    #     print "double negation detected!!!"
                    #     is_negated = False
                    # else:
                    is_negated = True
                    negations += 1
                    negated_words = NEGATION_NEIGHBORS
                scores.append((n, o, p))

                # Don't intensify an intensifier itself, that's why add polarity
                # first
                if w in INTENSIFIERS:
                    # Look at the next NEIGHBORS words for intensification
                    neighbor_words = NEIGHBORS
                    is_intensified = True
                    # Increase or decrease polarity words
                    intensify += INTENSIFIERS[w]
                    # Since it's a multiplicator, make sure it's different
                    # from 0
                    if intensify == 0:
                        intensify = 0.01
                    # In case of "very" or "a bit", adjust at most the previous
                    # (NEIGHBOR-1) polarity scores or if there are less entries,
                    # update them
                    if (w == "very") or (w == "bit" and prev_word == "a"):
                        # print "succeding intensifier", w, "with value",
                        # intensify Number of preceding scores without the
                        # current intensifier
                        n_entries = len(scores) - 1
                        for i in xrange(0, n_entries):
                            n, o, p = scores[i]
                            n *= abs(intensify)
                            o *= abs(intensify)
                            p *= abs(intensify)
                            # print "update score at position {} from {} to {}
                            # ".format(i, scores[i], (n, o, p))
                            scores[i] = (n, o, p)

            # Make sure intensifier will be reset after (NEIGHBOR-1) more words
            if is_intensified:
                neighbor_words -= 1

            # Reset intensifier after (NEIGHBOR-1) words
            if neighbor_words == 0:
                is_intensified = False
                neighbor_words = NEIGHBORS
                intensify = 1.

            # Reset negation after (NEGATION_NEIGHBORS-1) words
            if negated_words == 0:
                # print "last negated word:", w
                is_negated = False
                negated_words = NEGATION_NEIGHBORS

            # Update the previously seen word to the current word
            prev_word = w

            # Move sliding window 1 position further, i.e. remove the leftmost
            # score if it is full
            if len(scores) == NEIGHBORS:
                score = scores.pop(0)
                negative += score[0]
                objective += score[1]
                positive += score[2]

        # Test if the last word of the sentence is a single "not"
        # indicating the whole sentence should be negated
        last_word = hashtag[-1]
        last_word = last_word.lower()
        if last_word == "not":
            # print "sentence ends with 'not' -> flip polarity"
            # print "before flip"
            # print "pos: {}, obj: {}, neg: {}".format(pos, obj, neg)
            # Before switching, intensify the opposite polarity.
            if positive > negative:
                positive *= 1.5
            else:
                negative *= 1.5
            tmp = positive
            positive = negative
            negative = tmp

        # Update the polarity scores
        # [(negative, objective, positive), (negative, objective, positive)...]
        for score in scores:
            negative += score[0]
            objective += score[1]
            positive += score[2]

    # Normalize polarity scores first by number of words in sentence and then
    #  restrict its range to [0, 1]
    if words > 0:
        negative /= words
        objective /= words
        positive /= words
    total = negative + objective + positive
    if total == 0:
        negative = positive = objective = 0
    else:
        negative /= total
        positive /= total
        objective /= total
    # Average the sums of positive/objective/negative words
    if negative_words > 0:
        negative_words_sum /= negative_words
    else:
        negative_words_sum = 0.0
    if objective_words > 0:
        objective_words_sum /= objective_words
    else:
        objective_words_sum = 0.0
    if positive_words > 0:
        positive_words_sum /= positive_words
    else:
        positive_words_sum = 0.0
    return negative, objective, positive, negative_words, objective_words, \
        positive_words, polarity_words, negations, negative_words_sum, \
        objective_words_sum, positive_words_sum


def get_pos_tags(tweet):
    """
    Preprocesses a list of tweet messages and returns the NER representation.
    """
    pos = []
    # path1 = '/home/mishalkazmi/NLP/PyCharm/inspire/semeval/task2/resources/stanford-postagger-full-2015-04-20/'
    # path2 = '/home/mishalkazmi/NLP/PyCharm/inspire/semeval/task2/resources/stanford-ner-2015-04-20/'

    path1 = '/home/fensta/Stanford/stanford-postagger-full-2015-04-20/'
    sentences = tweet.split(".")
    for stc in sentences:
        pos.extend(pos_tags(path1, stc.split()))
    return pos


def remove_hash(text, dic):
    """
    If hashtag is comprised of a single known word, it is kept without
    hashtag. Same goes for the case when the hashtag is written in camel case.
    Otherwise the hashtag is tokenized either by an exhaustive search (if
    it's shorter than 21 letters) or in a greedy manner otherwise.

    Returns
    -------
    list, list.
    List of words representing the tweet and a separate
    list of words extracted from hashtags. In the latter, each entry
    corresponds to a hashtag.

    """
    cleaned = []
    # List of words extracted from hashtags. Each entry corresponds to a
    # separate hashtag
    hashtags = []
    # Punctuation
    # puncts = list(string.punctuation)
    for w in text:
        if w.startswith("#"):
            # Is it a single word?
            w = w[1:]
            # Make sure that something comes after '#'
            if len(w) > 0:
                # Does the word exist in the dictionary?
                if dic.check(w):
                    cleaned.append(w)
                    hashtags.append([w])
                else:
                    # Is it CamelCase? Word must contain at least 2 capitalized
                    # letters
                    camel_string = ""
                    is_camel = False
                    # And first character is uppercase while 2nd is lowercase
                    if len(w) > 1 and w[0].isupper() and w[1]:
                        for l in w[1:]:
                            if l.isupper():
                                is_camel = True
                                break
                    if is_camel:
                        for l in w:
                            if l.isupper():
                                l = " " + l
                            camel_string += l
                        cleaned.extend(camel_string.strip().split())
                        hashtags.append(camel_string.split())
                    else:
                        # Multiple words in hashtag
                        # Optimal solution if hashtag is sufficiently short
                        if len(w) < 22:
                            result = tokenize_hashtag(w, dic)
                        else:
                            result = tokenize_hashtag_greedy(w, dic)
                        if result is not None:
                            hashtags.append(result)
                            cleaned.extend(result)
        else:
            cleaned.append(w)
    return cleaned, hashtags


# http://stackoverflow.com/questions/18406776/python-split-a-string-into-all-
# possible-ordered-phrases
def candidates(word):
    """
    Finds all potential splits of a string.

    Parameters
    ----------
    word: String word.

    Returns
    -------
    Iterator.
    Each combination of splits of a given word.

    """
    # n = 1..(n-1)
    ns = range(1, len(word))
    # split into 2, 3, 4, ..., n parts.
    for n in ns:
        for idxs in itertools.combinations(ns, n):
            yield [''.join(word[i:j]) for i, j in zip((0,) + idxs, idxs + (
                None,))]


def tokenize_hashtag(w, dic):
    """
    Tokenizes a multi-word hashtag that isn't written in camelcase.

    Parameters
    ----------
    w: Word which represents a hashtag without the hash.
    dic: PyEnchant Dic representing the language to be expected.

    Returns
    -------
    List.
    Tokenized hashtag as a list of words. If no such tokenization can be
    found, None is returned.

    """
    likely = []
    for candidate in candidates(w):
        to_add = []
        is_candidate = True
        for w in candidate:
            is_correct = False
            # Replace slang and expand negations
            if w in NEGATION_REPLACEMENTS:
                is_correct = True
                w = NEGATION_REPLACEMENTS[w]
            if w in SLANG:
                is_correct = True
                w = SLANG[w]
            # Replaced words don't need to be checked
            if not is_correct:
                # If it's a single letter word or not present in the dictionary
                if not dic.check(w) or w.lower() in "bcdfghjklmnopqrstvwyz":
                    is_candidate = False
                    break
                else:
                    # Valid word
                    to_add.append(w)
            else:
                # Valid word after replacement -> might contain multiple words
                to_add.extend(w.split())
        if is_candidate:
            likely.append(to_add)
    # If a combination of valid words was found, use the one with the lowest
    # number of words
    if len(likely) > 0:
        # Use the candidate with lowest number of words
        shortest = 100000000
        for idx, candidate in enumerate(likely):
            if len(candidate) < shortest:
                shortest = idx
        return likely[shortest]
    return None


def tokenize_hashtag(w, dic):
    """
    Tokenizes a multi-word hashtag that isn't written in camelcase.

    Parameters
    ----------
    w: Word which represents a hashtag without the hash.
    dic: PyEnchant Dic representing the language to be expected.

    Returns
    -------
    List.
    Tokenized hashtag as a list of words. If no such tokenization can be
    found, None is returned.

    """
    likely = []
    for candidate in candidates(w):
        to_add = []
        is_candidate = True
        for w in candidate:
            is_correct = False
            # Replace slang and expand negations
            if w in NEGATION_REPLACEMENTS:
                is_correct = True
                w = NEGATION_REPLACEMENTS[w]
            if w in SLANG:
                is_correct = True
                w = SLANG[w]
            # Replaced words don't need to be checked
            if not is_correct:
                # If it's a single letter word or not present in the dictionary
                if not dic.check(w) or w.lower() in "bcdfghjklmnopqrstvwyz":
                    is_candidate = False
                    break
                else:
                    # Valid word
                    to_add.append(w)
            else:
                # Valid word after replacement -> might contain multiple words
                to_add.extend(w.split())
        if is_candidate:
            likely.append(to_add)
    # If a combination of valid words was found, use the one with the lowest
    # number of words
    if len(likely) > 0:
        # Use the candidate with lowest number of words
        shortest = 100000000
        for idx, candidate in enumerate(likely):
            if len(candidate) < shortest:
                shortest = idx
        return likely[shortest]
    return None


def tokenize_hashtag(w, dic):
    """
    Tokenizes a multi-word hashtag that isn't written in camelcase.

    Parameters
    ----------
    w: Word which represents a hashtag without the hash.
    dic: PyEnchant Dic representing the language to be expected.

    Returns
    -------
    List.
    Tokenized hashtag as a list of words. If no such tokenization can be
    found, None is returned.

    """
    likely = []
    for candidate in candidates(w):
        to_add = []
        is_candidate = True
        for w in candidate:
            is_correct = False
            # Replace slang and expand negations
            if w in NEGATION_REPLACEMENTS:
                is_correct = True
                w = NEGATION_REPLACEMENTS[w]
            if w in SLANG:
                is_correct = True
                w = SLANG[w]
            # Replaced words don't need to be checked
            if not is_correct:
                # If it's a single letter word or not present in the dictionary
                if not dic.check(w) or w.lower() in "bcdfghjklmnopqrstvwyz":
                    is_candidate = False
                    break
                else:
                    # Valid word
                    to_add.append(w)
            else:
                # Valid word after replacement -> might contain multiple words
                to_add.extend(w.split())
        if is_candidate:
            likely.append(to_add)
    # If a combination of valid words was found, use the one with the lowest
    # number of words
    if len(likely) > 0:
        # Use the candidate with lowest number of words
        shortest = 100000000
        for idx, candidate in enumerate(likely):
            if len(candidate) < shortest:
                shortest = idx
        return likely[shortest]
    return None


def tokenize_hashtag_greedy(w, dic):
    """
    Tokenizes a multi-word hashtag that isn't written in camelcase greedily.

    Take the longest word starting from the back of a string (which exists in a
    dictionary) and traverse toward the beginning. Could also return partial
    words and not the full hashtag.

    Parameters
    ----------
    w: Word which represents a hashtag without the hash.
    dic: PyEnchant Dic representing the language to be expected.

    Returns
    -------
    List.
    Tokenized hashtag as a list of words. If no such tokenization can be
    found, a partial solution is returned.

    """
    result = []
    while len(w) > 0:
        # Index indicating the longest word that existed in the dictionary
        max_ = 0
        word = ""
        for idx, char in enumerate(w[::-1]):
            word = char.lower() + word
            # If it's a sufficiently long word (exceptions: I, a) and it's valid
            if (len(word) > 1 or word == "i" or word == "a") and dic.check(
                    word):
                max_ = idx
        longest_word = w[-max_-1:]
        # If it's not an invalid word, add it
        if max_ != 0:
            result.append(longest_word)
        w = w[:-max_-1]
    return result


def remove_stop_words(text, removed_words):
    """
    Removes stop words from a tweet. Careful, words must be in lowercase to
    be detected as stop words. Also, words that don't correlate with a class
    (according to CPD) will be removed.

    Parameters
    ----------
    text: List of (word, POS tag) tuples representing a tweet.
    removed_words: Dictionary of words that should be ignored as they don't
    correlate with at least a class.

    Returns
    -------
    List.
    List of (word, POS tag) tuples representing a list. All stop words are
    removed.

    """
    stop = stopwords.words('english')
    cleaned = [w for w in text if w not in stop]
    cleaned = [w for w in cleaned if w not in removed_words]
    return cleaned


def remove_url(text):
    # Remove URLs
    cleaned = [w for w in text if not w.startswith('http') and not
    w.startswith('https')]
    # print "Removed URLs: ", cleanURL
    return cleaned


def remove_twitter_handle(text):
    """
    Removes all Twitter handles (@xxx) from tweets.

    Parameters
    ----------
    text: List of strings representing a tweet.

    Returns
    -------
    List.
    List of words representing a tweet without any Twitter handles.

    """
    cleaned = [w for w in text if not w.startswith('@')]
    return cleaned


def expand_negations(words, rep_dict):
    """
    Replaces short-forms of negations by long forms, e.g. can't -> can not.
    """
    expanded = []
    for w in words:
        for neg in rep_dict:
            if w.lower() == neg:
                w = rep_dict[neg]
        expanded.append(w)
    return expanded


def to_lower(text):
    """
    Turns any word of a tweet into lowercase.

    Parameters
    ----------
    text: List of words representing a tweet.

    Returns
    -------
    List.
    List of words in lowercase representing tweet message.

    """
    low = []
    for w in text:
        low.append(w.lower())
    return low


def separate_punctuation(sentence):
    """
    Adds a whitespace between a word and punctuation if there's no whitespace
    inbetween.

    Parameters
    ----------
    sentence: A tweet message as string.

    Returns
    -------
    List.
    The list of words representing the message with additional whitespaces
    between punctuation and words, so that they can be analyzed more easily.

    """
    # Add a whitespace between a word and a punctuation group if it doesn't
    # exist yet
    txt = re.sub(r"([a-zA-Z])([,.!-;:()=\?*]+)", r'\1 \2 ', sentence)
    # Do it also for the inverse case
    txt = re.sub(r"([,.!;=:()-\?*])([a-zA-Z]+)", r'\1 \2 ', txt)
    return txt.split()


def remove_numbers(sentence):
    """
    Remove any positive or negative number from a text and replace it by
    nothing. Also remove ordinals.

    Parameters
    ----------
    sentence: Sentence as a string describing a tweet.

    Returns
    -------
    String.
    Sentence without any numbers.

    """
    s = re.sub(r"(?<=[0-9])(?:st|nd|rd|th|)", "", sentence)
    return re.sub(r"[-+]?\d+[\.,]?\d*", "", s)


def remove_all_words_except_(text, tags={"RB": None, "JJ": None, "VB": None}):
    """

    Parameters
    ----------
    text: List of (word, POS tag) tuples describing
    tags: Dictionary of POS-tags to be kept - any word with other POS-tags
          will be removed.

    Returns
    -------
    List.
    List of words representing a tweet containing only
    adjectives and adverbs.

    """
    cleaned = []
    for word, tag in text:
        tag = tag[:2]
        if tag in tags:
            cleaned.append(word)
    return cleaned


def replace_slang(text, subst):
    """
    Replaces slang words with a given substitution in a tweet.

    Parameters
    ----------
    text: List of words representing a tweet.
    subst: Dictionary containing substitutions for each slang word.

    Returns
    -------
    Text without slang.

    """
    corrected = []
    for word in text:
        # Don't transform original word into lowercase
        w = word.lower()
        for slang in subst:
            if w == slang:
                word = subst[slang]
        corrected.append(word)
    return corrected


def dummy_preprocess(text):
    """ Returns pseudo results because it'll only be invoked if no
    preprocessing is desired for an experiment such that the text is directly
    converted into BoW. """
    return text, 1, 1, 1, 1, 1, 1, 0.25, 0.25, 0.25, 0.25, get_dummy_polarity(
        text)


def get_dummy_polarity(text):
    """
    Returns dummy data making sure that preprocess works as expected.
    """
    return 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "BLA"


def stem(text, stemmer=None, lang=None):
    """
    Stems the tweets as a pre-processing step.

    Parameters
    ----------
    text: A list of strings describing a single tweet.t
    stemmer: Name of the stemmer, either None, "porter" or "snowball". If
    it's None, no stemming will be performed.
    lang: Language to be used in stemmer, e.g. "english".

    Returns
    -------
    List.
    Returns a list of strings representing the stemmed tweet.

    """
    if stemmer is not None:
        if stemmer == "porter":
            stemmer_ = SnowballStemmer("porter")
        elif lang is not None:
            stemmer_ = SnowballStemmer(lang)
        else:
            warnings.warn(
                "No stemmer language was specified!. Let's use English.")
            stemmer_ = SnowballStemmer("english")
        return [stemmer_.stem(word) for word in text]
    return text


def get_pseudo_pos_tags(text):
    """ Assign pseudo POS-tags to a tweet if this option isn't chosen by a
    user for the sake of compatibility with the following functions."""
    return [(w, "none") for w in text]


def get_pos_percentages(text):
    """
    Calculates the number of adjectives, nouns, verbs and nouns in a whole
    tweet.

    Parameters
    ----------
    text: List of words describing a tweet.

    Returns
    -------
    double, double, double, double.
    Number of adjectives, adverbs, verbs, nouns in the sentence.

    """
    pos = {"JJ": 0.0, "RB": 0.0, "VB": 0.0, "NN": 0.0}
    for word, tag in text:
        # Consider only the first 2 letters: JJ = adjective, RB = adverb,
        # VB = verb, NN = noun
        tag = tag[:2]
        if tag in pos:
            pos[tag] += 1
    return pos["JJ"], pos["RB"], pos["VB"], pos["NN"]


def get_median(l):
    """ Returns median for a given list with words"""
    return numpy.median(numpy.array(l))


def scale(lst, l, u):
    """
    Scales all values in a list into a specific range (including boundaries).

    Parameters
    ----------
    lst: List of values to be scaled.
    l: Lower bound.
    u: Upper bound.

    Returns
    -------
    List.
    List with scaled values.

    """
    to_min = l
    to_max = u
    # AlchemyAPI values are always in this range
    from_min = -1
    from_max = 1
    scaled = []
    for x in lst:
        x_ = (x - from_min) * (to_max - to_min) / (from_max - from_min) + to_min
        scaled.append(x_)
    return scaled


def get_sentiment_from_alchemyapi(alchemy_dir, tweet):
    """
    Extracts the sentiment of the tweet, entities, relations and keywords
    using AlchemyAPI.

    Parameters
    ----------
    alchemy_dir: Base directory in which all tweet information from
    AlchemyAPI is stored.
    tweet: Tweet object.

    Returns
    -------
    double, double, double, double, int, double, double, double, double.
    Returns two value for each of the 4 types, where the first value
    represents the averaged sentiment, while the second one refers to the
    type, i.e. positive, neutral or negative. 3 represents positive,
    2 neutral and 1 negative. 4 indicates that no information was available
    from AlchemyAPI. This value is chosen as median.
    The order of the results is as follows:
    1. tweet sentiment score
    2. median tweet sentiment type
    3. averaged entity sentiment score
    4. median entity sentiment type
    5. number of positive entities in tweet
    6. number of neutral entities in tweet
    7. number of negative entities in tweet
    8. does a tweet contain mixed sentiment? 1 if yes, 0 if no
    9. averaged keyword score
    10. median keyword score
    11. averaged relations score
    12. median relations score

    For more information, have a look at
    experiments.extract_sentiment_via_alchemyapi.py

    """
    entity_types = []
    entity_polarity_scores = []
    # Set it to a value outside the range
    entity_type = 4.0
    entity_polarity = 4.0
    tweet_type = 4.0
    tweet_polarity = 4.0
    keyword_type = 4.0
    keyword_sentiment = 4.0
    relations_type = 4.0
    relations_sentiment = 4.0
    mixed_sentiment = 0
    positive_entities = 0
    negative_entities = 0
    neutral_entities = 0
    MAPPING = {"positive": 3, "neutral": 2, "negative": 1}
    src = os.path.join(alchemy_dir, tweet.tweet_id + ".txt")
    if os.path.isfile(src):
        print "open stored AlchemyAPI file", src
        with codecs.open(src, "r", "utf-8-sig") as f:
            stored_metadata = json.load(f)

        # 1. Extract entity sentiment
        for entity in stored_metadata["entities"]:
            # Could be empty
            if "sentiment" in entity:
                sentiment = entity["sentiment"]
                if "type" in sentiment:
                    entity = MAPPING[sentiment["type"]]
                    entity_types.append(entity)
                    if sentiment["type"] == "negative":
                        negative_entities += 1
                    if sentiment["type"] == "neutral":
                        neutral_entities += 1
                    if sentiment["type"] == "positive":
                        positive_entities += 1
                if "score" in sentiment:
                    score = float(sentiment["score"])
                    entity_polarity_scores.append(score)
        # Could be empty
        if len(entity_types) > 0:
            entity_type = get_median(entity_types)
        scaled_entitiy_polarity_scores = scale(entity_polarity_scores, 0, 1)
        # Could be empty
        if len(scaled_entitiy_polarity_scores) > 0:
            entity_polarity = sum(scaled_entitiy_polarity_scores) / len(
                scaled_entitiy_polarity_scores)

        # 2. Extract overall tweet sentiment
        # Could be empty
        if "score" in stored_metadata["docSentiment"]:
            tmp_tweet_polarity = float(stored_metadata["docSentiment"]["score"])
            # There's only 1 score for overall sentiment available
            tweet_polarity = scale([tmp_tweet_polarity], 0, 1)[0]
        # Could be empty
        if "type" in stored_metadata["docSentiment"]:
            tweet_type = MAPPING[stored_metadata["docSentiment"]["type"]]
        if "mixed" in stored_metadata["docSentiment"]:
            mixed_sentiment = 1
        # 3. Extract keyword sentiment

    # 4. Extract relations sentiment
    return tweet_polarity, tweet_type, entity_polarity, entity_type, \
           negative_entities, neutral_entities, positive_entities,\
           mixed_sentiment, keyword_sentiment, keyword_type, \
           relations_sentiment, relations_type


def preprocess_cpd(tweets, options):
    """
    Preprocesses tweets before applying CPD. These are the same steps used
    later on for real preprocessing, only polarity extraction and POS-tagging
    isn't used. The preprocessed text is exposed via the attribute
    "preprocessed_cpd" of the Tweet objects.

    Parameters
    ----------
    tweets: List of Tweet objects.
    options: Option object containing all parameters for preprocessing.

    """
    for tweet in tweets:
        text = tweet.text
        d = enchant.Dict("en")
        text = multiple_replace(text, REPLACEMENTS)
        text = text.split()
        text = replace_slang(text, SLANG)
        text = expand_negations(text, NEGATION_REPLACEMENTS)
        text = remove_numbers(" ".join(text))
        text = text.split()
        if options.remove_urls:
            text = remove_url(text)
        text = separate_punctuation(" ".join(text))
        if options.remove_handles:
            text = remove_twitter_handle(text)
        if options.remove_hashtags:
            # Slang and shortened negations have already been replaced by correct
            # versions in hashtags
            text, hashtags = remove_hash(text, d)
        if options.use_emojis:
            text, intensify_emoji, pos, neg = emoticons(text)

        # If no POS-tags should be extracted, use pseudo ones as the following
        # functions expect POS tuples
        if not options.use_pos_tags and options.remove_punctuation:
            text = get_pseudo_pos_tags(text)
            text, intensify_punct = punctuation(text)

        # Store preprocessed text for CPD
        tweet.text_cpd = " ".join(text)

# TODO: rewrite preprocess to process all tweets at once because POS tagging
# is fast in batch mode!!!!
def preprocess(tweet, options, removed_words):
    """
    Preprocesses a single tweet.

    Parameters
    ----------
    tweet: Tweet object.
    options: Option object containing all parameters for preprocessing.
    removed_words: Dictionary containing words to be ignored when extracting
                   polarities.

    Returns
    -------
    (String, int, int, int, int, int, double, int, (double, double, double, int,
    int, int, int, double, double), (double, double, double, int, int, int,
    int, int, int).)
    The tweet text, number of positive emoticons, number of negative
    emoticons, number of exclamation/question marks,
    20 values describing:
    - tweet text
    - number of positive emoticons
    - number of negative emoticons
    - number of exclamation marks/question marks
    - length of tweet before preprocessing (based on words)
    - length of tweet after preprocessing (based on words)
    - average sentence length (based on words per sentence)
    - normalized percentage of adjectives
    - normalized percentage of adverbs
    - normalized percentage of verbs
    - normalized percentage of nouns
    - hashtag(s) are present in tweets (0 or 1)
    - tweet polarity score according to AlchemyAPI (normalized [0-1])
    - tweet type according to AlchemyAPI
    - average entity polarity score according to AlchemyAPI (normalized [0-1])
    - median entity polarity type according to AlchemyAPI
    - number of negative entities in tweet as per AlchemyAPI
    - number of neutral entities in tweet as per AlchemyAPI
    - number of positive entities in tweet as per AlchemyAPI
    - mixed sentiment (1 = yes, 0 = no)
    - Tuple describing the tweets' 13 polarity features
        - negative tweet polarity averaged by number of words and normalized
        - objective tweet polarity averaged by number of words and normalized
        - positive tweet polarity averaged by number of words and normalized
        - number of elongated words in the tweet
        - number of UPPERCASE words in the tweet
        - number of negative words in the tweet
        - number of objective words in the tweet
        - number of positive words in the tweet
        - number of polarity words in tweet
        - number of negation words in tweet
        - sum of the polarity scores of the negative words
        - sum of the polarity scores of the objective words
        - sum of the polarity scores of the positive words.
    - Tuple describing the hashtags' 11 polarity features
        - negative tweet polarity averaged by number of words and normalized
        - objective tweet polarity averaged by number of words and normalized
        - positive tweet polarity averaged by number of words and normalized
        - number of negative words in the tweet
        - number of objective words in the tweet
        - number of positive words in the tweet
        - number of polarity words in tweet
        - number of negation words in tweet
        - sum of the polarity scores of the negative words
        - sum of the polarity scores of the objective words
        - sum of the polarity scores of the positive words.

    """
    alchemy_dir = "../extracted_alchemy_api_data"
    # The remaining 4 features aren't used as the results from AlchemyAPI
    # look random and not trustworthy
    alchemy_tweet_polarity, alchemy_tweet_type, alchemy_entity_polarity, \
        alchemy_entity_type, negative_entities, neutral_entities, \
        positive_entities, mixed_sentiment, _, _, _, _ = \
        get_sentiment_from_alchemyapi(alchemy_dir, tweet)
    text = tweet.text
    intensify_emoji = 1
    pos = 0
    neg = 0
    intensify_punct = 1
    adjectives = 0.25
    adverbs = 0.25
    verbs = 0.25
    nouns = 0.25
    sentence_polarity = get_dummy_polarity(text)
    hashtag_polarity = (0.0, 0.0, 0.0, 0, 0, 0, 0, 0, 0.0, 0.0, 0.0)
    d = enchant.Dict("en")
    text = multiple_replace(text, REPLACEMENTS)
    words = len(text.split())
    # Number of words in tweets
    start_length = words
    # Average length per sentence in this tweet
    average_length = 1.0 * words / len(text.split("."))
    # text = remove_numbers(text)
    # See tweets_with_hashtags.py that there's a correlation between tweets
    # with hashtags and positive sentiment
    has_hashtag = 1 if "#" in text else 0
    text = text.split()
    text = replace_slang(text, SLANG)
    text = expand_negations(text, NEGATION_REPLACEMENTS)
    text = remove_numbers(" ".join(text))
    text = text.split()
    if options.remove_urls:
        text = remove_url(text)
    text = separate_punctuation(" ".join(text))
    if options.remove_handles:
        text = remove_twitter_handle(text)
    if options.remove_hashtags:
        # Slang and shortened negations have already been replaced by correct
        # versions in hashtags
        text, hashtags = remove_hash(text, d)
        hashtag_polarity = determine_hashtag_polarity(hashtags, removed_words)
    if options.use_emojis:
        text, intensify_emoji, pos, neg = emoticons(text)

    # text = replace_slang(text, SLANG)
    # text = expand_negations(text, NEGATION_REPLACEMENTS)

    # If no POS-tags should be extracted, use pseudo ones as the following
    # functions expect POS tuples
    if not options.use_pos_tags and options.remove_punctuation:
        text = get_pseudo_pos_tags(text)
        text, intensify_punct = punctuation(text)

    if options.use_pos_tags:
        text = " ".join(text)
        text = get_pos_tags(text)
        adjectives, adverbs, verbs, nouns = get_pos_percentages(text)
        if words == 0:
            adjectives = adverbs = verbs = nouns = 0
        else:
            adjectives /= words
            adverbs /= words
            verbs /= words
            nouns /= words
        if options.remove_punctuation:
            text, intensify_punct = punctuation(text)

        if options.use_polarity:
            sentence_polarity = determine_sentence_polarity(
                text, intensify_emoji, intensify_punct, removed_words, None,
                None)
            # Which words to keep? a) sentiment bearing ones or b) adjectives +
            # adverbs
            if options.use_polarity_for_bow:
                # Text contains now only sentiment bearing words
                text = sentence_polarity[-1]
            # Now only keep polarity bearing words that are either verbs,
            # adjectives or adverbs
            text = remove_all_words_except_(text)
    text = to_lower(text)
    if options.remove_stopwords:
        text = remove_stop_words(text, removed_words)
    # print text
    if options.stemmer != "None":
        text = stem(text, options.stemmer, options.stemmer_lang)
    # Remaining words in the tweet after preprocessing
    end_length = len(text)
    # This is the preprocessed version of the tweet text to be used for
    # building the BoW representation
    result = " ".join(text)
    # For sentence_polarity, skip the text at the end
    return result, pos, neg, intensify_punct, start_length, end_length, \
        average_length, adjectives, adverbs, verbs, nouns, has_hashtag, \
        alchemy_tweet_polarity, alchemy_tweet_type, \
        alchemy_entity_polarity, alchemy_entity_type, negative_entities, \
        neutral_entities, positive_entities, mixed_sentiment, \
        sentence_polarity[:-1], hashtag_polarity


def preprocess_test(tweet, removed_words):
    """
    Preprocesses a single tweet. See preprocess() for an explanation. This
    one is only for testing its functionalities.

    Parameters
    ----------
    text: Tweet object.
    removed_words: Dictionary of words to be ignored.

    Returns
    -------
    (String, int, int, int, int, int, double, (double, double, double, int,
    int, int, int, double, double), (double, double, double, int, int, int,
    int, int, int).)
    The tweet text, number of positive emoticons, number of negative
    emoticons, number of exclamation/question marks,
    13 values describing:
    - tweet text
    - number of positive emoticons
    - number of negative emoticons
    - number of exclamation marks/question marks
    - length of tweet before preprocessing (based on words)
    - length of tweet after preprocessing (based on words)
    - average sentence length (based on words per sentence)
    - normalized percentage of adjectives
    - normalized percentage of adverbs
    - normalized percentage of verbs
    - normalized percentage of nouns
    - Tuple describing the tweets' 13 polarity features
        - negative tweet polarity averaged by number of words and normalized
        - objective tweet polarity averaged by number of words and normalized
        - positive tweet polarity averaged by number of words and normalized
        - number of elongated words in the tweet
        - number of UPPERCASE words in the tweet
        - number of negative words in the tweet
        - number of objective words in the tweet
        - number of positive words in the tweet
        - number of polarity words in tweet
        - number of negation words in tweet
        - sum of the polarity scores of the negative words
        - sum of the polarity scores of the objective words
        - sum of the polarity scores of the positive words.
    - Tuple describing the hashtags'
        - negative tweet polarity averaged by number of words and normalized
        - objective tweet polarity averaged by number of words and normalized
        - positive tweet polarity averaged by number of words and normalized
        - number of negative words in the tweet
        - number of positive words in the tweet
        - number of polarity words in tweet
        - number of negation words in tweet
        - sum of the polarity scores of the negative words
        - sum of the polarity scores of the positive words.

    """
    # Alchemy doesn't work with those dummy tweets
    alchemy_dir = "../extracted_alchemy_api_data"
    alchemy_tweet_polarity, alchemy_tweet_type, alchemy_entity_polarity, \
        alchemy_entity_type, mixed_sentiment = 0, 0, 0, 0, 0
    # The remaining 4 features aren't used as the results from AlchemyAPI
    # look random and not trustworthy
    # alchemy_tweet_polarity, alchemy_tweet_type, alchemy_entity_polarity, \
    #     alchemy_entity_type, mixed_sentiment, _, _, _, _ = \
    #     get_sentiment_from_alchemyapi(alchemy_dir, tweet)
    d = enchant.Dict("en")
    text = tweet.text
    text = multiple_replace(text, REPLACEMENTS)
    words = len(text.split()) * 1.0
    # Number of words in tweets
    start_length = words
    # Average length per sentence in this tweet
    average_length = 1.0 * words / len(text.split("."))
    text = remove_numbers(text)
    text = text.split()
    text = remove_url(text)
    text = separate_punctuation(" ".join(text))
    text = remove_twitter_handle(text)
    # Slang and shortened negations have already been replaced by correct
    # versions in hashtags
    text, hashtags = remove_hash(text, d)
    print "hashtags", hashtags
    hashtag_polarity = determine_hashtag_polarity(hashtags, removed_words)
    print "hashtag polarity", hashtag_polarity
    text, intensify_emoji, pos, neg = emoticons(text)
    text = replace_slang(text, SLANG)
    text = expand_negations(text, NEGATION_REPLACEMENTS)
    text = " ".join(text)
    text = get_pos_tags(text)

    adjectives, adverbs, verbs, nouns = get_pos_percentages(text)
    if words == 0:
        adjectives = adverbs = verbs = nouns = 0
    else:
        adjectives /= words
        adverbs /= words
        verbs /= words
        nouns /= words
    text, intensify_punct = punctuation(text)
    sentence_polarity = determine_sentence_polarity(
        text, intensify_emoji, intensify_punct, None, None)
    # Which words to keep? a) sentiment bearing ones or b) adjectives +
    # adverbs
    # Text contains now only sentiment bearing words
    text = sentence_polarity[-1]
    text = remove_all_words_except_(text)
    text = to_lower(text)
    text = remove_stop_words(text)
    text = stem(text, "snowball", "english")
    end_length = len(text)
    # This is the preprocessed text used for the BoW representation
    result = " ".join(text)
    print "mixed?", mixed_sentiment
    return result, pos, neg, intensify_punct, start_length, end_length, \
        average_length, adjectives, adverbs, verbs, nouns, \
        sentence_polarity[:-1], hashtag_polarity

if __name__ == "__main__":
    service_url = 'https://babelfy.io/v1/disambiguate'
    # text = ["I do not just like you, I love you"]
    # text = ["I do not do not love you"]
    # text1 = ["I don't liiikke you very much", "I love you a bit!!?!", "Let's see... VaTech/OSU tonight AC/DC tomorrow night NFL kicks off Thursday 5:00 a.m. alarms every morning #startschoolyearoffwithnosleep!"]
    text1 = ["Innovation for jobs is just around the corner - to be exact next Wednesday 8/19 at @Microsoft http://t.co/3DK6ToZeA8 http://t.co/L2wOZcwgRb"]
        # "Amazon Prime Day deals pose little Black Friday threat 1: A "
        #      "bl2ue Selfie Stick Pro with 3-in-1 Bluetoo...1 http://t.co/CcVDOpXnHd #Bluetooth"]
    for text in text1:
        t = Tweet("1")
        t.text = text
        print "resulting sentence for", text, ":"
        print preprocess_test(t)
    lang = "EN"
    key = "eef3945e-c99e-4993-b7b6-19cac00e3d7a"
    # get_wsd_(text, service_url, lang, key)
    # pos_dict = read_opinion_words("../positive-words.txt", LABELS["+"])
    # # Use only positive labels as swn also does so!!!
    # neg_dict = read_opinion_words("../negative-words.txt", LABELS["+"])
    # print "words in opinion dictionary", (len(pos_dict) + len(neg_dict))
    # sentence_polarity1 = determine_sentence_polarity(text1, pos_dict, neg_dict)
    # print "sentence polarity", sentence_polarity1
    # text2 = ["I do not like you very much"]
    # sentence_polarity2 = determine_sentence_polarity(text2, pos_dict, neg_dict)
    # print "sentence polarity", sentence_polarity2
    # print "negation vs. negation with diminisher"
    # print sentence_polarity1, "vs.", sentence_polarity2
    # TODO: Patched a bug in wordnet:
    # https://github.com/nltk/nltk/issues/1062
    print "test the bugfix"
    synsets = wn.synsets('amazing')
    target = synsets[2]
    senti_synset = swn.senti_synset(target.name())
    if senti_synset is not None:
        print "bug is fixed"
    else:
        print "bug still exists"
