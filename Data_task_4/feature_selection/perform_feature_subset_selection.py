"""
Performs feature subset selection. Remove 1 feature at a time - the one that
yields the least decrease in performance.
"""
import os


if __name__ == "__main__":
    k = 10
    config_dir = "../experiments/configs/feature_subset_selection"
    # Create configuration files
    # For each configuration file:
    # Load dataset
    # Select a subset