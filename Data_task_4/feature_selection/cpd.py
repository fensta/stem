"""
This file contains different techniques to reduce the feature space of our
problem.
"""
from collections import Counter, defaultdict

from Data_task_4.io.input_output import open_dataset
from Data_task_4.preprocess.preprocessing import *


class CPD(object):
    """
    Categorical proportional difference. See "Categorical Proportional
    Difference: A Feature Selection Method for Text Categorization" (Simeon and
    Hildermann) and a successful application: "Feature Selection and Weighting
    in Sentiment Analysis" (O'Keefe).
    """
    def __init__(self, tweets, threshold, dst=None):
        """
        Parameters
        ----------
        tweets: List of Tweet objects.
        threshold: Threshold below which words will be ignored as features.
        dst: Path to the file where all ignored words should be stored. If
        None, no file will be created.

        """
        self.tweets = tweets
        self.threshold = threshold

    def create(self):
        """
        Creates a dictionary of words to be ignored according to CPD.

        Returns
        -------
        dict.
        Dictionary with the words to be ignored.

        """
        classes = []
        # {word1: [cpd1, cpd2, cpd3], word2: [cpd1, cpd2, cpd3]...}
        cpds = defaultdict(list)
        # Find out how many classes exist
        for tweet in self.tweets:
            classes.append(tweet.label)
        classes = set(classes)

        # For every class
        for cls in classes:
            # {word1: Counter(cls1: XX, cls2: YY...)}
            dic = {}
            # Counter for words inside
            for tweet in self.tweets:
                is_within = False
                # Count all words of the tweet as belonging to the class
                if tweet.label == cls:
                    is_within = True
                for w in tweet.text_cpd.split():
                    if w not in dic:
                        dic[w] = Counter()
                    if is_within:
                        # print "increase counter ({}) for class {}".format(w,
                        #                                                   cls)
                        dic[w][cls] += 1
                    else:
                        # print "increase counter ({}) for class {}".\
                        #     format(w, "NOT_" + cls)
                        dic[w]["NOT_" + cls] += 1
            # Now compute cpd per word for this class
            d = self._compute_cpd(dic)
            # Now add the CPDs computed for this class to the list
            for key, value in d.iteritems():
                cpds[key].append(value)
        # {word1: cpd, word2: cpd}
        cpd = {}
        # Choose the maximum value per word as CPD
        for w, values in cpds.iteritems():
            max_val = max(values)
            if max_val < self.threshold:
                cpd[w] = max_val
        print "WORDS TO BE IGNORED:", cpd
        return cpd

    def _compute_cpd(self, dic):
        """
        Computes the actual categorical proportional difference

        Parameters
        ----------
        dic: Dictionary describing how often a word occurs among all classes.

        Returns
        -------
        dict.
        Dictionary containing for each word the categorical proportional
        difference value for specific class.

        """
        # {word1: cpd}
        result = {}
        for word, classes in dic.iteritems():
            total = sum(classes.values())
            vals = classes.values()
            # Word occurs only in one class
            cls2 = 0
            if len(vals) == 2:
                cls2 = vals[1]
            # There are always 2 classes
            cpd = 1.0 * abs(vals[0] - cls2) / total
            result[word] = cpd
        return result


def punctuation_bla(text):
    """Function adjusted to this test case, i.e. it doesn't expect (word,
    POS-tag) tuples, but only words"""
    #Removes punctuation and RT
    puncts = list(string.punctuation)
    intensifier = 1  # Will be used as multiplicator
    text_update = []
    punct_found = False

    for w in text:
        puncts_present = []
        w_tmp = w
        # Search for ocurrences (potentially multiple times in consecutive order)
        #  of "!" and "!?" or "?!" indicating intensification of polarity
        # matcher = re.compile(r"[\?!]+")
        # for m in matcher.finditer(w):
        #     match = m.group()
        #     print "punct", match
        #     intensifier += 1
        # Accounts for '!?' or '?!' by counting '!'
        intensifier += w.count("!")
        intensifier += w.count("??")
        for punct in puncts:
            if punct in w_tmp:
                puncts_present.append(punct)
                punct_found = True
                not_seen = True
                while not_seen:
                    splitted = w_tmp.split(punct, 1)
                    w_tmp = ''.join(splitted)
                    if punct not in w_tmp:
                        not_seen = False
        update_word = w
        for punct in puncts_present:
            # If word ends with "s'" (Andreas'), remove only "'"
            if punct == "'" and len(update_word) > 2 and update_word[-2] == \
                    "s" and update_word[-1] == "'":
                update_word = update_word[:-1]
            # If apostrophe, add NO whitespace except word ends with "s" (
            # possessive form)
            if punct == "'" and update_word[-1] != "s":
                update_word = update_word.replace(punct, "")
            # Word contains a single letter which isn't "a" or "i" or a number
            elif len(update_word) == 1 and (update_word.lower() != "a" or \
                update_word.lower() != "i" or not update_word.isdigit()):
                    update_word = ""
            else:
                update_word = update_word.replace(punct, " ")
        update_word_tmp = update_word.strip()
        updated_word = []
        # Check if one of the words now is a single letter after split ->
        # remove it
        for word in update_word_tmp.split():
            if len(word) == 1:
                if update_word_tmp.lower() == "a" or update_word_tmp.lower() \
                        == "i" or update_word_tmp.isdigit():
                    updated_word.append(word)
            else:
                updated_word.append(word)
        # Remove "RT" or "rt"
        cleaned = []
        for w_ in updated_word:
            if w_ != "rt" and w_ != "RT":
                cleaned.append(w_)

        if punct_found:
            text_update.extend(cleaned)
        if not punct_found and w != "rt" and w != "RT":
            text_update.append(w)
        punct_found = False
    return text_update, intensifier


def preprocess_bla(text):
    """
    Models the whole preprocessing part of our approach. However, POS-tags
    are omitted as they slow things down and aren't crucial for this test.

    """
    d = enchant.Dict("en")
    text = multiple_replace(text, REPLACEMENTS)
    text = remove_numbers(text)
    text = text.split()
    text = remove_url(text)
    text = separate_punctuation(" ".join(text))
    text = remove_twitter_handle(text)
    text, hashtags = remove_hash(text, d)
    text, intensify_emoji, pos, neg = emoticons(text)
    text = replace_slang(text, SLANG)
    text = expand_negations(text, NEGATION_REPLACEMENTS)
    text = get_pos_tags(text)
    text, intensify_punct = punctuation_bla(text)
    # cpds =
    text = to_lower(text)
    text = remove_stop_words(text)
    text = stem(text, "snowball", "english")
    result = " ".join(text)

    return result


if __name__ == "__main__":
    dataset_path = "../semeval2016_task4/train/tweets30.txt"
    tweets = open_dataset(dataset_path)

    # for threshold in range(0, 11, 1):
    #     # Run it in 0.05 steps
    #     threshold /= 20.0
    #     print threshold
    #     cpd = CPD(tweets, threshold)
    #     print "#words with threshold {}".format(threshold)
    #     cpds = cpd.create()
    #     print "CPDs"
    #     print len(cpds)
    #

    ########################################################################
    # Without preprocessing 24312 words exist. Using CPD only, this number #
    # can be reduced to 24080 (threshold = 1), i.e. 232 words could be     #
    # removed from the initial corpus                                      #
    ########################################################################

    for tweet in tweets:
        tweet.text_cpd = preprocess_bla(tweet.text)

    for threshold in range(0, 21, 1):
        # Run it in 0.05 steps
        threshold /= 20.0
        print threshold
        cpd = CPD(tweets, threshold)
        print "#words with threshold {}".format(threshold)
        cpds = cpd.create()
        print "CPDs"
        print len(cpds)

    ########################################################################
    # With preprocessing 7948 words exist. Using CPD additionally,         #
    # this number can be reduced to 6514 (threshold = 1), i.e. 1434 words  #
    # could be removed from the initial corpus                             #
    ########################################################################
    # NOTE: We use all preprocessing steps here that alter the text #
    #################################################################
    # Output from console:
    # words with threshold 0.0
    # CPDs
    # 7948
    # 0.05
    # #words with threshold 0.05
    # CPDs
    # 7948
    # 0.1
    # #words with threshold 0.1
    # CPDs
    # 7948
    # 0.15
    # #words with threshold 0.15
    # CPDs
    # 7948
    # 0.2
    # #words with threshold 0.2
    # CPDs
    # 7948
    # 0.25
    # #words with threshold 0.25
    # CPDs
    # 7948
    # 0.3
    # #words with threshold 0.3
    # CPDs
    # 7948
    # 0.35
    # #words with threshold 0.35
    # CPDs
    # 7879
    # 0.4
    # #words with threshold 0.4
    # CPDs
    # 7874
    # 0.45
    # #words with threshold 0.45
    # CPDs
    # 7831
    # 0.5
    # #words with threshold 0.5
    # CPDs
    # 7793
    # 0.55
    # #words with threshold 0.55
    # CPDs
    # 7615
    # 0.6
    # #words with threshold 0.6
    # CPDs
    # 7540
    # 0.65
    # #words with threshold 0.65
    # CPDs
    # 7340
    # 0.7
    # #words with threshold 0.7
    # CPDs
    # 7171
    # 0.75
    # #words with threshold 0.75
    # CPDs
    # 7043
    # 0.8
    # #words with threshold 0.8
    # CPDs
    # 6856
    # 0.85
    # #words with threshold 0.85
    # CPDs
    # 6690
    # 0.9
    # #words with threshold 0.9
    # CPDs
    # 6587
    # 0.95
    # #words with threshold 0.95
    # CPDs
    # 6519
    # 1.0
    # #words with threshold 1.0
    # CPDs
    # 6514
