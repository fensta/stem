"""
Checks whether CPD has any influence on the extracted features. Idea: check
whether the tweets of 2 alternative representations are identically
represented. If so, CPD didn't work -> bug in code?
"""
import cPickle


def analyze(tweets1, tweets2):
    """
    Checks whether the representations of all tweets are identical or not.

    Parameters
    ----------
    tweets1: List of Tweet objects.
    tweets2: List of Tweet objects.

    """
    print "tweets", len(tweets1), len(tweets2)
    for idx in xrange(len(tweets1)):
        print tweets1[idx].bow == tweets2[idx].bow
        # print tweets1[idx].bow
        print "----------"
        # print tweets2[idx].bow

if __name__ == "__main__":
    p1 = \
        "../preprocessed_datasets" \
        "/alt_test_cpd_1_0_3_train_full.txt"
    p2 = \
        "../preprocessed_datasets" \
        "/alt_test_cpd_0_4_3_train_full.txt"
    with open(p1, "rb") as f:
        tweets1 = cPickle.load(f)
    with open(p2, "rb") as f:
        tweets2 = cPickle.load(f)
    analyze(tweets1, tweets2)