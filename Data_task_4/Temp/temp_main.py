# coding=utf-8

import csv
import string
import re
import collections
import itertools

from nltk.corpus import wordnet as wn
from nltk.corpus import sentiwordnet as swn
import nltk
from nltk.corpus import stopwords
import enchant
from nltk.tag.stanford import StanfordPOSTagger, StanfordNERTagger

# import SpellChecker
from Data_task_4.io.input_output import open_dataset
from manual_improvements import NEGATION_REPLACEMENTS, SLANG, REPLACEMENTS, \
    EMOTICONS


def readtweets():
    ready_tweets = []
    # with open('Tweets.csv', 'rb') as csvfile:
    tweets = open_dataset("Tweets.csv")#csv.reader(csvfile)
    for t in tweets:
        print t.text
    for no, tweet in enumerate(tweets):
        text = tweet.text
        # print "------------------"
        #print(type(text))
        # print "Tweet data: ", text
        # Text is a list with 1 entry - the message
        if text != "Not Available":
            # Converts list to strings
            # text = ''.join(text)
            # emoticon_text = emoticons(text)
            # print(type(text))
            # print(text)
            # Replace some patterns
            text = multiple_replace(text, REPLACEMENTS)
            #Converts each word into a token
            text = text.split()
            # print "Tokenised Tweet: ", text
            tweet.preprocessed_text, tweet.intensify = preprocess(text)
        # else:
        #     print "tweet", no, "should be removed"
    for t in tweets:
        print t.preprocessed_text, t.intensify
    return tweets


# def preprocess(text):
#     """
#     Preprocesses a tweet message.
#
#     Parameters
#     ----------
#     text: A single tokenized tweet.
#
#     Returns
#     -------
#     Preprocessed string of the text.
#     """
#     line = removeURL(text)
#     line = removeTWHandle(line)
#     d = enchant.Dict("en")
#     line = remove_hash(line, d)
#     line, intensify1 = emoticons(line)
#     #cleanSpell = SpellCorrect(checkemojis)
#     line, intensify2 = punctuation(line)
#     line = replace_slang(line, SLANG)
#     line = expand_negations(line, NEGATION_REPLACEMENTS)
#     # TODO: change BoW by incorporating negations before removing them as
#     # stop words
#     # line = extract_polarity(line)
#     line = removeStopWord(line)
#     pos, ner = get_pos_tags(line)
#     # TODO: remove all words except adverbs and adjectives based on POS tags
#     line, err = spell_checking(ner)
#     line = to_lower(line)
#     result = " ".join(line)
#     print result
#     print intensify1, intensify2
#     return result, intensify1 + intensify2


def to_lower(text):
    low = []
    for w in text:
        low.append(w.lower())
    return low


def expand_negations(words, rep_dict):
    """
    Replaces short-forms of negations by long forms, e.g. can't -> can not.
    """
    expanded = []
    for w in words:
        for neg in rep_dict:
            if w.lower() == neg:
                w = rep_dict[neg]
        expanded.append(w)
    return expanded


def get_pos_tags(sentence):
    """
    Pre-processes a list of tweet messages and returns the NER representation.
    """
    tokenized = []
    pos = []
    ner = []

    # path1 = '/home/mishalkazmi/NLP/PyCharm/inspire/semeval/task2/resources/stanford-postagger-full-2015-04-20/'
    # path2 = '/home/mishalkazmi/NLP/PyCharm/inspire/semeval/task2/resources/stanford-ner-2015-04-20/'

    path1 = '/home/fensta/Stanford/stanford-postagger-full-2015-04-20/'
    path2 = '/home/fensta/Stanford/stanford-ner-2015-04-20/'
    for s in sentence:
        stc_tokens = lemmatize(s)
        tokenized.append(stc_tokens)
        stc_POS = POSTags(path1, stc_tokens)
        pos.append(stc_POS)
        stc_NER = NERTags(path2, stc_tokens)
        ner.append(stc_NER)
    return pos, ner


def lemmatize(sentence):
    stc_tokens = nltk.word_tokenize(sentence)
    # print "Tokens: ", stc_tokens
    return stc_tokens


def POSTags(path, sentence):
    POS = StanfordPOSTagger(path +
                            'models/english-bidirectional-distsim.tagger',
                            path + 'stanford-postagger.jar')
    stc_POS = POS.tag(sentence)
    # print "POS tagged sentence: ", stc_POS
    return stc_POS


def NERTags(path, sentence):
    NER = StanfordNERTagger(
        path + 'classifiers/english.conll.4class.distsim.crf.ser.gz', path +
        'stanford-ner.jar')
    stc_NER = NER.tag(sentence)
    # print "NERs in sentence: ", stc_NER
    return stc_NER


def replace_slang(text, subst):
    """
    Replaces slang words with a given substitution in a tweet.

    Parameters
    ----------
    text: List of words representing a tweet.
    subst: Dictionary containing substitutions for each slang word.

    Returns
    -------
    Text without slang.

    """
    corrected = []
    for word in text:
        # Don't transform original word into lowercase
        w = word.lower()
        for slang in subst:
            if w == slang:
                word = subst[slang]
        corrected.append(word)
    return corrected


def removeURL(text):
    # Remove URLs
    cleanURL = [w for w in text if not w.startswith('http') and not
    w.startswith('https')]
    # print "Removed URLs: ", cleanURL
    return cleanURL


def removeTWHandle(text):
    #Remove Twitter Handle
    cleanTWHandle = [w for w in text if not w.startswith('@')]
    # print "Removed Twitter handle: ", cleanTWHandle
    return cleanTWHandle


# http://stackoverflow.com/questions/6116978/python-replace-multiple-strings
def multiple_replace(string, rep_dict):
    """
    Replaces patterns specified in dictionary with respective values in a
    string.

    Parameters
    ----------
    string: A string representing a tweet.
    rep_dict: Dictionary containing the patterns to be replaced and its
    substitute values.

    """
    pattern = re.compile("|".join([re.escape(k) for k in rep_dict.keys()]),
                         re.M)
    return pattern.sub(lambda x: rep_dict[x.group(0)], string)


def emoticons(text):
    intensifier = 1  # Will be used as multiplicator
    new_text = []
    match_found = False

    for w in text:
        # Emoticons present in the word
        # existing_emojis = {}
        existing_emojis = []
        w_tmp = w
        word_polarity = 0
        for emoji in EMOTICONS.iterkeys():
            if emoji in w_tmp:
                #print "emoji {} in {}".format(emoji, w_tmp)
                # existing_emojis[emoji] = ""
                existing_emojis.append(emoji)
                match_found = True
                not_processed = True
                # Search for multiple occurrences of the emoticon
                while not_processed:
                    # Split at first match and process remainder separately
                    splitted = w_tmp.split(emoji, 1)
                    word_polarity += EMOTICONS[emoji]
                    w_tmp = ''.join(splitted)
                    if emoji not in w_tmp:
                        not_processed = False
        # Remove emoticons from message
        # print "existing emojis in {}: {}".format(w, existing_emojis)
        # print "polarity", word_polarity
        new_word = w
        for emoji in existing_emojis:
            new_word = new_word.replace(emoji, " ")
        new_word = new_word.strip()
        # Add word only if an emoticon exists in it
        if match_found:
            new_text.extend(new_word.split())
        if w in EMOTICONS:
             word_polarity = EMOTICONS[w]
        if not match_found:
            new_text.append(w)
        match_found = False
        intensifier += word_polarity
        #print "new text", new_text
        #print "Sentiment extracted and Emoticons removed"
    # print "Sentence Polarity from Emoticons dectected: ", sentence_polarity
    # print "Emoticons removed: ", new_text
    return new_text, intensifier


def SpellCorrect(text):
    cleanSpell =[]
    for w in text:
        Spell = SpellChecker.correct(w)
        cleanSpell.append(Spell)
    print "Spell Check: ", cleanSpell
    return cleanSpell


def removeStopWord(text):
    stop = stopwords.words('english')
    cleanStopWord = [w for w in text if w not in stop]
    # print "Stopwords removed: ", cleanStopWord
    return cleanStopWord


def spell_checking(tweet):
    """
    Takes a list of (word, POS tag) tuples and checks spelling if it's not a
    NER.

    """
    d = enchant.Dict("en")
    err = []
    corrected = []
    # [[(word1, tag)], [(word2, tag)]]
    for pair in tweet:
        w, tag = pair[0][0], pair[0][1]
        # If not a NER (-> don't change NERs, just append them)
        if tag == "O":
            if not d.check(w) and w != "can not":
                # First, remove letters that occur more than twice and check
                # again in the dictionary. If it still can't be found,
                # correct typos
                # Matches any characters that occurs > 2 times (case
                # insensitive)
                matcher = re.compile(r"(?i)([a-z])\1{2,}")
                matches = list(match.group() for match in matcher.finditer(w))
                # Replace each match with a single occurrence of the
                # elongated letter
                for m in matches:
                    letter = m[0].lower()
                    w = w.replace(m, letter)
                # Correct typos
                if not d.check(w) and w != "can not":
                    try:
                        first_sug = d.suggest(w)[0]
                        corrected.append(first_sug)
                        # print "word: {} suggestions: {}".\
                        #     format(w, d.suggest(w))
                    except IndexError:
                        # Words which need to be treated manually
                        err.append((w, None))
                else:
                    corrected.append(w)
            else:
                corrected.append(w)
        else:
            corrected.append(w)
    return corrected, err


def punctuation(text):
    #Removes punctuation and RT
    puncts = list(string.punctuation)
    intensifier = 1 # Will be used as multiplicator
    text_update = []
    punct_found = False

    for w in text:
        puncts_present = []
        w_tmp = w
        # Search for ocurrences (potentially multiple times in consecutive order)
        #  of "!" and "!?" or "?!" indicating intensification of polarity
        # matcher = re.compile(r"[\?!]+")
        # for m in matcher.finditer(w):
        #     match = m.group()
        #     intensifier += 1
        # intensifier = len(re.findall(r"[\?!]", w))
        # Accounts for '!?' or '?!' by counting '!'
        intensifier = w.count("!")
        intensifier += w.count("??")
        for punct in puncts:
            if punct in w_tmp:
                puncts_present.append(punct)
                punct_found = True
                not_seen = True
                while not_seen:
                    splitted = w_tmp.split(punct, 1)
                    w_tmp = ''.join(splitted)
                    if punct not in w_tmp:
                        not_seen = False
        update_word = w
        for punct in puncts_present:
            # If word ends with "s'" (Andreas'), remove only "'"
            if punct == "'" and len(update_word) > 2 and update_word[-2] == \
                    "s" and update_word[-1] == "'":
                update_word = update_word[:-1]
            # If apostrophe, add NO whitespace except word ends with "s" (
            # possessive form)
            if punct == "'" and update_word[-1] != "s":
                update_word = update_word.replace(punct, "")
            # Word contains a single letter which isn't "a" or "i" or a number
            elif len(update_word) == 1 and (update_word.lower() != "a" or \
                update_word.lower() != "i" or not update_word.isdigit()):
                    update_word = ""
            else:
                update_word = update_word.replace(punct, " ")
        update_word_tmp = update_word.strip()
        updated_word = []
        # Check if one of the words now is a single letter after split ->
        # remove it
        for word in update_word_tmp.split():
            if len(word) == 1:
                if update_word_tmp.lower() == "a" or update_word_tmp.lower() \
                        == "i" or update_word_tmp.isdigit():
                    updated_word.append(word)
            else:
                updated_word.append(word)
        # Remove "RT" or "rt"
        cleaned = []
        for w_ in updated_word:
            if w_ != "rt" and w_ != "RT":
                cleaned.append(w_)

        if punct_found:
            text_update.extend(cleaned)
        if not punct_found and w != "rt" and w != "RT":
            text_update.append(w)
        punct_found = False
    return text_update, intensifier


# def punctuation(text):
#     """
#     Removes punctuation and retweets.
#     """
#     puncts = list(string.punctuation)
#
#     text_update = []
#     punct_found = False
#
#     for w in text:
#         puncts_present = []
#         w_tmp = w
#         for punct in puncts:
#             if punct in w_tmp:
#                 puncts_present.append(punct)
#                 punct_found = True
#                 not_seen = True
#                 while not_seen:
#                     splitted = w_tmp.split(punct, 1)
#                     w_tmp = ''.join(splitted)
#                     if punct not in w_tmp:
#                         not_seen = False
#
#         update_word = w
#
#         for punct in puncts_present:
#             update_word = update_word.replace(punct, " ")
#         update_word = update_word.strip()
#
#         # Remove "RT" or "rt"
#         cleaned = []
#         for w_ in update_word.split():
#             if w_ != "rt" and w_ != "RT":
#                 cleaned.append(w_)
#
#         if punct_found:
#             text_update.extend(cleaned)
#         if not punct_found and w != "rt" and w != "RT":
#             text_update.append(w)
#     # print "Removed Punctuation", text_update
#     return text_update


def remove_hash(text, dic):
    """
    If hashtag is comprised of a single known word, it is kept without
    hashtag. Same goes for the case when the hashtag is written in camel case.
    Otherwise the hashtags are completely discarded.

    """
    cleaned = []
    for w in text:
        if w.startswith("#"):
            # Is it a single word?
            w = w[1:]
            if dic.check(w):
                cleaned.append(w)
            else:
                # Is it CamelCase? Word must contain at least 2 capitalized
                # letters
                camel_string = ""
                is_camel = False
                # And first character is uppercase while 2nd is lowercase
                if len(w) > 1 and w[0].isupper() and w[1]:
                    for l in w[1:]:
                        if l.isupper():
                            is_camel = True
                            break
                if is_camel:
                    for l in w:
                        if l.isupper():
                            l = " " + l
                        camel_string += l
                    cleaned.extend(camel_string.strip().split())
                else:
                    # Multiple words in hashtag
                    result = tokenize_hashtag(w, dic)
                    if result is not None:
                        cleaned.extend(result)
        else:
            cleaned.append(w)
    return cleaned


# http://stackoverflow.com/questions/18406776/python-split-a-string-into-all-
# possible-ordered-phrases
def candidates(word):
    # n = 1..(n-1)
    ns = range(1, len(word))
    # split into 2, 3, 4, ..., n parts.
    for n in ns:
        for idxs in itertools.combinations(ns, n):
            yield [''.join(word[i:j]) for i, j in zip((0,) + idxs, idxs + (
                None,))]


def tokenize_hashtag(w, dic):
    """
    Tokenizes a multi-word hashtag that isn't written in camelcase.

    Parameters
    ----------
    w: Word which represents a hashtag without the hash.
    dic: PyEnchant Dic representing the language to be expected.

    Returns
    -------
    List.
    Tokenized hashtag as a list of words. If no such tokenization can be
    found, None is returned.

    """
    likely = []
    for candidate in candidates(w):
        to_add = []
        is_candidate = True
        for w in candidate:
            is_correct = False
            # Replace slang and expand negations
            if w in NEGATION_REPLACEMENTS:
                is_correct = True
                w = NEGATION_REPLACEMENTS[w]
            if w in SLANG:
                is_correct = True
                w = SLANG[w]
            # Replaced words don't need to be checked
            if not is_correct:
                # If it's a single letter word or not present in the dictionary
                if not dic.check(w) or w.lower() in "bcdfghjklmnopqrstvwyz":
                    is_candidate = False
                    break
                else:
                    # Valid word
                    to_add.append(w)
            else:
                # Valid word after replacement -> might contain multiple words
                to_add.extend(w.split())
        if is_candidate:
            likely.append(to_add)
    # If a combination of valid words was found, use the one with the lowest
    # number of words
    if len(likely) > 0:
        # Use the candidate with lowest number of words
        shortest = 100000000
        for idx, candidate in enumerate(likely):
            if len(candidate) < shortest:
                shortest = idx
        return likely[shortest]
    return None

if __name__ == '__main__':
    text = readtweets()
    print text
    # TODO: Emoticon detection and punctuation removal
    #     emoticons_str = r"""
    #     (?:
    #         [:=;] # Eyes
    #         [oO\-]? # Nose (optional)
    #         [D\)\]\(\]/\\OpP] # Mouth
    #     )"""
    #
    #     regex_str = [
    #         emoticons_str,
    #         r'<[^>]+>', # HTML tags
    #         r'(?:@[\w_]+)', # @-mentions
    #         r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    #         r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
    #
    #         r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    #         r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    #         r'(?:[\w_]+)', # other words
    #         r'(?:\S)' # anything else
    #     ]
    #
    #
    #     tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
    #     emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)
