"""
This script tokenizes hashtags and tests the capabilities of the algorithm.
"""
import itertools

import enchant

from manual_improvements import NEGATION_REPLACEMENTS, SLANG


def remove_hash(text, dic):
    """
    If hashtag is comprised of a single known word, it is kept without
    hashtag. Same goes for the case when the hashtag is written in camel case.
    Otherwise the hashtags are completely discarded.

    """
    cleaned = []
    for w in text:
        if w.startswith("#"):
            # Is it a single word?
            w = w[1:]
            if dic.check(w):
                cleaned.append(w)
            else:
                # Is it CamelCase? Word must contain at least 2 capitalized
                # letters
                camel_string = ""
                is_camel = False
                # And first character is uppercase while 2nd is lowercase
                if len(w) > 1 and w[0].isupper() and w[1]:
                    for l in w[1:]:
                        if l.isupper():
                            is_camel = True
                            break
                if is_camel:
                    for l in w:
                        if l.isupper():
                            l = " " + l
                        camel_string += l
                    cleaned.extend(camel_string.strip().split())
                else:
                    # Multiple words in hashtag
                    result = tokenize_hashtag(w, dic)
                    if result is not None:
                        cleaned.extend(result)
        else:
            cleaned.append(w)
    return cleaned


# http://stackoverflow.com/questions/18406776/python-split-a-string-into-all-
# possible-ordered-phrases
def candidates(word):
    # n = 1..(n-1)
    ns = range(1, len(word))
    # split into 2, 3, 4, ..., n parts.
    for n in ns:
        for idxs in itertools.combinations(ns, n):
            yield [''.join(word[i:j]) for i, j in zip((0,) + idxs, idxs + (
                None,))]


def tokenize_hashtag(w, dic):
    """
    Tokenizes a multi-word hashtag that isn't written in camelcase.

    Parameters
    ----------
    w: Word which represents a hashtag without the hash.
    dic: PyEnchant Dic representing the language to be expected.

    Returns
    -------
    List.
    Tokenized hashtag as a list of words. If no such tokenization can be
    found, None is returned.

    """
    likely = []
    for candidate in candidates(w):
        to_add = []
        is_candidate = True
        for w in candidate:
            is_correct = False
            # Replace slang and expand negations
            if w in NEGATION_REPLACEMENTS:
                is_correct = True
                w = NEGATION_REPLACEMENTS[w]
            if w in SLANG:
                is_correct = True
                w = SLANG[w]
            # Replaced words don't need to be checked
            if not is_correct:
                # If it's a single letter word or not present in the dictionary
                if not dic.check(w) or w.lower() in "bcdfghjklmnopqrstvwyz":
                    is_candidate = False
                    break
                else:
                    # Valid word
                    to_add.append(w)
            else:
                # Valid word after replacement -> might contain multiple words
                to_add.extend(w.split())
        if is_candidate:
            likely.append(to_add)
    # If a combination of valid words was found, use the one with the lowest
    # number of words
    if len(likely) > 0:
        # Use the candidate with lowest number of words
        shortest = 100000000
        for idx, candidate in enumerate(likely):
            if len(candidate) < shortest:
                shortest = idx
        return likely[shortest]
    return None


if __name__ == "__main__":
    hashtags = ["#test", "hi", "#Iloveit", "#Moreofthesame",
                "#thisisasuperlongtext", "#HeyYou", "#YesYou",
                "#xkcdftw", "#aaronrodgers", "#Idntknowb4"]
    d = enchant.Dict("en")
    print remove_hash(hashtags, d)
