# Use dummy data for testing purposes
DEBUG = False
# All tweets that contain messages from this list, will be discarded from the
# training set
INVALID_TWEETS = {
    "Not Available": None,
    "Error 508": None
}
