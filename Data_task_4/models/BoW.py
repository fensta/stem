"""
A vectorizer that converts a text into a BoW representation allowing to
enrich it with additional features. If no text should be considered, i.e. the
representation uses only the additional features, .preprocessed of a Tweet
object
MUST BE EMPTY. This attribute has to be present. This vectorizer can deal
with UTF-8 encoded strings as it treats every word like that.
"""
from collections import Counter, OrderedDict
import sys
import warnings
import operator

from scipy.sparse import csr_matrix
import numpy as np

from Data_task_4.models.containers import Tweet


# https://codefisher.org/catch/blog/2015/06/16/how-create-ordered-counter-class-python/
# This class is necessary for transform()
class OrderedCounter(Counter, OrderedDict):
    pass


class BoW(object):
    # Prefix for artificial attributes as they "look" like words in the BoW,
    # we make them unique by adding a prefix in front of each artificial
    # attribute
    PREFIX = u"$$$_"

    def __init__(self, min_words=0.0, max_words=1.0, use_bigrams=False,
                 additional_attrs=[], min_length=2):
        """
        Initialize the bag-of-words vectorizer.

        Parameters
        ----------
        min_words: Minimum number of occurrences of a word in
        the whole corpus to be considered. If it's an integer, it'll be
        treated as an absolute value. In case of a float, it'll be
        interpreted as percentage w.r.t. the corpus vocabulary.
        max_words: Default: -1. -1 indicates that there's no upper limit.
        Integers and floats are handled similarly to <min_words>.
        use_bigrams: If True, bi-grams and uni-grams are used in BoW
        representation. If False, only uni-grams are used.
        additional_attrs: List of attributes to add to BoW. Those attributes
        must exist in all Tweet objects.
        min_length: Minimum length of words to be stored in the BoW
        representation. If a word is shorter, it's discarded.

        """
        # Set range of word frequencies to consider
        self.min_freq = min_words
        self.max_freq = max_words
        if self.max_freq == -1:
            self.max_freq = sys.maxint
        # {word1: index1 in matrix}
        self.vocabulary = {}
        # BoW matrix
        self.mat = None
        self.use_bigrams = use_bigrams
        # attrs: List of attributes of tweets that should be added to the BoW
        #        representation. MUST be numerical data. Each attribute
        #        represents exactly one value that should be stored in an
        #        additional column.
        self.attrs = additional_attrs
        self.min_len = min_length

    def __repr__(self):
        return self.__class__.__name__ + str(self.__dict__)

    # http://locallyoptimal.com/blog/2013/01/20/elegant-n-gram-generation-in-python/
    def _find_ngrams(self, input_list, n):
        return zip(*[input_list[i:] for i in range(n)])

    def _init_bow(self, tweets):
        """Sets up the mapping for BoW"""
        # List of all words in the vocabulary - unigrams
        vocab = []
        for tweet in tweets:
            # Tokenize tweet by words
            words_tmp = tweet.preprocessed.split()
            words = []
            # Discard any short words
            for w in words_tmp:
                if len(w) >= self.min_len:
                    words.append(w)
            vocab.extend(words)
            # Add bi-grams
            if self.use_bigrams:
                vocab.extend(self._find_ngrams(words, 2))
        uniqs = Counter(vocab)
        # print "Word frequencies in corpus:", uniqs
        del vocab

        # User entered percentages for cut-off frequencies
        if isinstance(self.min_freq, float):
            self.min_freq = int(sum(uniqs.values()) * self.min_freq)
            # print "min set to", self.min_freq
        if isinstance(self.max_freq, float):
            print sum(uniqs.values())
            self.max_freq = int(sum(uniqs.values()) * self.max_freq)
            # print "max set to", self.max_freq

        # Check whether any words occur too often/seldom
        for entry in list(uniqs.iteritems()):
            word = entry[0]
            freq = entry[1]
            # Only allow values in the valid frequency range
            if freq < self.min_freq or freq > self.max_freq:
                del uniqs[word]

        if self.attrs is not None:
            # Now add the additional features
            for attr in self.attrs:
                # Each artifical attribute has a prefix
                uniqs[self.PREFIX + attr] = 0

        # Create a mapping such that each word stores its index as value
        # {word1: index1, word2: index2}
        idx = 0
        for word in uniqs:
            word = self._to_unicode(word)
            self.vocabulary[word] = idx
            idx += 1
        # print "Overall vocabulary:", self.vocabulary
        del uniqs

    def get_feature_names(self):
        """Returns a list of words present in the BoW representation."""
        return self.vocabulary.keys()

    def _sane(self, tweets):
        """
        Performs initial checks to test whether the input should be converted
        into BoW representation or not. In the latter case exceptions are
        raised.

        Parameters
        ----------
        tweets: List of tweet objects


        Raises
        ------
        TypeError if <tweets> isn't a list of Tweet objects.
        AttributeError if not all specified attributes in the list exist for
        a tweet object.
        """
        # Check that only Tweet objects are used
        if not isinstance(tweets[0], Tweet):
            raise TypeError("Input must be a Tweet object! You used {"
                            "}".format(type(tweets[0])))
        # Make sure that the .preprocessed attribute exists
        if not hasattr(tweets[0], "preprocessed"):
            raise AttributeError("Tweet {} doesn't have attribute {"
                                 "}!".format(tweets[0].tweet_id,
                                             "preprocessed"))
        # Check that all specified attributes are set
        if self.attrs is not None:
            for attr in self.attrs:
                if not hasattr(tweets[0], attr):
                    raise AttributeError("Tweet {} doesn't have attribute {"
                                         "}!".format(tweets[0].tweet_id, attr))

    def _to_unicode(self, word):
        """
        Converts a string into a UTF-8 encoded string.

        Parameters
        ----------
        word: String to be converted into UTF-8.

        Returns
        -------
        UTF-8 encoded string.
        """
        word = word
        # Try to convert to unicode
        try:
            word = unicode(word, "utf-8")
        except TypeError:
            # It's already unicode
            pass
        return word

    def _build_matrix(self, tweets):
        """
        Given some Tweet objects and additional attributes to be added,
        the tweet text is converted into a BoW representation.

        Parameters
        ----------
        tweets: List of tweet objects

        Returns
        -------
        scipy.sparse.csr_matrix.
        A sparse matrix of the BoW indicating which words occur. Each row
        represents a single tweet object.

        """
        DIM = (len(tweets), len(self.vocabulary))
        # indices[indptr[i]:indptr[i+1]] and their corresponding values are
        # stored in data[indptr[i]:indptr[i+1]]. If the shape parameter is not
        # supplied, the matrix dimensions are inferred from the index arrays.
        data = []
        # Indices where data should be stored in the matrix.
        indices = []
        # Indicates the index at which a new line starts in the matrix.
        indptr = [0]
        # Add data from tweets
        for tweet_idx, tweet in enumerate(tweets):
            # print "#tweet", tweet_idx
            text = tweet.preprocessed.split()
            freqs = OrderedCounter(text)
            prev = ""
            for word, freq in freqs.iteritems():
                # Set the count of the respective word according to the
                # word frequency if the word is not an artificial attribute
                word = self._to_unicode(word)
                if word in self.vocabulary and not (self.PREFIX + word) in \
                        self.vocabulary:
                    idx = self.vocabulary[word]
                    # print "update {}-th column for {} in row {} with value {}" \
                    #       "".format(idx, word, tweet_idx, freq)
                    # Update the count in the (tweet_idx)-th row
                    data.append(freq)
                    # Indices are consecutive, e.g. for a 2x4 matrix
                    # [1,2,3,4; 5,6,7,8], hence for each row add #cols to index
                    indices.append(idx)
                # Check bi-grams
                if len(prev) > 0 and self.use_bigrams:
                    prev = self._to_unicode(prev)
                    word_ = tuple((prev, word))
                    if word_ in self.vocabulary:
                        idx = self.vocabulary[word_]
                        data.append(freq)
                        indices.append(idx)
                prev = word
            if self.attrs is not None:
                # Add additional attributes
                for attr in self.attrs:
                    # Artifical attributes have special prefix
                    idx = self.vocabulary[self.PREFIX + attr]
                    attr_value = getattr(tweet, attr)
                    # print "update {}-th column for {} in row {} with value {}" \
                    #       "".format(idx, attr, tweet_idx, attr_value)
                    data.append(attr_value)
                    indices.append(idx)
            # This one indicates when a row ends
            indptr.append(len(indices))
            # Increase offset
            tweet_idx += len(self.vocabulary)
            # print "tweet idx offset changed to", tweet_idx
        # print "Indices", indices
        # print "row indices", indptr
        # print "data", data
        mat = csr_matrix((data, indices, indptr), shape=DIM, dtype=np.float64)
        # print mat.toarray()
        return mat

    def fit(self, tweets):
        """
        Builds a BoW model based on the entered list of tweets.

        Parameters
        ----------
        tweets: List of tweet objects

        Returns
        -------
        scipy.sparse.csr_matrix.
        A sparse matrix of the BoW indicating which words occur. Each row
        represents a single tweet object.

        Raises
        ------
        TypeError if <tweets> isn't a list of Tweet objects.
        AttributeError if not all specified attributes in the list exist for
        a tweet object.

        """
        self._sane(tweets)
        self._init_bow(tweets)
        return self._build_matrix(tweets)

    def transform(self, tweets):
        """
        Builds a BoW model based on the entered list of tweets.

        Parameters
        ----------
        tweets: List of tweet objects

        Returns
        -------
        scipy.sparse.csr_matrix.
        A sparse matrix of the BoW indicating which words occur. Each row
        represents a single tweet object.

        Raises
        ------
        TypeError if <tweets> isn't a list of Tweet objects.
        AttributeError if not all specified attributes in the list exist for
        a tweet object.
        ValueError if the vocabulary is empty

        """
        self._sane(tweets)
        # If vocabulary is empty
        if len(self.vocabulary) == 0:
            warnings.warn(
                "Vocabulary is empty - only fine if use don't use text at all!")
        return self._build_matrix(tweets)

if __name__ == "__main__":
    # Tweet objects must receive a unique ID upon instantiation
    t1 = Tweet(1)
    # Now add whatever attributes you want - you may choose any names
    t1.text = "Today is a wonderful day."
    # This attribute MUST be present: "preprocessed" corresponding to the
    # text that should be transformed into BoW representation. Usually this
    # is the preprocessed text, e.g., after stop word removal, lowercasing etc.
    t1.preprocessed = "today wonderful day."
    # Now add a custom feature called "my_feature"
    t1.my_feature = 1.2
    t2 = Tweet(2)
    t2.preprocessed = "yohoho bla blub"
    # Now add a custom feature called "my_feature"
    t2.my_feature = 3.4
    # Tell the vectorizer to add your custom feature to the BoW
    # representation too
    vec = BoW(additional_attrs=["my_feature"])
    # Now create a list of those objects, i.e., add all sentences that should
    #  be converted into BoW
    tweets = [t1, t2]
    # M is a matrix that has <number_of_instances> rows and
    # <number_of_features> columns.
    M = vec.fit(tweets)
    # Now sort the indices of the column headers according to ascending indices
    tmp = sorted(vec.vocabulary.items(), key=operator.itemgetter(1))
    print "This is how the matrix looks like"
    print M
    # (0, 6)	1.0
    # (0, 4)	1.0
    # (0, 3)	1.0
    # (0, 2)	1.2
    # (1, 1)	1.0
    # (1, 5)	1.0
    # (1, 0)	1.0
    # (1, 2)	3.4
    # It's a sparse matrix representation, i.e., only non-zero entries are shown
    # E.g., for tweet 1 (stored in row 0), column 2, representing the custom
    # feature, is 1.2 and the value for column 3, representing the word
    # "day", is 1
    print "And here are the words represented by each column"
    # [(u'blub', 0), (u'yohoho', 1), (u'$$$_my_feature', 2), (u'day.', 3), (u'wonderful', 4), (u'bla', 5), (u'today', 6)]
    print tmp
    print "Now let's print the matrix in a dense format"
    # This prints the full matrix including 0 entries.
    print M.todense()