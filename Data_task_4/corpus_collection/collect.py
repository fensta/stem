# -*- coding: utf-8 -*-
"""
Collects tweets regarding specific topics in real-time using the Twitter Streaming API.
"""
import tweepy
import json
import pprint
from tweetlistener import TweetListener
import traceback
import sys


if __name__ == "__main__":
    # Topics covered
    # Politics, politicians, stars, musicians, financial crisis, US election,
    # refugees, TV series, movies, companies, CEOs, health, travel, sports,
    # news, product, science,
    hashtags = ["TRUMP", "trump", "Trump", "DonaldTrump", "Donaldtrump",
                "donaldtrump", "DONALDTRUMP", "TrevorNoah", "trevornoah",
                "TREVORNOAH", "MICROSOFT", "Microsoft", "BillGates",
                "BILLGATES", "LinusTorvalds", "LINUSTORVALDS", "LINUX",
                "Linux", "Arsenal", "ManUtd", "Football", "NFL", "Berlin",
                "BarackObama", "Varoufakis", "FinancialCrisis", "KatyPerry",
                "JustinBieber", "Syria", "ISIS", "VladimirPutin", "StarWars",
                "JamesBond", "SherlockHolmes", "GameOfThrones", "Apple",
                "Michelin", "Mediterranean", "AIDS", "HIV", "Cancer", "STD",
                "STI", "Travel", "Muse", "ThomYorke", "LeisureActivity",
                "BreakingNews", "IPhone", "iPhone", "NASA", "StephenHawking"]
    # stovgu account
    consumer_key = "moSGCibt2a8N2uv23rr6FdtaZ"
    consumer_secret = "2flGvgGyhi92YJQeeH9HXykhODCQalobH41i79XwK8OJFrxjQT"
    access_token_key = "1251403087-EgnT69b4tnEWPXmpLzsqSZyKCL8eFB2ifjCv67L"
    access_token_secret = "bWKUT04wDhaXB3WaddUeVsR3JjsDmaDPeuiqHz4ASOCjC"

    # su_collector account
    # consumer_key = "KWWiSKLjvePXa8vEOYzkSRqlM"
    # consumer_secret = "uZZDZtDieHaehblYfFKriCcNqBJYbHmY4ppk5lkClWM8Qo5F7L"
    # access_token_key = "3155739323-HD0ye2ej5WXdo0Xa8jJk0PvWLbVhlDIBQAYHmrT"
    # access_token_secret = "rBLD6g1H0Un0Kqhy3PayOaXCXfkZvTHvdq02aarxJZhjt"

    auth = tweepy.auth.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token_key, access_token_secret)
    api = tweepy.API(auth)
    stream = tweepy.Stream(auth, TweetListener(
        "/media/data/dataset/semeval2016/whatever.txt", api))
    print "Start collecting tweets..."                                      
    try:
        stream.filter(track=hashtags)

        # If the authentication was successful, you should
        # see the name of the account print out
        print(api.me().name)
    except Exception, e:
        print "The following error occurred in main:",e
        print "a", traceback.format_exc(), "b", sys.exc_info()[0]

        stream.disconnect()
