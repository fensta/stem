"""
This file creates a training corpus for the spell checker and for our hash
tokenizer respectively.

Tweets are preprocessed as follows:
1. Remove Twitter handles
2. Remove URLs
3. Integrate hashtags if possible (remove only hashtags where not all
multi-words exist in dictionary)
4. Remove emoticons
5. Remove punctuation
6. Replace slang
7. NER detection according to Stanford POS tagger from Mishal (extra script)
8. Expand negations
9. spellcheck remaining words, but ignore NERs as names shouldn't be changed.
10. Convert to lowercase
"""


import codecs
import re
import string
import itertools
import cPickle

import enchant
from nltk.corpus import wordnet as wn
from nltk.corpus import sentiwordnet as swn
import wikipedia

from Data_task_4.corpus_collection.POS_NER_Tags import preprocess_ner
from manual_improvements import NEGATION_REPLACEMENTS, SLANG, REPLACEMENTS, \
    EMOTICONS


def process_tweets(in_file, out_file):
    """
    Reads in tweets, preprocesses them and stores the resulting tweets in a
    separate file.

    Parameters
    ----------
    in_file: Path to the .txt file containing only tweet messages.
    out_file: Path to the .txt file in which the preprocessed texts are stored.

    """
    training_tweets = 0
    with codecs.open(in_file, "r", "utf-8-sig") as f:
        for line in f:
            # Remove empty lines and then split on whitespaces
            line = line.strip()
            # line = replace_symbols(line)

            line = multiple_replace(line, REPLACEMENTS)
            line = line.split()
            d = enchant.Dict("en")
            if line:
                line = remove_url(line)
                line = remove_twitter_handle(line)
                line = remove_hash(line, d)
                line = emoticons(line)
                line = punctuation(line)
                line = replace_slang(line, SLANG)
                with codecs.open(out_file, "a", encoding="utf-8") as f:
                    f.write(" ".join(line) + "\n")
                training_tweets += 1
    print "Number of preprocessed tweets in the training set:", training_tweets


def replace_slang(text, subst):
    """
    Replaces slang words with a given substitution in a tweet.

    Parameters
    ----------
    text: List of words representing a tweet.
    subst: Dictionary containing substitutions for each slang word.

    Returns
    -------
    Text without slang.

    """
    corrected = []
    for word in text:
        # Don't transform original word into lowercase
        w = word.lower()
        for slang in subst:
            if w == slang:
                word = subst[slang]
        corrected.append(word)
    return corrected


def remove_url(text):
    """
    Remove URLs from tweet.

    Parameters
    ----------
    text: List of words representing a tweet.

    Returns
    -------
    Tweet without URLs.

    """
    clean = [w for w in text if not w.startswith('http') and not
             w.startswith('https')]
    return clean


def remove_twitter_handle(text):
    """
    Remove Twitter handles from the tweet.
    """
    clean = [w for w in text if not w.startswith('@')]
    return clean


def multiple_replace(strg, rep_dict):
    pattern = re.compile("|".join([re.escape(k) for k in rep_dict.keys()]),
                         re.M)
    return pattern.sub(lambda x: rep_dict[x.group(0)], strg)


def expand_negations(words, rep_dict):
    """
    Replaces short-forms of negations by long forms, e.g. can't -> can not.
    """
    expanded = []
    # [(word, POS tag), ...]
    for w, tag in words:
        for neg in rep_dict:
            if w.lower() == neg:
                w = rep_dict[neg]
        expanded.append(tuple((w, tag)))
    return expanded


def emoticons(text):
    """
    Remove emoticons from tweets.
    """
    new_text = []
    match_found = False
    for w in text:
        existing_emojis = []
        w_tmp = w
        for emoji in EMOTICONS.iterkeys():
            if emoji in w_tmp:
                existing_emojis.append(emoji)
                match_found = True
                not_processed = True
                while not_processed:
                    # Split at first match and process remainder separately
                    splitted = w_tmp.split(emoji, 1)
                    w_tmp = ''.join(splitted)
                    if emoji not in w_tmp:
                        not_processed = False
        # Remove emoticons from message
        new_word = w
        for emoji in existing_emojis:
            new_word = new_word.replace(emoji, " ")
        new_word = new_word.strip()
        # Add word only if an emoticon exists in it
        if match_found:
            new_text.extend(new_word.split())
        if not match_found:
            new_text.append(w)
        match_found = False
    return new_text


def punctuation(text):
    #Removes punctuation and RT
    puncts = list(string.punctuation)

    text_update = []
    punct_found = False

    for w in text:
        puncts_present = []
        w_tmp = w
        for punct in puncts:
            if punct in w_tmp:
                puncts_present.append(punct)
                punct_found = True
                not_seen = True
                while not_seen:
                    splitted = w_tmp.split(punct, 1)
                    w_tmp = ''.join(splitted)
                    if punct not in w_tmp:
                        not_seen = False
        update_word = w
        for punct in puncts_present:
            # If word ends with "s'" (Andreas'), remove only "'"
            if punct == "'" and len(update_word) > 2 and update_word[-2] == \
                    "s" and update_word[-1] == "'":
                update_word = update_word[:-1]
            # If apostrophe, add NO whitespace except word ends with "s" (
            # possessive form)
            if punct == "'" and update_word[-1] != "s":
                update_word = update_word.replace(punct, "")
            # Word contains a single letter which isn't "a" or "i" or a number
            elif len(update_word) == 1 and (update_word.lower() != "a" or \
                update_word.lower() != "i" or not update_word.isdigit()):
                    update_word = ""
            else:
                update_word = update_word.replace(punct, " ")
        update_word_tmp = update_word.strip()
        updated_word = []
        # Check if one of the words now is a single letter after split ->
        # remove it
        for word in update_word_tmp.split():
            if len(word) == 1:
                if update_word_tmp.lower() == "a" or update_word_tmp.lower() \
                        == "i" or update_word_tmp.isdigit():
                    updated_word.append(word)
            else:
                updated_word.append(word)
        # Remove "RT" or "rt"
        cleaned = []
        for w_ in updated_word:
            if w_ != "rt" and w_ != "RT":
                cleaned.append(w_)

        if punct_found:
            text_update.extend(cleaned)
        if not punct_found and w != "rt" and w != "RT":
            text_update.append(w)
        punct_found = False
    return text_update


def remove_hash(text, dic):
    """
    If hashtag is comprised of a single known word, it is kept without
    hashtag. Same goes for the case when the hashtag is written in camel case.
    Otherwise the hashtags are completely discarded.

    """
    cleaned = []
    for w in text:
        if w.startswith("#"):
            # Is it a single word?
            w = w[1:]
            if dic.check(w):
                cleaned.append(w)
            else:
                # Is it CamelCase? Word must contain at least 2 capitalized
                # letters
                camel_string = ""
                is_camel = False
                # And first character is uppercase while 2nd is lowercase
                if len(w) > 1 and w[0].isupper() and w[1]:
                    for l in w[1:]:
                        if l.isupper():
                            is_camel = True
                            break
                if is_camel:
                    for l in w:
                        if l.isupper():
                            l = " " + l
                        camel_string += l
                    cleaned.extend(camel_string.strip().split())
                else:
                    # Multiple words in hashtag
                    result = tokenize_hashtag(w, dic)
                    if result is not None:
                        cleaned.extend(result)
        else:
            cleaned.append(w)
    return cleaned


# http://stackoverflow.com/questions/18406776/python-split-a-string-into-all-
# possible-ordered-phrases
def candidates(word):
    # n = 1..(n-1)
    ns = range(1, len(word))
    # split into 2, 3, 4, ..., n parts.
    for n in ns:
        for idxs in itertools.combinations(ns, n):
            yield [''.join(word[i:j]) for i, j in zip((0,) + idxs, idxs + (
                None,))]


def tokenize_hashtag(w, dic):
    """
    Tokenizes a multi-word hashtag that isn't written in camelcase.

    Parameters
    ----------
    w: Word which represents a hashtag without the hash.
    dic: PyEnchant Dic representing the language to be expected.

    Returns
    -------
    List.
    Tokenized hashtag as a list of words. If no such tokenization can be
    found, None is returned.

    """
    likely = []
    for candidate in candidates(w):
        to_add = []
        is_candidate = True
        for w in candidate:
            is_correct = False
            # Replace slang and expand negations
            if w in NEGATION_REPLACEMENTS:
                is_correct = True
                w = NEGATION_REPLACEMENTS[w]
            if w in SLANG:
                is_correct = True
                w = SLANG[w]
            # Replaced words don't need to be checked
            if not is_correct:
                # If it's a single letter word or not present in the dictionary
                if not dic.check(w) or w.lower() in "bcdfghjklmnopqrstvwyz":
                    is_candidate = False
                    break
                else:
                    # Valid word
                    to_add.append(w)
            else:
                # Valid word after replacement -> might contain multiple words
                to_add.extend(w.split())
        if is_candidate:
            likely.append(to_add)
    # If a combination of valid words was found, use the one with the lowest
    # number of words
    if len(likely) > 0:
        # Use the candidate with lowest number of words
        shortest = 100000000
        for idx, candidate in enumerate(likely):
            if len(candidate) < shortest:
                shortest = idx
        return likely[shortest]
    return None


# def remove_elongation(text):
#     """
#     Removes elongations of vowels or consonants as they are grammatically
#     incorrect words.
#
#     """
#     cleaned = []
#     for w in text:
#         chars_to_remove = []
#         # List of lists, e.g. [["h", 1], ["i", 1]] for word "hi"
#         char_counts = [[k, len(list(g))] for k, g in groupby(w)]
#         for char, cnt in char_counts:
#
#             print char, cnt
#             # For any duplicate characters
#             if cnt > 1:
#                 wn_results = wn.synsets(w)
#                 swn_results = list(swn.senti_synsets(w))
#                 print "Wordnet", wn_results
#                 print "Sentiwordnet", swn_results
#                 # Is it a known word?
#                 if len(wn_results) > 0 or len(swn_results) > 0:
#                     print "single word in hashtag:", w
#                     cleaned.append(w)
#                 else:
#                     # Remove one of the repeated characters and try again
#                     chars_to_remove.append(char)
#         print "duplicate characters that need to be removed"
#         is_real_word = False
#         w_temp = w
#         for char in chars_to_remove:
#             w_temp = remove_consecutive_letter(w_temp, char)
#             wn_results = wn.synsets(w)
#             swn_results = list(swn.senti_synsets(w))
#             print "Wordnet", wn_results
#             print "Sentiwordnet", swn_results
#             # Is it a known word?
#             if len(wn_results) > 0 or len(swn_results) > 0:
#                 print "single word in hashtag:", w
#                 cleaned.append(w_temp)
#                 break
#
#     return cleaned


def to_lower(text):
    low = []
    for w in text:
        low.append(w.lower())
    return low


def spell_checking(tweet):
    """
    Takes a list of (word, POS tag) tuples and checks spelling if it's not a
    NER.

    """
    d = enchant.Dict("en")
    err = []
    corrected = []
    for w, tag in tweet:
        # If not a NER (-> don't change NERs, just append them)
        if tag == "O":
            if not d.check(w) and w != "can not":
                # First, remove letters that occur more than twice and check
                # again in the dictionary. If it still can't be found,
                # correct typos
                # Matches any characters that occurs > 2 times (case
                # insensitive)
                matcher = re.compile(r"(?i)([a-z])\1{2,}")
                matches = list(match.group() for match in matcher.finditer(w))
                # Replace each match with a single occurrence of the
                # elongated letter
                for m in matches:
                    letter = m[0].lower()
                    w = w.replace(m, letter)
                # Correct typos
                if not d.check(w) and w != "can not":
                    try:
                        first_sug = d.suggest(w)[0]
                        corrected.append(first_sug)
                        # print "word: {} suggestions: {}".\
                        #     format(w, d.suggest(w))
                    except IndexError:
                        # Words which need to be treated manually
                        err.append((w, None))
                else:
                    corrected.append(w)
            else:
                corrected.append(w)
        else:
            corrected.append(w)
    return corrected, err


def further_process_tweets(in_file, out_file):
    """
    Turns the words into lowercase and checks spelling of all non-NERs.
    Stores results in a file where each line corresponds to a tweet.

    Parameters
    ----------
    in_file: Path to the input file containing the pickled POS and NER tags.
    out_file: File in which the results should be stored.

    """
    # wikipedia.set_lang("en")
    with open(in_file, "rb") as f:
        ner_tags_corpus = cPickle.load(f)
    # print ner_tags_corpus
    # corrected_tweets = []
    # error_words = []
    for tweet in ner_tags_corpus:
        negated = \
            expand_negations(tweet, NEGATION_REPLACEMENTS)
        corrected, errors_to_inspect = spell_checking(negated)
        corrected = to_lower(corrected)
        # corrected_tweets.append(corrected)
        # error_words.append(errors_to_inspect)
        with codecs.open(out_file, "a", encoding="utf-8") as f:
                    f.write(" ".join(corrected) + "\n")
        # This file contains words for which the check didn't work,
        # so we need to manually deal with them
        with codecs.open(out_file + ".err", "a", encoding="utf-8") as f:
                        f.write(" ".join(errors_to_inspect) + "\n")
    # checked_tweets = []
    # for tweet in corrected_tweets:
    #     merged = []
    #     # Skip the first word
    #     for idx, w in enumerate(tweet[1:]):
    #         tmp_word = w
    #         # If word contains at most 2 letters, try to merge it with
    #         # predecessor
    #         if len(w) < 3:
    #             print idx
    #             previous_word = tweet[idx]
    #             result = wikipedia.search(previous_word + tmp_word)
    #             print "For {} WiKi says {}".format(previous_word + tmp_word,
    #                                                result)
    #             # Entry in Wikipedia exists
    #             if result:
    #                 # Remove word from tweet as it's now added with the
    #                 # short word again
    #                 if merged:
    #                     del merged[-1]
    #                 tmp_word = previous_word + tmp_word
    #         merged.append(tmp_word)
    #     checked_tweets.append(merged)
    # print checked_tweets


def delete_content(fname):
    with open(fname, "w"):
        pass


def reset_files(a, b, c):
    delete_content(a)
    delete_content(b)
    delete_content(c)


if __name__ == "__main__":
    # Debugging
    test_file = "test_raw.txt"
    tmp_file = "test_preprocessed.txt"
    ner_file = "ner_test_tags.pkl"
    out_file = "training_corpus_test.txt"
    #  TODO: INCREASE heap space
    #     Loading default properties from tagger /home/fensta/Stanford/stanford-postagger-full-2015-04-20/models/english-bidirectional-distsim.tagger
    #   File "/media/data/Workspaces/PythonWorkspace/SemEval2016 (SA)/Data_task_4/corpus_collection/create_corpus.py", line 484, in <module>
    # Reading POS tagger model from /home/fensta/Stanford/stanford-postagger-full-2015-04-20/models/english-bidirectional-distsim.tagger ... done [1.0 sec].
    # Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
    # 	at edu.stanford.nlp.sequences.ExactBestSequenceFinder.bestSequence(ExactBestSequenceFinder.java:109)
    # 	at edu.stanford.nlp.sequences.ExactBestSequenceFinder.bestSequence(ExactBestSequenceFinder.java:31)
    # 	at edu.stanford.nlp.tagger.maxent.TestSentence.runTagInference(TestSentence.java:322)
    # 	at edu.stanford.nlp.tagger.maxent.TestSentence.testTagInference(TestSentence.java:312)
    # 	at edu.stanford.nlp.tagger.maxent.TestSentence.tagSentence(TestSentence.java:135)
    # 	at edu.stanford.nlp.tagger.maxent.MaxentTagger.tagSentence(MaxentTagger.java:998)
    # 	at edu.stanford.nlp.tagger.maxent.MaxentTagger.tagCoreLabelsOrHasWords(MaxentTagger.java:1788)
    # 	at edu.stanford.nlp.tagger.maxent.MaxentTagger.tagAndOutputSentence(MaxentTagger.java:1798)
    # 	at edu.stanford.nlp.tagger.maxent.MaxentTagger.runTagger(MaxentTagger.java:1709)
    # 	at edu.stanford.nlp.tagger.maxent.MaxentTagger.runTagger(MaxentTagger.java:1770)
    # 	at edu.stanford.nlp.tagger.maxent.MaxentTagger.runTagger(MaxentTagger.java:1543)
    # 	at edu.stanford.nlp.tagger.maxent.MaxentTagger.runTagger(MaxentTagger.java:1499)
    # 	at edu.stanford.nlp.tagger.maxent.MaxentTagger.main(MaxentTagger.java:1842)
    #
    #     preprocess_ner(tweets, ner_file)
    #   File "/media/data/Workspaces/PythonWorkspace/SemEval2016 (SA)/Data_task_4/corpus_collection/POS_NER_Tags.py", line 53, in preprocess_ner
    #     stc_POS = POSTags(path1, stc_tokens)
    #   File "/media/data/Workspaces/PythonWorkspace/SemEval2016 (SA)/Data_task_4/corpus_collection/POS_NER_Tags.py", line 21, in POSTags
    #     stc_POS = POS.tag(sentence)
    #   File "/usr/local/lib/python2.7/dist-packages/nltk/tag/stanford.py", line 66, in tag
    #     return sum(self.tag_sents([tokens]), [])
    #   File "/usr/local/lib/python2.7/dist-packages/nltk/tag/stanford.py", line 89, in tag_sents
    #     stdout=PIPE, stderr=PIPE)
    #   File "/usr/local/lib/python2.7/dist-packages/nltk/internals.py", line 162, in java
    #     raise OSError('Java command failed : ' + str(cmd))
    # OSError: Java command failed : ['/usr/lib/jvm/java-8-oracle/bin/java', '-mx1000m', '-cp', '/home/fensta/Stanford/stanford-postagger-full-2015-04-20/stanford-postagger.jar', 'edu.stanford.nlp.tagger.maxent.MaxentTagger', '-model', '/home/fensta/Stanford/stanford-postagger-full-2015-04-20/models/english-bidirectional-distsim.tagger', '-textFile', '/tmp/tmpmpOWki', '-tokenize', 'false', '-outputFormatOptions', 'keepEmptySentences', '-encoding', 'utf8']

    # Testing
    # test_file = "/media/data/dataset/semeval2016/whatever (copy).txt"
    # tmp_file = "/media/data/dataset/semeval2016/whatever (" \
    #            "copy)_preprocessed.txt"
    # ner_file = "/media/data/dataset/semeval2016/whatever (" \
    #            "copy)_ner_tags.pkl"
    # out_file = "/media/data/dataset/semeval2016/whatever (" \
    #            "copy)_training_corpus.txt"

    # Production
    # test_file = "/media/data/dataset/semeval2016/whatever.txt"
    # out_file = "/media/data/dataset/semeval2016/whatever_preprocessed.txt"

    reset_files(tmp_file, ner_file, out_file)
    process_tweets(test_file, tmp_file)
    content = open(tmp_file, 'r')
    tweets = content.readlines()
    preprocess_ner(tweets, ner_file)
    further_process_tweets(ner_file, out_file)



