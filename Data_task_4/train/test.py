from sklearn.ensemble import RandomForestClassifier


# Initialize a Random Forest classifier with 500 trees
forest = RandomForestClassifier(n_estimators=500)

# Fit the forest to the training set, using the bag of words as
# features and the sentiment labels as the response variable
#
# This may take a few minutes to run
forest = forest.fit(train_data_features, train["sentiment"])
