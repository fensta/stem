# This file contains information that we added manually to improve our
# performance.

# Mapping of the class to its sentiment
LABELS = {
    "+": 1,    # Positive
    "0": 0,    # Neutral
    "-": -1,    # Negative
    "*": 0.5,   # Intensifier
    "/": -0.5,    # Diminisher
}

# This dictionary contains the lists of emoticons carrying sentiment in our
# view. Taken from: https://en.wikipedia.org/wiki/List_of_emoticons
# {emoticon: sentiment}
# added some additional emoticons: ^^ and <3, ^_^, T_T, t_t
EMOTICONS = {
    # Happy faces
    ":-)": LABELS["+"],
    ":)": LABELS["+"],
    ":D": LABELS["+"],
    ":o)": LABELS["+"],
    ":]": LABELS["+"],
    ":3": LABELS["+"],
    ":c)": LABELS["+"],
    ":>": LABELS["+"],
    "=]": LABELS["+"],
    "8)": LABELS["+"],
    "=)": LABELS["+"],
    ":}": LABELS["+"],
    ":^": LABELS["+"],
    # Laughing
    ":-D": LABELS["+"],
    "8-D": LABELS["+"],
    "8D": LABELS["+"],
    "x-D": LABELS["+"],
    "xD": LABELS["+"],
    "X-D": LABELS["+"],
    "XD": LABELS["+"],
    "=-D": LABELS["+"],
    "=D": LABELS["+"],
    "=-3": LABELS["+"],
    "=3": LABELS["+"],
    "B^D": LABELS["+"],
    # Very happy
    ":-))": LABELS["+"],
    # Tears of happiness
    ":'-)": LABELS["+"],
    ":')": LABELS["+"],
    # Sad
    ">:[": LABELS["-"],
    ":-(": LABELS["-"],
    ":=(": LABELS["-"],
    ":(": LABELS["-"],
    ":-c": LABELS["-"],
    ":c": LABELS["-"],
    ":-<": LABELS["-"],
    ":<": LABELS["-"],
    ":-[": LABELS["-"],
    ":[": LABELS["-"],
    ":{": LABELS["-"],
    # Angry
    ":-||": LABELS["-"],
    ":@": LABELS["-"],
    ">:(": LABELS["-"],
    # Crying
    ":'-(": LABELS["-"],
    ":'(": LABELS["-"],
    # Horror, disgust, sadness
    "D:<": LABELS["-"],
    "D:": LABELS["-"],
    "D8": LABELS["-"],
    "D;": LABELS["-"],
    "D=": LABELS["-"],
    "DX": LABELS["-"],
    "v.v": LABELS["-"],
    "D-'": LABELS["-"],
    # Sceptical, annoyed, undecided, uneasy, hesitant
    ">:\\": LABELS["-"],
    ">:/": LABELS["-"],
    ":-/": LABELS["-"],
    ":-.": LABELS["-"],
    ":/": LABELS["-"],
    ":\\": LABELS["-"],
    "=/": LABELS["-"],
    "=\\": LABELS["-"],
    ":L": LABELS["-"],
    "=L": LABELS["-"],
    ":S": LABELS["-"],
    ">.<": LABELS["-"],
    # Own
    "<3": LABELS["+"],
    "^^": LABELS["+"],
    "^_^": LABELS["+"],
    "t_t": LABELS["-"],
    "T_T": LABELS["-"],
    "xo": LABELS["+"],
    "xx": LABELS["+"],
    "XO": LABELS["+"],
    "XX": LABELS["+"],
    "\o/": LABELS["+"],
    "*\0/*": LABELS["+"],
    "yay": LABELS["+"],
    "lol": LABELS["+"],
    "lolol": LABELS["+"],
    "hohoho": LABELS["+"],
    "rofl": LABELS["+"],
}

# Taken from the paper: "SentiKLUE: Updating a Polarity Classifier in 48 Hours"
NEGATIONS = ["not", "don't", "doesn't", "won't", "can't", "mustn't", "isn't",
             "aren't", "wasn't", "weren't", "couldn't", "shouldn't", "wouldn't"]

# HTML-escaped symbols that need to be replaced in tweets
REPLACEMENTS = {
    "&lt;": "<",
    "&gt;": ">",
    "&amp;": "and",
    "&lt; 3": "<3",
    "< 3": "<3",
    "\\'": "'",
    "...": " ",
    "..": " ",
    # Remove all ambiguous emoticons
    ";)": " ",
    ">:O": " ",
    ":O": " ",
    ":o": " ",
    "O_O": " ",
    "O_o": " ",
    "o_O": " ",
    "o_o": " ",
    "O-O": " ",
    ">:P": " ",
    ":P": " ",
    "xp": " ",
    "XP": " ",
    ":-p": " ",
    ":p": " ",
    "=p": " ",
    ":-P": " ",
    ":P": " ",
    ":P": " ",
    ":-b": " ",
    ":b": " ",
    "d:": " ",
    "O:-)": " ",
    "0:-3": " ",
    "0:3": " ",
    "0:-)": " ",
    "0:)": " ",
    "0;^)": " ",
    "'s": " 's",
    "c'mon": "come on",
    "C'mon": "come on",
    "1st": "first",
    "2nd": "second",
    "3rd": "third"
}

NEGATION_REPLACEMENTS = {
    # Negations
    "don't": "do not",
    "doesn't": "does not",
    "won't": "will not",
    "can't": "can not",
    "mustn't": "must not",
    "isn't": "is not",
    "aren't": "are not",
    "wasn't": "was not",
    "weren't": "were not",
    "couldn't": "could not",
    "shouldn't": "should not",
    "wouldn't": "would not",
    # Forms without apostrophe
    "dont": "do not",
    "dnt": "do not",
    "doesnt": "does not",
    "wont": "will not",
    "cant": "can not",
    "mustnt": "must not",
    "isnt": "is not",
    "arent": "are not",
    "wasnt": "was not",
    "werent": "were not",
    "couldnt": "could not",
    "shouldnt": "should not",
    "wouldnt": "would not",
    # This is the only case that will be true when checking for negations after
    # expanding them -> don't delete it!
    "not": "not"
}

# Slang to be replaced
# http://searchcrm.techtarget.com/definition/Twitter-chat-and-text-messaging-abbreviations
SLANG = {
    "u": "you",
    "ur": "your",
    "r": "are",
    "bc": "because",
    "b/c": "because",
    "b/w": "between",
    "thx": "thanks",
    "2": "too",
    "4": "for",
    "imo": "in my opinion",
    "imho": "in my humble opinion",
    "k": "okay",
    "kk": "okay",
    "bday": "birthday",
    "b-day": "birthday",
    "bf": "boyfriend",
    "gf": "girlfriend",
    "bff": "best friends forever",
    "cu": "see you",
    "abt": "about",
    "afaik": "as far as I can tell",
    "asap": "as soon as possible",
    "b4": "before",
    "btw": "by the way",
    "cmon": "come on",
    "diy": "do it yourself",
    "fb": "Facebook",
    "faq": "frequently asked questions",
    "ff": "follow Friday",
    "ftw": "for the win",
    "fyi": "for your information",
    "hru": "how are you",
    "idk": "I do not know",
    "n8": "night",
    "RIP": "rest in peace",
    "R.I.P.": "rest in peace",
    "lmao": "laughing my ass off",
    "lmfao": "laughing my ass off",
    # "rofl": "rolling on the floor laughing",
    # "lol": "laughing out loud",
    "noob": "novice",
    "newb": "novice",
    "np": "no problem",
    "omw": "on my way",
    "pls": "please",
    "rly": "really",
    "roflcopter": "rolling on the floor and spinning around",
    "rtfm": "read the manual",
    "rfm": "read the manual",
    "smh": "shaking my head",
    "srsly": "seriously",
    "stfu": "shut the fuck up",
    "sup": "what is up",
    "shud": "should",
    "wud": "would",
    "cud": "could",
    "tbh": "to be honest",
    "tgif": "thank god it's Friday",
    "w/e": "whatever",
    "wrk": "work",
    "y": "why",
    "mo": "Monday",
    "mon": "Monday",
    "tu": "Tuesday",
    "tue": "Tuesday",
    "wed": "Wednesday",
    "th": "Thursday",
    "thu": "thursday",
    "fr": "Friday",
    "fri": "Friday",
    "sa": "Saturday",
    "su": "Sunday",
    "thru": "through",
    "tho": "though",
    "wtf": "what the fuck",
    "xmas": "christmas",
    "x-mas": "christmas",
    "nye": "new year's eve",
    "ny": "New York",
    "n.y.": "New York",
    "ofc": "of course",
    "j/k": "just kidding",
    "lamer": "lame",
    "m8": "mate",
    "l8r": "later",
    "n1": "nice",
    "nvm": "nevermind",
    "ot": "off topic",
    "pita": "pain in the arse",
    "ppl": "people",
    "wat": "what",
    "tg": "that is great",
    "tos": "terms of service",
    "ty": "thank you",
    "wb": "welcome back",
    "wth": "what the hell",
    "yw": "you are welcome",
    "jan": "January",
    "feb": "February",
    "mar": "March",
    "apr": "April",
    # "may": "May",
    "june": "June",
    "july": "July",
    "aug": "August",
    "sept": "September",
    "oct": "October",
    "nov": "November",
    "dec": "December",
    "ya": "you",
    "damn": "damned",
    "hekka": "really",
    "purrty": "pretty",
    "f##k": "fuck",
    "f#ck": "fuck",
    "fu#k": "fuck",
    "s**t": "shit",
    "hella": "very",
    "awol": "missing",
    "AWOL": "missing"
}


INTENSIFIERS = {
    # Diminishers
    "slightly": LABELS["/"],
    "rather": LABELS["/"],
    "little": LABELS["/"],
    "fairly": LABELS["/"],
    "relatively": LABELS["/"],
    "quite": LABELS["/"],
    "bit": LABELS["/"],
    # Boosters - synonyms of extremely from google "extremely synonym"
    "bloody": LABELS["*"],
    "dead": LABELS["*"],
    "too": LABELS["*"],
    # "so": LABELS["*"],
    "fucking": LABELS["*"],
    "extremely": LABELS["*"],
    "very":	LABELS["*"],
    "exceptionally": LABELS["*"],
    "exceedingly": LABELS["*"],
    "especially": LABELS["*"],
    "extraordinarily": LABELS["*"],
    "extra": LABELS["*"],
    "tremendously": LABELS["*"],
    "immensely": LABELS["*"],
    "mega": LABELS["*"],
    "highly": LABELS["*"],
    "ultra": LABELS["*"],
    "vastly": LABELS["*"],
    "hugely": LABELS["*"],
    "abundantly": LABELS["*"],
    "intensely": LABELS["*"],
    "significantly": LABELS["*"],
    "outstandingly": LABELS["*"],
    "particularly": LABELS["*"],
    "supremely": LABELS["*"],
    "remarkably": LABELS["*"],
    "really": LABELS["*"],
    "truly": LABELS["*"],
    "awfully": LABELS["*"],
    "terribly": LABELS["*"],
    "seriously": LABELS["*"],
    "damn": LABELS["*"],
    "damned": LABELS["*"],
    "absolutely": LABELS["*"],
    "totally": LABELS["*"],
}
