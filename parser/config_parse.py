import ConfigParser


def parse_config_file(conf_path):
    """
    Parses a config file and returns all extracted parameters.

    Parameters
    ----------
    conf_path: Path to the configuration file to be opened.

    Returns
    -------
    List of all the parameters in a structured way.

    """
    parser = ConfigParser.ConfigParser()
    print parser.read(conf_path)
    print parser.sections()
    all_params = []
    for section in parser.sections():
        print "now retrieve parameters for section", section
        options = __get_options(parser, section)
        all_params.append(options)
    return all_params


def get_list(option, sep="\n", chars=None):
    """Return a list from a ConfigParser option. By default,
       split on a newline and strip whitespaces."""
    result = []
    for chunk in option.split(sep):
        # Only if current line isn't blank
        if chunk.strip():
            # print "chunk", chunk
            # Remove newline character
            result.append(chunk.strip(chars))
            # return chunk.strip()
    return result


def get_dict(option, value, sep="\n", chars=None):
    """
    Creates a dictionary of the key value pairs stored in a string.

    Parameters
    ----------
    option: Name of the parameter.
    value: String to be converted into a dictionary.
    sep: Seperator to be used to distinguish the different key-value pairs
    from each other.
    chars: Characters to remove.

    Returns
    -------
    Dictionary with the transformed dictionary

    """
    result = {}
    # value contains some more keys, so we have as value for option a nested
    # dictionary
    if sep in value:
        tmp = {}
        for entry in value.split(sep):
            # Ignore blank lines
            if len(entry) > 0:
                k = entry.split(":")[0].strip()
                v = entry.split(":")[1].strip()
                tmp[k.strip()] = v.strip()
        result.update({option.strip(): tmp})
    else:
        result = {option: value}
    return result


def __get_options(parser, section):
    """
    Get the options (=parameters) of each section in the configuration file.

    Parameter
    ---------
    parser: ConfigParser object.
    section: Section in the .ini file.

    Returns
    -------
    Returns a dictionary of the parameters. Only for preprocessing a list of
    parameters is returned to preserve the order of the parameters. It's
    accessible via the "preprocess" entry. Likewise, classifier options are
    exposed via "classifier" and evaluation parameters via "evaluation".
    """
    params = []
    param = {}
    # print "options", parser.options(section)
    for option in parser.options(section):
        try:
            # print "option:", option
            value = parser.get(section, option)
            # Return all parameters as a list because we want to preserve order
            if section == "preprocess":
                value = get_list(value)
                params.append(tuple([option, value]))
                # print "params", params
            if section == "classifier" or section == "evaluation":
                param[section] = {}
                d = get_dict(option, value)
                # d = {option: value}
                params.append(d)
            else:
                # Store values in a dictionary to allow direct access
                param[option] = value
                if param[option] == -1:
                    print("skip: %s" % option)
        except Exception, e:
            print("exception on %s!" % option)
            print str(e)
            param[option] = None
    if section == "input" or section == "output" or section == \
            "feature_selection":
        return {section: param}
    if section == "preprocess":
        return {section: params}
    if section == "classifier" or section == "evaluation":
        for dictionary in params:
            param[section].update(dictionary)
        return param
    print "result of section", param
    return param


if __name__ == "__main__":
    config = "config.ini"
    params = parse_config_file(config)
    print params

    # for entry in params:
    #     print "entry", entry
    #     # How to iterate over parameters for the classifier
    #     if "classifier" in entry:
    #         for k,v in entry["classifier"].iteritems():
    #             print k,v
    #     # How to iterate over parameters for preprocessing
    #     if "preprocess" in entry:
    #         for t in entry["preprocess"]:
    #             print "key: {} value: {}".format(t[0], t[1])